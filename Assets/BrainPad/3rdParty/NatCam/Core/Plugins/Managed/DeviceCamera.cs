/* 
*   NatCam Core
*   Copyright (c) 2016 Yusuf Olokoba
*/

namespace NatCamU.Core {

    using UnityEngine;
    using System.Linq;
    using Platforms;

    [CoreDoc(@"DeviceCamera")]
    public sealed class DeviceCamera {

        #region --Getters--

        /// <summary>
        /// Get the current preview resolution of the camera
        /// </summary>
        [CoreDoc(@"PreviewResolution")]
        public Resolution PreviewResolution { get { return Device.GetPreviewResolution(this); }}
        /// <summary>
        /// Get the current photo resolution of the camera
        /// </summary>
        [CoreDoc(@"PhotoResolution")]
        public Resolution PhotoResolution { get { return Device.GetPhotoResolution(this); }}
        /// <summary>
        /// Get the current framerate of the camera
        /// </summary>
        [CoreDoc(@"Framerate")]
        public float Framerate { get { return Device.GetFramerate(this); }}
        /// <summary>
        /// Is the camera front facing?
        /// </summary>
        [CoreDoc(@"IsFrontFacing")]
        public bool IsFrontFacing { get { return Device.IsFrontFacing(this); }}
        /// <summary>
        /// Does this camera support flash?
        /// </summary>
        [CoreDoc(@"IsFlashSupported")]
        public bool IsFlashSupported { get { return Device.IsFlashSupported(this); }}
        /// <summary>
        /// Does this camera support torch?
        /// </summary>
        [CoreDoc(@"IsTorchSupported")]
        public bool IsTorchSupported { get { return Device.IsTorchSupported(this); }}
        /// <summary>
        /// Get the camera's horizontal field-of-view
        /// </summary>
        [CoreDoc(@"HorizontalFOV")]
        public float HorizontalFOV { get { return Device.HorizontalFOV(this); }}
        /// <summary>
        /// Get the camera's vertical field-of-view
        /// </summary>
        [CoreDoc(@"VerticalFOV")]
        public float VerticalFOV { get { return Device.VerticalFOV(this); }}
        /// <summary>
        /// Get the camera's minimum exposure bias
        /// </summary>
        [CoreDoc(@"MinExposureBias")]
        public float MinExposureBias { get { return Device.MinExposureBias(this); }}
        /// <summary>
        /// Get the camera's maximum exposure bias
        /// </summary>
        [CoreDoc(@"MaxExposureBias")]
        public float MaxExposureBias { get { return Device.MaxExposureBias(this); }}
        /// <summary>
        /// Get the camera's maximum zoom ratio
        /// </summary>
        [CoreDoc(@"MaxZoomRatio")]
        public float MaxZoomRatio { get { return Device.MaxZoomRatio(this); }}
        #endregion


        #region ---Properties---

        /// <summary>
        /// Get or set the camera's focus mode
        /// </summary>
        [CoreDoc(@"CameraFocusMode"), Code(@"ContinuousFocus")]
        public FocusMode FocusMode {
            get { return (FocusMode)Device.GetFocusMode(this); }
            set { Device.SetFocusMode(this, (int)value); }
        }
        /// <summary>
        /// Get or set the camera's exposure mode
        /// </summary>
        [CoreDoc(@"CameraExposureMode")]
        public ExposureMode ExposureMode {
            get { return (ExposureMode)Device.GetExposureMode(this); }
            set { Device.SetExposureMode(this, (int)value); }
        }
        /// <summary>
        /// Get or set the camera's exposure bias
        /// </summary>
        [CoreDoc(@"ExposureBias", @"ExposureBiasDiscussion")]
        public float ExposureBias {
            get { return Device.GetExposure(this); }
            set { Device.SetExposure(this, value); }
        }
        /// <summary>
        /// Get or set the camera's flash mode when taking a picture
        /// </summary>
        [CoreDoc(@"CameraFlashMode")]
        public FlashMode FlashMode {
            get { return (FlashMode)Device.GetFlash(this); }
            set { Device.SetFlash(this, (int)value); }
        }
        /// <summary>
        /// Get or set the camera's torch mode
        /// </summary>
        [CoreDoc(@"TorchEnabled")]
        public bool TorchEnabled {
            get { return Device.GetTorchEnabled(this); }
            set { Device.SetTorchEnabled(this, value); }
        }
        /// <summary>
        /// Get or set the camera's current zoom ratio. This value must be between [1, MaxZoomRatio]
        /// </summary>
        [CoreDoc(@"ZoomRatio")]
        public float ZoomRatio {
            get { return Device.GetZoom(this); }
            set { Device.SetZoom(this, value); }
        }
        #endregion


        #region --Ops--

        /// <summary>
        /// Set the camera's focus point of interest
        /// </summary>
        [CoreDoc(@"SetFocus", @"SetFocusDiscussion"), Code(@"FocusCamera")]
        public void SetFocus (Vector2 viewportPoint) {
            Device.SetFocus(this, viewportPoint.x, viewportPoint.y);
        }
        
        /// <summary>
        /// Set the camera's frame rate
        /// </summary>
        [CoreDoc(@"SetFramerate")]
        public void SetFramerate (float framerate) {
            Device.SetFramerate(this, framerate);
        }

        /// <summary>
        /// Set the camera's preview resolution
        /// </summary>
        [CoreDoc(@"SetPreviewResolution")]
        public void SetPreviewResolution (Resolution resolution) {
            Device.SetPreviewResolution(this, new Resolution(
                Mathf.Min(resolution.width, Resolution._1920x1080.width),
                Mathf.Min(resolution.height, Resolution._1920x1080.height)
            ));
        }
        
        /// <summary>
        /// Set the camera's photo resolution
        /// </summary>
        [CoreDoc(@"SetPhotoResolution")]
        public void SetPhotoResolution (Resolution resolution) {
            Device.SetPhotoResolution(this, resolution);
        }
        #endregion


        #region ---Typecasting---
        private static IDeviceCamera Device {get {return NatCam.Implementation.Device;}}
        
		public static implicit operator int (DeviceCamera cam) {
			return cam ? cam.index : -1;
		}
        public static implicit operator DeviceCamera (int index) {
            return Cameras.Where(c => c.index == index).FirstOrDefault();
        }
        public static implicit operator bool (DeviceCamera cam) {
            return cam != null;
        }
        #endregion


        #region ---Intializers---

        private readonly int index;

        private DeviceCamera (int i) { index = i; }

        static DeviceCamera () {
            Cameras = new DeviceCamera[WebCamTexture.devices.Length];
            for (int i = 0; i < Cameras.Length; i++) Cameras[i] = new DeviceCamera(i);
            RearCamera = Cameras.FirstOrDefault(c => !c.IsFrontFacing);
            FrontCamera = Cameras.FirstOrDefault(c => c.IsFrontFacing);
        }
        #endregion
        
        
		#region ---Statics---
        [CoreDoc(@"FrontCamera")] public static readonly DeviceCamera FrontCamera;
		[CoreDoc(@"RearCamera")] public static readonly DeviceCamera RearCamera;
        [CoreDoc(@"Cameras"), Code(@"EnableTorch")] public static readonly DeviceCamera[] Cameras; // You shall not touch!
        #endregion
    }
}
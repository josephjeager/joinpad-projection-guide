/* 
*   NatCam Core
*   Copyright (c) 2016 Yusuf Olokoba
*/

namespace NatCamU.Dispatch {

    using UnityEngine;
    using System.Threading;

    public class MainDispatch : IDispatch {

        #region --Client API--

        /// <summary>
        /// Creates a dispatcher that will execute on the main thread
        /// </summary>
        public MainDispatch () : base () {
            DispatchUtility.onFrame += Update;
            Debug.Log("NatCam Dispatch: Initialized main dispatch");
        }

        public override void Release () {
            DispatchUtility.onFrame -= Update;
            base.Release();
        }

        protected override void SafeRelease () {
            Dispatch(Release);
        }

        protected override void Update () {
            thread = Thread.CurrentThread;
            base.Update();
        }
        #endregion
    }
}
/* 
*   NatCam Core
*   Copyright (c) 2016 Yusuf Olokoba
*/

namespace NatCamU.Core {

	using UnityEngine;
	using UnityEngine.UI;
	
	[CoreDoc(@"NatCamBehaviour")]
	public class NatCamBehaviour : MonoBehaviour { // This class is easily subclassed and overriden
		
		[Header("Preview")]
		[CoreDoc(@"PreviewPanel")] public RawImage preview;
		
		[Header("Parameters")]
		[CoreDoc(@"UseFrontCamera")] public bool useFrontCamera;
		[CoreDoc(@"PreviewSize")] public Resolution previewResolution = Resolution._1280x720;
		[CoreDoc(@"PhotoSize")] public Resolution photoResolution = Resolution._1920x1080;
		
		[Header("Debugging")]
		[CoreDoc(@"Verbose")] public bool verbose;

		
		
		// Use this for initialization
		public virtual void Start () {
			// Set verbose mode
			NatCam.Verbose = verbose;
			// Set the active camera
			NatCam.Camera = useFrontCamera ? DeviceCamera.FrontCamera : DeviceCamera.RearCamera;
			// Null checking
			if (!NatCam.Camera) {
				Debug.LogError("Active camera is null. Consider using the " + (useFrontCamera ? "rear" : "front") + " camera");
				return;
			}
			if (!preview) {
				Debug.LogError("Preview RawImage has not been set");
				return;
			}
			// Set the camera's preview resolution
			NatCam.Camera.SetPreviewResolution(previewResolution);
			// Set the camera's photo resolution
			NatCam.Camera.SetPhotoResolution(photoResolution);
			// Play
			NatCam.Play();
			// Register callback for when the preview starts
			// Note that this is a MUST when assigning the preview texture to anything
			NatCam.OnStart += OnStart;
			// Register for preview updates
			NatCam.OnFrame += OnFrame;
		}

		/// <summary>
		/// Method called when the camera preview starts
		/// </summary>
		[CoreDoc(@"PreviewStart")]
		public virtual void OnStart () {
			// Set the preview RawImage texture once the preview starts
			preview.texture = NatCam.Preview;
		}

		/// <summary>
		/// Method called on every frame that the camera preview updates
		/// </summary>
		[CoreDoc(@"PreviewUpdate"), Code(@"CalculateFramerate")]
		public virtual void OnFrame () {}

		protected virtual void OnDisable () {
			// Unregister from NatCam callbacks
			NatCam.OnStart -= OnStart;
			NatCam.OnFrame -= OnFrame;
		}

		/// <summary>
		/// Switch the active camera of the preview
		/// </summary>
		/// <param name="newCamera">Optional. The camera to switch to. An int or a DeviceCamera can be passed here</param>
		[CoreDoc(@"SwitchCamera", @"SwitchCameraDiscussion")]
		public virtual void SwitchCamera (int newCamera = -1) {
			// Select the new camera ID // If no argument is given, switch to the next camera
			newCamera = newCamera < 0 ? (NatCam.Camera + 1) % DeviceCamera.Cameras.Length : newCamera;
			// Set the new active camera
			NatCam.Camera = newCamera;
		}
	}
}
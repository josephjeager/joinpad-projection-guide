﻿using System;
using System.Collections.Generic;
using OpenCVForUnity;
using UnityEngine;

namespace BrainPad.Core
{
    [Serializable]
    public class ArObject
    {
        public GameObject ArGameObjectRoot;
        public Texture2D ReferenceImage;
        public float ReferenceImageWidth;
        public float ReferenceImageHeight;

        public string ReferenceImageName;
    }

    public class ArObjectInternal
    {
        public ArObject ArObject;
        public List<GameObject> Instances = new List<GameObject>();
        private Mat _worldPlane = new Mat(4, 3, CvType.CV_64FC1);

        public void SetupWorldPlane()
        {
            float referenceImageWidthScaled = ArObject.ReferenceImageWidth;
            float referenceImageHeightScaled = ArObject.ReferenceImageHeight;

            _worldPlane.put(0, 0, -referenceImageWidthScaled / 2);
            _worldPlane.put(0, 1, 0f);
            _worldPlane.put(0, 2, referenceImageHeightScaled / 2);

            _worldPlane.put(1, 0, -referenceImageWidthScaled / 2);
            _worldPlane.put(1, 1, 0f);
            _worldPlane.put(1, 2, -referenceImageHeightScaled / 2);

            _worldPlane.put(2, 0, referenceImageWidthScaled / 2);
            _worldPlane.put(2, 1, 0f);
            _worldPlane.put(2, 2, -referenceImageHeightScaled / 2);

            _worldPlane.put(3, 0, referenceImageWidthScaled / 2);
            _worldPlane.put(3, 1, 0f);
            _worldPlane.put(3, 2, referenceImageHeightScaled / 2);
        }

        public Mat GetWorldPlane()
        {
            return _worldPlane;
        }
    }
}
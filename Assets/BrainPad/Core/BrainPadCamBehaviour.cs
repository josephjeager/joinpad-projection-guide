/* 
*   NatCam Core
*   Copyright (c) 2016 Yusuf Olokoba
*/

using NatCamU.Core;
using UnityEngine;
using UnityEngine.UI;
using Resolution = NatCamU.Core.Resolution;

namespace BrainPad.Core {
	[CoreDoc(@"NatCamBehaviour")]
	public class BrainPadCamBehaviour : MonoBehaviour { // This class is easily subclassed and overriden
		
		[Header("Webcam")]
		[CoreDoc(@"PreviewPanel")] public RawImage preview;
		
//		[Header("Parameters")]
		[CoreDoc(@"UseFrontCamera")] public bool useFrontCamera;
		[CoreDoc(@"PreviewSize")] private Resolution previewResolution = Resolution._640x480;
		[CoreDoc(@"PhotoSize")] private Resolution photoResolution = Resolution._640x480;
		
		[Header("Debugging")]
		[CoreDoc(@"Verbose")] private bool verbose;
		
		// Use this for initialization
		public virtual void Start () {
			// Set verbose mode
			NatCam.Verbose = verbose;
			// Set the active camera
			NatCam.Camera = useFrontCamera ? DeviceCamera.FrontCamera : DeviceCamera.RearCamera;
			// Null checking
			if (!NatCam.Camera) {
//				Debug.LogError("Active camera is null. Consider using the " + (useFrontCamera ? "rear" : "front") + " camera");
				Debug.LogError("Active camera is null. Switching to " + (useFrontCamera ? "rear" : "front") + " camera");
				useFrontCamera = !useFrontCamera;
				NatCam.Camera = useFrontCamera ? DeviceCamera.FrontCamera : DeviceCamera.RearCamera;
//				return;
			}

			if (!NatCam.Camera) 
			{
				Debug.LogError("No front / rear camera detected");
				return;
			}

			if (!preview) {
				Debug.LogError("Preview RawImage has not been set");
				return;
			}
			// Set the camera's preview resolution
			NatCam.Camera.SetPreviewResolution(previewResolution);

#if UNITY_ANDROID && !UNITY_EDITOR
			// Set the camera's photo resolution
			NatCam.Camera.SetPhotoResolution(photoResolution);
#endif
			
			NatCam.Camera.SetFramerate(30);
			// Play
			NatCam.Play();
			// Register callback for when the preview starts
			// Note that this is a MUST when assigning the preview texture to anything
			NatCam.OnStart += OnStart;
			// Register for preview updates
			NatCam.OnFrame += OnFrame;
		}

		/// <summary>
		/// Method called when the camera preview starts
		/// </summary>
		[CoreDoc(@"PreviewStart")]
		public virtual void OnStart () {
			// Set the preview RawImage texture once the preview starts
//			preview.texture = NatCam.Preview;
//			preview.texture = NatCam.Preview;
			if (preview != null) preview.texture = NatCam.Preview;
		}

		/// <summary>
		/// Method called on every frame that the camera preview updates
		/// </summary>
		[CoreDoc(@"PreviewUpdate"), Code(@"CalculateFramerate")]
		public virtual void OnFrame () {}

		protected virtual void OnDisable () {
			// Unregister from NatCam callbacks
			NatCam.OnStart -= OnStart;
			NatCam.OnFrame -= OnFrame;
		}

		/// <summary>
		/// Switch the active camera of the preview
		/// </summary>
		/// <param name="newCamera">Optional. The camera to switch to. An int or a DeviceCamera can be passed here</param>
		[CoreDoc(@"SwitchCamera", @"SwitchCameraDiscussion")]
		public virtual void SwitchCamera (int newCamera = -1) {
			// Select the new camera ID // If no argument is given, switch to the next camera
			newCamera = newCamera < 0 ? (NatCam.Camera + 1) % DeviceCamera.Cameras.Length : newCamera;
			// Set the new active camera
			NatCam.Camera = newCamera;
		}
	}
}
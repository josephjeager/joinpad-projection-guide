﻿using UnityEngine;

namespace BrainPad.Core
{
    public class HideOnAwake : MonoBehaviour
    {
        void Awake()
        {
            GetComponent<MeshRenderer>().enabled = false;
        }
    }
}
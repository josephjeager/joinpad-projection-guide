﻿#define OPENCV_API

using System;
using System.Collections.Generic;
using NatCamU.Core;
using OpenCVForUnity;
using UnityEngine;
using UnityEngine.UI;

namespace BrainPad.Core
{
    public static class Utils
    {
        public static void ConfigureArObjectsInternal(List<ArObject> arObjects, List<ArObjectInternal> arObjectsInternal, IBrainPadNative nativeObject)
        {
            for (int i = 0; i < arObjects.Count; i++)
            {
                ArObjectInternal arObjectInternal = new ArObjectInternal();
                arObjectInternal.ArObject = arObjects[i];
                arObjectsInternal.Add(arObjectInternal);
            }

            for (int i = 0; i < arObjectsInternal.Count; i++)
            {
                arObjectsInternal[i].SetupWorldPlane();
                nativeObject.AddNewObjectToDetect(i, arObjectsInternal[i].ArObject.ReferenceImage, arObjectsInternal[i].GetWorldPlane());
            }
        }

        public static void ConfigureCamera(Camera arCamera, RawImage preview, IBrainPadNative nativeObject)
        {
            float cameraWidth;
            float cameraHeight;

            float previewWidth;
            float previewHeight;

            int matchingWidth;

            // Fix preview anchors (in the editor they are set to match container for cosmetic puropses)
            ((RectTransform) preview.gameObject.transform).anchorMin = new Vector2(0.5f, 0.5f);
            ((RectTransform) preview.gameObject.transform).anchorMax = new Vector2(0.5f, 0.5f);
            ((RectTransform) preview.gameObject.transform).pivot = new Vector2(0.5f, 0.5f);

#if UNITY_ANDROID && !UNITY_EDITOR
            if (Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown)
            {
                // If PortraitUpsideDown rotate the preview itself by 180 so NatCam handles it properly
                if (Screen.orientation == ScreenOrientation.PortraitUpsideDown)
                {
                    preview.transform.rotation = Quaternion.Euler(0, 0, -180);
                }
                else
                {
                    preview.transform.rotation = Quaternion.Euler(0, 0, 0);
                }

                // Swap width and height in portrait
                cameraWidth = NatCam.Preview.height;
                cameraHeight = NatCam.Preview.width;

                int screenW = Screen.width;
                int screenH = Screen.height;

                // Sometimes Android screws up on orientation changes and inverts screen sizes,
                // in portrait the shortest edge is the width, no matter what lie the system says
                if (screenH < screenW)
                {
                    // We dont need screenW, it is gonna be recalculated anyway
//                    int tmp = screenW;
//                    screenW = screenH;
//                    screenH = tmp;
                    screenH = screenW;
                }

                matchingWidth = screenH;
                previewWidth = matchingWidth * ((float) NatCam.Preview.width / NatCam.Preview.height);
                previewHeight = matchingWidth;
                ((RectTransform) preview.gameObject.transform).sizeDelta = new Vector2(previewWidth, previewHeight);

                float horizontalFov = NatCam.Camera.HorizontalFOV;
                if (horizontalFov <= 0)
                {
                    horizontalFov = 45; // Fallback value
                }
                
                float hardwareCameraVFov = CalculateVerticalFov(horizontalFov, cameraWidth, cameraHeight);
                arCamera.fieldOfView = horizontalFov;
                nativeObject.SetCameraParams(NatCam.Preview.width, NatCam.Preview.height, hardwareCameraVFov, horizontalFov);
            }
            else
            {
                cameraWidth = NatCam.Preview.width;
                cameraHeight = NatCam.Preview.height;

                int screenW = Screen.width;
                int screenH = Screen.height;

                // Sometimes Android screws up on orientation changes and inverts screen sizes,
                // in landscape the longest edge is the width, no matter what lie the system says
                if (screenW < screenH)
                {
                    // We dont need screenH, it is gonna be recalculated anyway
//                    int tmp = screenW;
                    screenW = screenH;
//                    screenH = tmp;
                }

                matchingWidth = screenW;
                previewWidth = matchingWidth;
                previewHeight = matchingWidth * ((float) NatCam.Preview.height / NatCam.Preview.width);
                ((RectTransform) preview.gameObject.transform).sizeDelta = new Vector2(previewWidth, previewHeight);

                // Unity Camera.fieldOfView is the VERTICAL FOV, so UnityCamera.fieldOfView needs the SCREEN CALCULATED FOV (matching 16/9 aspect ratio), 
                // instead SetCameraParams needs the CAMERA CALCULATED FOV (matching 4/3 aspect ratio), 
                // the only hardware camera parameter we can trust is the HORIZONTAL FOV since the given VERTICAL FOV is always the same 
                // (no matter which resolution the camera preview is set) and always refers to the maximum camera resolution, which is usually 16/9
                
                float horizontalFov = NatCam.Camera.HorizontalFOV;
                if (horizontalFov <= 0)
                {
                    horizontalFov = 60; // Fallback value
                }
                
                float hardwareCameraVFov = CalculateVerticalFov(horizontalFov, cameraWidth, cameraHeight);
                float unityCameraVFov = CalculateVerticalFov(horizontalFov, Screen.width, Screen.height);

                arCamera.fieldOfView = unityCameraVFov;
                nativeObject.SetCameraParams(NatCam.Preview.width, NatCam.Preview.height, horizontalFov, hardwareCameraVFov);
            }
#else
            int screenW = Screen.width;
            int screenH = Screen.height;

            if (screenW > screenH) // Editor landscape
            {
                cameraWidth = NatCam.Preview.width;
                cameraHeight = NatCam.Preview.height;

                matchingWidth = screenW;
                previewWidth = matchingWidth;
                previewHeight = matchingWidth * ((float) NatCam.Preview.height / NatCam.Preview.width);
                ((RectTransform) preview.gameObject.transform).sizeDelta = new Vector2(previewWidth, previewHeight);

                float horizontalFov = 60; // Unity editr does not expose camera horizontal fov, we use an arbitrary vaule
                float hardwareCameraVFov = CalculateVerticalFov(horizontalFov, cameraWidth, cameraHeight);
                float unityCameraVFov = CalculateVerticalFov(horizontalFov, Screen.width, Screen.height);

                arCamera.fieldOfView = unityCameraVFov;
                nativeObject.SetCameraParams(NatCam.Preview.width, NatCam.Preview.height, horizontalFov, hardwareCameraVFov);
            }
            else // Editor portrait
            {
                cameraWidth = NatCam.Preview.width;
                cameraHeight = NatCam.Preview.height;

                matchingWidth = screenH;
                previewWidth = matchingWidth * ((float) NatCam.Preview.width / NatCam.Preview.height);
                previewHeight = matchingWidth;
                ((RectTransform) preview.gameObject.transform).sizeDelta = new Vector2(previewWidth, previewHeight);

                float verticalFov = 45; // Unity editr does not expose camera horizontal fov, we use an arbitrary vaule
                float horizontalFov = CalculateVerticalFov(verticalFov, cameraHeight, cameraWidth);
                arCamera.fieldOfView = verticalFov;
                nativeObject.SetCameraParams(NatCam.Preview.width, NatCam.Preview.height, horizontalFov, verticalFov);
            }

#endif

            arCamera.transform.position = new Vector3(0, 0, 0);
        }

        
//        public static Quaternion QuaternionFromMatrix(Matrix4x4 m) {
//            // Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
////            Quaternion q = new Quaternion();
////            q.w = Mathf.Sqrt( Mathf.Max( 0, 1 + m[0,0] + m[1,1] + m[2,2] ) ) / 2; 
////            q.x = Mathf.Sqrt( Mathf.Max( 0, 1 + m[0,0] - m[1,1] - m[2,2] ) ) / 2; 
////            q.y = Mathf.Sqrt( Mathf.Max( 0, 1 - m[0,0] + m[1,1] - m[2,2] ) ) / 2; 
////            q.z = Mathf.Sqrt( Mathf.Max( 0, 1 - m[0,0] - m[1,1] + m[2,2] ) ) / 2; 
////            q.x *= Mathf.Sign( q.x * ( m[2,1] - m[1,2] ) );
////            q.y *= Mathf.Sign( q.y * ( m[0,2] - m[2,0] ) );
////            q.z *= Mathf.Sign( q.z * ( m[1,0] - m[0,1] ) );
////            return q;
//
//            return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
//                
//        }
//
//        public static Vector3 PositionFromMatrix(Matrix4x4 m)
//        {
//            return m.GetColumn(3);
//        }
        
        public static float CalculateVerticalFov(float horizonfalFov, float cameraWidth, float cameraHeight)
        {
            float hFovRad = horizonfalFov * Mathf.Deg2Rad;
            float cameraHeightAt1 = Mathf.Tan((float) (hFovRad * .5));
            float vfovrad = Mathf.Atan(cameraHeightAt1 * (cameraHeight / cameraWidth)) * 2;
            float vfov = vfovrad * Mathf.Rad2Deg;
            return vfov;
        }

        /// <summary>
        /// Extract translation from transform matrix.
        /// </summary>
        /// <param name="matrix">Transform matrix. This parameter is passed by reference
        /// to improve performance; no changes will be made to it.</param>
        /// <returns>
        /// Translation offset.
        /// </returns>
        public static Vector3 ExtractTranslationFromMatrix(ref Matrix4x4 matrix)
        {
            Vector3 translate;
            translate.x = matrix.m03;
            translate.y = matrix.m13;
            translate.z = matrix.m23;
            return translate;
        }

        /// <summary>
        /// Extract rotation quaternion from transform matrix.
        /// </summary>
        /// <param name="matrix">Transform matrix. This parameter is passed by reference
        /// to improve performance; no changes will be made to it.</param>
        /// <returns>
        /// Quaternion representation of rotation transform.
        /// </returns>
        public static Quaternion ExtractRotationFromMatrix(ref Matrix4x4 matrix)
        {
            Vector3 forward;
            forward.x = matrix.m02;
            forward.y = matrix.m12;
            forward.z = matrix.m22;

            Vector3 upwards;
            upwards.x = matrix.m01;
            upwards.y = matrix.m11;
            upwards.z = matrix.m21;

            return Quaternion.LookRotation(forward, upwards);
        }

        /// <summary>
        /// Extract scale from transform matrix.
        /// </summary>
        /// <param name="matrix">Transform matrix. This parameter is passed by reference
        /// to improve performance; no changes will be made to it.</param>
        /// <returns>
        /// Scale vector.
        /// </returns>
        public static Vector3 ExtractScaleFromMatrix(ref Matrix4x4 matrix)
        {
            Vector3 scale;
            scale.x = new Vector4(matrix.m00, matrix.m10, matrix.m20, matrix.m30).magnitude;
            scale.y = new Vector4(matrix.m01, matrix.m11, matrix.m21, matrix.m31).magnitude;
            scale.z = new Vector4(matrix.m02, matrix.m12, matrix.m22, matrix.m32).magnitude;
            return scale;
        }

        /// <summary>
        /// Extract position, rotation and scale from TRS matrix.
        /// </summary>
        /// <param name="matrix">Transform matrix. This parameter is passed by reference
        /// to improve performance; no changes will be made to it.</param>
        /// <param name="localPosition">Output position.</param>
        /// <param name="localRotation">Output rotation.</param>
        /// <param name="localScale">Output scale.</param>
        public static void DecomposeMatrix(ref Matrix4x4 matrix, out Vector3 localPosition, out Quaternion localRotation, out Vector3 localScale)
        {
            localPosition = ExtractTranslationFromMatrix(ref matrix);
            localRotation = ExtractRotationFromMatrix(ref matrix);
            localScale = ExtractScaleFromMatrix(ref matrix);
        }

        /// <summary>
        /// Set transform component from TRS matrix.
        /// </summary>
        /// <param name="transform">Transform component.</param>
        /// <param name="matrix">Transform matrix. This parameter is passed by reference
        /// to improve performance; no changes will be made to it.</param>
        public static void SetTransformFromMatrix(Transform transform, ref Matrix4x4 matrix)
        {
            transform.localPosition = ExtractTranslationFromMatrix(ref matrix);
            transform.localRotation = ExtractRotationFromMatrix(ref matrix);
            transform.localScale = ExtractScaleFromMatrix(ref matrix);
        }

        public static string matToString(Mat mat, bool round)
        {
            string str = "";
            try
            {
                if (mat != null)
                {
                    str += "Cols: " + mat.cols() + ", Rows: " + mat.rows() + " " + "\n";

                    for (int i = 0; i < mat.rows(); i++)
                    {
                        for (int j = 0; j < mat.cols(); j++)
                        {
                            if (round)
                            {
//                                str +=  String.Format("{0:#,###.##}", mat.get(i, j)[0] ) + " ";
                                str += mat.get(i, j)[0].ToString("####0.00") + " |  ";
                            }
                            else
                            {
                                str += mat.get(i, j)[0] + " ";
                            }
                        }

                        str += "\n";
                    }
                }
                else
                {
                    str += "Matrix is NULL";
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                str += "Some odd error occurred: " + e;
            }

            return str;
        }
    }
}
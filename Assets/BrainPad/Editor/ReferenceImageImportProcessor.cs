﻿using BrainPad.Obj2DLocalization.Editor;
using UnityEditor;

namespace BrainPad.Editor
{
    public class AssetProcessing : AssetPostprocessor
    {
        public void OnPreprocessTexture()
        {
            if (Obj2DLocalizationNewImageTargetWizard.BrainPadImport)
            {
                TextureImporter textureImporter = assetImporter as TextureImporter;
                if (textureImporter != null)
                {
                    textureImporter.mipmapEnabled = false;
                    textureImporter.maxTextureSize = 2048;
                    TextureImporterSettings textureImporterSettings = new TextureImporterSettings();
                    textureImporter.ReadTextureSettings(textureImporterSettings);
//                textureImporterSettings.textureFormat = TextureImporterFormat.RGBA32;
                    textureImporterSettings.npotScale = TextureImporterNPOTScale.None;
                    textureImporterSettings.readable = true;
                    textureImporter.SetTextureSettings(textureImporterSettings);
                }
            }
        }
    }
}
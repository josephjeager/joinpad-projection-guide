﻿using System;
using System.IO;
using BrainPad.Core;
using UnityEditor;
using UnityEngine;
using Utils = BrainPad.Editor.Utils;

namespace BrainPad.Obj2DLocalization.Editor
{
    public class Obj2DLocalizationNewImageTargetWizard : ScriptableWizard
    {
        public static Boolean BrainPadImport;

        private string _referenceImage;
        private float _width = 1.0f;

        private float _height = 1.0f;
//    private Enums.MeasureUnits _measureUnit;

        private Texture2D _texture;
        private string _path;

        private float _imageRatio = 1;

        private readonly int widthSegments = 1;
        private readonly int heightSegments = 1;

        static Camera _cam;
        static Camera _lastUsedCam;

        [MenuItem("BrainPad/Object 2D Localization/New Image Target")]
        public static void CreateWizard()
        {
            DisplayWizard("New Image Target", typeof(Obj2DLocalizationNewImageTargetWizard));
        }

        private void OnGUI()
        {
            if (GUILayout.Button("Select Reference Image"))
            {
                _path = EditorUtility.OpenFilePanel("Choose texture", "", "png");
                if (_path.Length != 0)
                {
                    var www = new WWW("file:///" + _path);
                    _texture = new Texture2D(1, 1);
                    www.LoadImageIntoTexture(_texture);

                    _imageRatio = (float) _texture.width / _texture.height;
                    if (_imageRatio < 1)
                    {
                        _width = 1;
                        _height = (float) Math.Round((double) _width / _imageRatio, 2);
                    }
                    else
                    {
                        _height = 1;
                        _width = (float) Math.Round((double) _height * _imageRatio, 2);
                    }
                }
            }

            if (_texture != null)
            {
                GUI.enabled = false;
                EditorGUILayout.TextField("Selected Image", _referenceImage);
                GUI.enabled = true;

                EditorGUI.BeginChangeCheck();
                _width = EditorGUILayout.FloatField("Physical Width", _width);
                if (EditorGUI.EndChangeCheck())
                {
                    if (_width < 0)
                    {
                        _width = 0;
                    }

                    _height = (float) Math.Round((double) _width / _imageRatio, 2);
                }

                EditorGUI.BeginChangeCheck();
                _height = EditorGUILayout.FloatField("Physical Height", _height);
                if (EditorGUI.EndChangeCheck())
                {
                    if (_height < 0)
                    {
                        _height = 0;
                    }

                    _width = (float) Math.Round((double) _height * _imageRatio, 2);
                }

//            _measureUnit = (Enums.MeasureUnits) EditorGUILayout.EnumPopup("Measure Unit", _measureUnit);
                _referenceImage = Path.GetFileName(_path);

                if (GUILayout.Button("Add Image Target"))
                {
                    if (Utils.CheckConfiguration())
                    {
                        InitFolders();

                        try
                        {
                            if (_texture.format != TextureFormat.ARGB32 && _texture.format != TextureFormat.RGBA32 && _texture.format != TextureFormat.RGB24 && _texture.format != TextureFormat.Alpha8)
                            {
                                EditorUtility.DisplayDialog("Error: wrong image target fromat", "The image target should be 32bit RGB with alpha, 24bit RGB or 8bit GreyScale", "Ok");
                                return;
                            }
                            
                            FileUtil.CopyFileOrDirectory(_path, "Assets/BrainPad/Generated/Textures/" + _referenceImage);

                            BrainPadImport = true;
                            AssetDatabase.Refresh();
                            BrainPadImport = false;

                            if (CreateImageTarget())
                            {
                                Close();
//                                GUIUtility.ExitGUI(); // throws exception
                            }
                        }
                        catch (Exception)
                        {
                            EditorUtility.DisplayDialog("Error: image target already exists", "There is already an image target named \"" + _referenceImage + "\" please pick an image target with a different name.\n\nIf you manually deleted a previous image target named \"" + _referenceImage + "\" make sure to also manually delete:\n\nAssets/BrainPad/Generated/Textures/" + _referenceImage + "\nAssets/BrainPad/Generated/Meshes/" + Path.GetFileNameWithoutExtension(_referenceImage) + ".asset\nAssets/BrainPad/Generated/Materials/" + Path.GetFileNameWithoutExtension(_referenceImage) + ".mtl", "Ok");
                        }
                    }
                }
            }
        }

        bool CreateImageTarget()
        {
            bool success = Utils.CheckScene();

            GameObject referencePlane = new GameObject();
            string fileName = Path.GetFileNameWithoutExtension(_referenceImage);
            if (fileName != null)
            {
                referencePlane.name = fileName;
            }

            referencePlane.transform.position = Vector3.zero;
            Vector2 anchorOffset = Vector2.zero;
            MeshFilter meshFilter = (MeshFilter) referencePlane.AddComponent(typeof(MeshFilter));
            referencePlane.AddComponent(typeof(MeshRenderer));

            string planeAssetName = fileName + ".asset";

            // Always create a new mesh
//        Mesh mesh = (Mesh) AssetDatabase.LoadAssetAtPath("Assets/BrainPad/Meshes/" + planeAssetName, typeof(Mesh));
//        if (mesh == null)
//        {
            Mesh mesh = new Mesh();
            mesh.name = referencePlane.name;

            int hCount2 = widthSegments + 1;
            int vCount2 = heightSegments + 1;
            int numTriangles = widthSegments * heightSegments * 6;
//            if (twoSided) {
            numTriangles *= 2;
//            }
            int numVertices = hCount2 * vCount2;

            Vector3[] vertices = new Vector3[numVertices];
            Vector2[] uvs = new Vector2[numVertices];
            int[] triangles = new int[numTriangles];
            Vector4[] tangents = new Vector4[numVertices];
            Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);

            int index = 0;
            float uvFactorX = 1.0f / widthSegments;
            float uvFactorY = 1.0f / heightSegments;
            float scaleX = _width / widthSegments;
            float scaleY = _height / heightSegments;
            for (float y = 0.0f; y < vCount2; y++)
            {
                for (float x = 0.0f; x < hCount2; x++)
                {
                    vertices[index] = new Vector3(x * scaleX - _width / 2f - anchorOffset.x, 0.0f, y * scaleY - _height / 2f - anchorOffset.y);
                    tangents[index] = tangent;
                    uvs[index++] = new Vector2(x * uvFactorX, y * uvFactorY);
                }
            }

            index = 0;
            for (int y = 0; y < heightSegments; y++)
            {
                for (int x = 0; x < widthSegments; x++)
                {
                    triangles[index] = y * hCount2 + x;
                    triangles[index + 1] = (y + 1) * hCount2 + x;
                    triangles[index + 2] = y * hCount2 + x + 1;

                    triangles[index + 3] = (y + 1) * hCount2 + x;
                    triangles[index + 4] = (y + 1) * hCount2 + x + 1;
                    triangles[index + 5] = y * hCount2 + x + 1;
                    index += 6;
                }

//                if (twoSided) {
                // Same tri vertices with order reversed, so normals point in the opposite direction
                for (int x = 0; x < widthSegments; x++)
                {
                    triangles[index] = y * hCount2 + x;
                    triangles[index + 1] = y * hCount2 + x + 1;
                    triangles[index + 2] = (y + 1) * hCount2 + x;

                    triangles[index + 3] = (y + 1) * hCount2 + x;
                    triangles[index + 4] = y * hCount2 + x + 1;
                    triangles[index + 5] = (y + 1) * hCount2 + x + 1;
                    index += 6;
                }

//                }
            }

            mesh.vertices = vertices;
            mesh.uv = uvs;
            mesh.triangles = triangles;
            mesh.tangents = tangents;
            mesh.RecalculateNormals();

            AssetDatabase.CreateAsset(mesh, "Assets/BrainPad/Generated/Meshes/" + planeAssetName);
//        }

            meshFilter.sharedMesh = mesh;
            mesh.RecalculateBounds();

//        if (addCollider)
//            plane.AddComponent(typeof(BoxCollider));

            Material tempMaterial = new Material(Shader.Find("Unlit/Transparent"));

            string materialName = fileName + ".mtl";
            tempMaterial.mainTexture = (Texture2D) AssetDatabase.LoadAssetAtPath("Assets/BrainPad/Generated/Textures/" + _referenceImage, typeof(Texture2D));
            AssetDatabase.CreateAsset(tempMaterial, "Assets/BrainPad/Generated/Materials/" + materialName);

            referencePlane.GetComponent<MeshRenderer>().material = tempMaterial;
            referencePlane.AddComponent<HideOnAwake>();

            AssetDatabase.SaveAssets();

            ArObject arObject = new ArObject();
            arObject.ArGameObjectRoot = referencePlane;
            arObject.ReferenceImage = (Texture2D) AssetDatabase.LoadAssetAtPath("Assets/BrainPad/Generated/Textures/" + _referenceImage, typeof(Texture2D));
            arObject.ReferenceImageWidth = _width;
            arObject.ReferenceImageHeight = _height;
            arObject.ReferenceImageName = _referenceImage;

            // Add example GameObject
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.parent = arObject.ArGameObjectRoot.transform;
            cube.transform.localPosition = new Vector3(0, 0.5f, 0);

            GameObject processorUnit = GameObject.Find("ProcessorUnit");
            processorUnit.GetComponent<Obj2DLocalization>().ArObjects.Add(arObject);

//        EditorSceneManager.SaveScene(SceneManager.GetActiveScene());
            return success;
        }

        private void InitFolders()
        {
            if (!AssetDatabase.IsValidFolder("Assets/BrainPad/Generated"))
            {
                AssetDatabase.CreateFolder("Assets/BrainPad", "Generated");
            }

            if (!AssetDatabase.IsValidFolder("Assets/BrainPad/Generated/Meshes"))
            {
                AssetDatabase.CreateFolder("Assets/BrainPad/Generated", "Meshes");
            }

            if (!AssetDatabase.IsValidFolder("Assets/BrainPad/Generated/Materials"))
            {
                AssetDatabase.CreateFolder("Assets/BrainPad/Generated", "Materials");
            }

            if (!AssetDatabase.IsValidFolder("Assets/BrainPad/Generated/Textures"))
            {
                AssetDatabase.CreateFolder("Assets/BrainPad/Generated", "Textures");
            }
        }
    }
}
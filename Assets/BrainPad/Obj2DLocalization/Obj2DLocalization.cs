﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using BrainPad.Core;
using NatCamU.Core;
using OpenCVForUnity;
using UnityEngine;
using Object = System.Object;
using Utils = BrainPad.Core.Utils;

namespace BrainPad.Obj2DLocalization
{
    public class Obj2DLocalization : BrainPadCamBehaviour
    {
        [Header("Augmented Reality")] public Camera UnityCamera;

        [HideInInspector] public List<ArObject> ArObjects;

        private static Obj2DLocalization _instance;
        private NativeObj2DLocalization _nativeObject;

        private bool _isTracking;
        private List<KeyValuePair<int, Matrix4x4>> _foundObjects;
        private Mat _frameMatrix;

        private Mat _frameMatrixPreprocessed;

        private Mat _poseMatrixes;
        private Matrix4x4 _rtMatrix4X4;
        private List<ArObjectInternal> _arObjectsInternal;

        private bool _initialized;
        private static Object _lock = new Object();

        private Texture2D _texture;
        private Color32[] _colors;
        private const TextureFormat Format =
#if UNITY_IOS && !UNITY_EDITOR
            TextureFormat.BGRA32;
#else
            TextureFormat.RGBA32;
#endif

        void Awake()
        {
            for (int i = 0; i < ArObjects.Count; i++)
            {
                ArObjects[i].ArGameObjectRoot.SetActive(false);
            }

            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
        }

        public override void OnStart()
        {
            base.OnStart();

            ReleaseObjects();

            _foundObjects = new List<KeyValuePair<int, Matrix4x4>>();
            _arObjectsInternal = new List<ArObjectInternal>();
            _nativeObject = new NativeObj2DLocalization();
            _poseMatrixes = new Mat(5, 4, CvType.CV_64FC1);
            lock (_lock)
            {
                _frameMatrixPreprocessed = new Mat(NatCam.Preview.height, NatCam.Preview.width, CvType.CV_8UC1);
            }

            Utils.ConfigureCamera(UnityCamera, preview, _nativeObject);
            _nativeObject.Configure();
            Utils.ConfigureArObjectsInternal(ArObjects, _arObjectsInternal, _nativeObject);

            _nativeObject.Init();
            _nativeObject.Start();

            preview.color = Color.white;

            _initialized = true;
        }

        public override void OnFrame()
        {
//#if NATCAM_PRO || NATCAM_PROFESSIONAL
            if (!NatCam.PreviewMatrix(ref _frameMatrix)) return;
            _colors = _colors ?? new Color32[_frameMatrix.cols() * _frameMatrix.rows()];
            _texture = _texture ?? new Texture2D(_frameMatrix.cols(), _frameMatrix.rows(), Format, false, false);
            if (_texture.width != _frameMatrix.cols() || _texture.height != _frameMatrix.rows())
                _texture.Resize(_frameMatrix.cols(), _frameMatrix.rows());

            OpenCVForUnity.Core.flip(_frameMatrix, _frameMatrix, 0);
            if (_initialized)
            {
                lock (_lock)
                {
                    Imgproc.cvtColor(_frameMatrix, _frameMatrixPreprocessed, Imgproc.COLOR_RGBA2GRAY);
                }
            }

//            New logic, now ProcessFrameWork() is executed on Unity main thread (Update())
//            ProcessFrameWork();

//            Not needed anymore
//            if (_isTracking)
//            {
//            OpenCVForUnity.Utils.matToTexture2D(_frameMatrix, _texture, _colors);
            OpenCVForUnity.Utils.fastMatToTexture2D(_frameMatrix, _texture);
            preview.texture = _texture;
//            }
//#endif
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                ReleaseObjects();
            }
        }

        void ProcessFrameWork()
        {
            if (_initialized)
            {
                lock (_lock)
                {
                    _nativeObject.ProcessFrame(_frameMatrixPreprocessed);
                }

                _nativeObject.GetRTMatrixes(_poseMatrixes);
                _foundObjects.Clear();

                for (int i = 0; i < _poseMatrixes.rows() / 5; i++)
                {
                    int maskedObjectId = (int) _poseMatrixes.get(i * 5, 0)[0];
                    int objectId = maskedObjectId / 1000;

                    Matrix4x4 rtMatrix4X4 = new Matrix4x4();
                    rtMatrix4X4.SetRow(0, new Vector4((float) _poseMatrixes.get(i * 5 + 1, 0)[0], (float) _poseMatrixes.get(i * 5 + 1, 1)[0], (float) _poseMatrixes.get(i * 5 + 1, 2)[0], (float) _poseMatrixes.get(i * 5 + 1, 3)[0]));
                    rtMatrix4X4.SetRow(1, new Vector4((float) _poseMatrixes.get(i * 5 + 2, 0)[0], (float) _poseMatrixes.get(i * 5 + 2, 1)[0], (float) _poseMatrixes.get(i * 5 + 2, 2)[0], (float) _poseMatrixes.get(i * 5 + 2, 3)[0]));
                    rtMatrix4X4.SetRow(2, new Vector4((float) _poseMatrixes.get(i * 5 + 3, 0)[0], (float) _poseMatrixes.get(i * 5 + 3, 1)[0], (float) _poseMatrixes.get(i * 5 + 3, 2)[0], (float) _poseMatrixes.get(i * 5 + 3, 3)[0]));
                    rtMatrix4X4.SetRow(3, new Vector4(0, 0, 0, 1));

                    _foundObjects.Add(new KeyValuePair<int, Matrix4x4>(objectId, rtMatrix4X4));
                }

                List<IGrouping<int, KeyValuePair<int, Matrix4x4>>> foundObjetsGroups = _foundObjects.GroupBy(i => i.Key).ToList();
                for (int i = 0; i < _arObjectsInternal.Count; i++)
                {
                    IGrouping<int, KeyValuePair<int, Matrix4x4>> foundObject = foundObjetsGroups.FirstOrDefault(foundObjetsGroup => foundObjetsGroup.Key == i);
                    if (foundObject == null)
                    {
                        for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= 0; j--)
                        {
//                            Destroy(_arObjectsInternal[i].Instances[j]);
//                            _arObjectsInternal[i].Instances.RemoveAt(j);
                            _arObjectsInternal[i].Instances[j].SetActive(false);
                        }
                    }
                    else
                    {
                        // Add missing instances
                        while (_arObjectsInternal[i].Instances.Count < foundObject.Count())
                        {
                            _arObjectsInternal[i].Instances.Add(Instantiate(_arObjectsInternal[i].ArObject.ArGameObjectRoot));
                        }

//                        // Remove exceeding instances
//                        for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= foundObject.Count(); j--)
//                        {
////                            Destroy(_arObjectsInternal[i].Instances[j]);
////                            _arObjectsInternal[i].Instances.RemoveAt(j);
//                            _arObjectsInternal[i].Instances[j].SetActive(false);
//                        }

                        // Hide exceeding instances
                        for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= foundObject.Count(); j--)
                        {
//                            Destroy(_arObjectsInternal[i].Instances[j]);
//                            _arObjectsInternal[i].Instances.RemoveAt(j);
                            _arObjectsInternal[i].Instances[j].SetActive(false);
                        }

                        // New positions to Gameobjects
                        List<KeyValuePair<int, Matrix4x4>> foundObjectList = foundObject.ToList();
                        for (int sharedIndex = 0; sharedIndex < _arObjectsInternal[i].Instances.Count && sharedIndex < foundObjectList.Count; sharedIndex++)
                        {
                            GameObject gameObjectInstance = _arObjectsInternal[i].Instances[sharedIndex];
                            Matrix4x4 matrix4X4 = foundObjectList[sharedIndex].Value;

                            // Flip x and y axes for PortraitUpsideDown
                            if (Screen.orientation == ScreenOrientation.PortraitUpsideDown)
                            {
                                Vector3 localPosition = Utils.ExtractTranslationFromMatrix(ref matrix4X4);
                                gameObjectInstance.transform.localPosition = new Vector3(-localPosition.x, -localPosition.y, localPosition.z);
                            }
                            else
                            {
                                gameObjectInstance.transform.localPosition = Utils.ExtractTranslationFromMatrix(ref matrix4X4);
                            }

                            gameObjectInstance.transform.localRotation = Utils.ExtractRotationFromMatrix(ref matrix4X4);
                            gameObjectInstance.SetActive(true);
                        }
                    }
                }

                _isTracking = _foundObjects.Count > 0;

//            Not needed anymore
//            if (!_isTracking && preview.texture != NatCam.Preview)
//            {
//                preview.texture = NatCam.Preview;
//            }
            }
        }

        void Update()
        {
            ProcessFrameWork();
        }

        void OnDestroy()
        {
            ReleaseObjects();
            _instance = null;
        }

        void ReleaseObjects()
        {
            _initialized = false;
            if (_arObjectsInternal != null)
            {
                for (int i = 0; i < _arObjectsInternal.Count; i++)
                {
                    for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= 0; j--)
                    {
                        DestroyImmediate(_arObjectsInternal[i].Instances[j]);
                        _arObjectsInternal[i].Instances.RemoveAt(j);
                    }
                }
            }

            if (_nativeObject != null)
            {
                _nativeObject.Destroy();
                _nativeObject = null;
            }

            // Hide artifacts generated by screen rotation 
            if (preview != null && preview.texture != null && _frameMatrix != null)
            {
                preview.texture = null;
                preview.color = Color.black;
            }

            if (_poseMatrixes != null)
            {
                _poseMatrixes.release();
                _poseMatrixes = null;
            }

            if (_frameMatrixPreprocessed != null)
            {
                lock (_lock)
                {
                    _frameMatrixPreprocessed.release();
                    _frameMatrixPreprocessed = null;
                }
            }

            _isTracking = false;
        }

        public class NativeObj2DLocalization : IBrainPadNative
        {
            private readonly IntPtr _nativeObjAddr;

            public NativeObj2DLocalization()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                _nativeObjAddr = nativeObj2DLocalization_Create();
//#endif
            }

            public void SetCameraParams(int cameraWidth, int cameraHeight, float horizontalViewAngle, float verticalViewAngle)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeObj2DLocalization_SetCameraParams(_nativeObjAddr, cameraWidth, cameraHeight, horizontalViewAngle, verticalViewAngle);
//#endif
            }

            public void AddNewObjectToDetect(int objectId, Texture2D referenceImage, Mat worldPlane)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                Mat referenceImageMat;
                Mat referenceImageGreyMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC1);
                if (referenceImage.format == TextureFormat.RGBA32)
                {
                    referenceImageMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC4);
                    OpenCVForUnity.Utils.fastTexture2DToMat(referenceImage, referenceImageMat);
                    Imgproc.cvtColor(referenceImageMat, referenceImageGreyMat, Imgproc.COLOR_RGBA2GRAY);
                } 
                else if (referenceImage.format == TextureFormat.RGB24)
                {
                    referenceImageMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC3);
                    OpenCVForUnity.Utils.fastTexture2DToMat(referenceImage, referenceImageMat);
                    Imgproc.cvtColor(referenceImageMat, referenceImageGreyMat, Imgproc.COLOR_RGB2GRAY);
                } 
                else if (referenceImage.format == TextureFormat.Alpha8)
                {
                    referenceImageMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC1);
                    OpenCVForUnity.Utils.fastTexture2DToMat(referenceImage, referenceImageMat);
                }
                else
                {
                    Debug.LogError("Error: wrong image target fromat, the image target should be 32bit RGB with alpha, 24bit RGB or 8bit GreyScale");
//                    EditorUtility.DisplayDialog("Error: wrong reference image", "Your project is correctly setup to run BrainPad", "Ok");
                    return;
                }

//                Old handling with OpenCVForUnity.Utils.texture2DToMat
//                Mat referenceImageMat; = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC4);
//                OpenCVForUnity.Utils.texture2DToMat(referenceImage, referenceImageMat);
//                Mat referenceImageGreyMat = new Mat(referenceImageMat.rows(), referenceImageMat.cols(), CvType.CV_8UC1);
//                Imgproc.cvtColor(referenceImageMat, referenceImageGreyMat, Imgproc.COLOR_RGBA2GRAY);

                nativeObj2DLocalization_AddNewObjectToDetect(_nativeObjAddr, objectId, referenceImageGreyMat.nativeObj, worldPlane.nativeObj);
//#endif
            }

            public void Init()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeObj2DLocalization_Init(_nativeObjAddr);
//#endif
            }

            public void Configure()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeObj2DLocalization_Configure(_nativeObjAddr);
//#endif
            }

            public void Start()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeObj2DLocalization_Start(_nativeObjAddr);
//#endif
            }

            public void Stop()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeObj2DLocalization_Stop(_nativeObjAddr);
//#endif
            }

            public int ProcessFrame(Mat imageRgba)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeObj2DLocalization_ProcessFrame(_nativeObjAddr, imageRgba.nativeObj);
//#else
//            return 0;
//#endif
            }

            public int GetRTMatrixes(Mat poseMatrixOut)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeObj2DLocalization_GetRTMatrixes(_nativeObjAddr, poseMatrixOut.nativeObj);
//#else
//            return 0;
//#endif
            }

            public void Destroy()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeObj2DLocalization_Destroy(_nativeObjAddr);
//#endif
            }

//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
//#if UNITY_ANDROID && !UNITY_EDITOR
//            const string LIBNAME = "Obj2DLocalization";
//#else
//        const string LIBNAME = "__Internal";
//        const string LIBNAME = "Sapphire";
//#endif
            
            // LibName is now Sapphire for all architectures
            const string LIBNAME = "Sapphire";
            
            [DllImport(LIBNAME)]
            private static extern IntPtr nativeObj2DLocalization_Create();

            [DllImport(LIBNAME)]
            private static extern void nativeObj2DLocalization_Destroy(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeObj2DLocalization_AddNewObjectToDetect(IntPtr nativeObjAddr, int objectId, IntPtr queryFrame, IntPtr worldPlane);

            [DllImport(LIBNAME)]
            private static extern int nativeObj2DLocalization_Init(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeObj2DLocalization_SetCameraParams(IntPtr nativeObjAddr, int width, int height, float hFov, float vFov);

            [DllImport(LIBNAME)]
            private static extern int nativeObj2DLocalization_Configure(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeObj2DLocalization_Start(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeObj2DLocalization_Stop(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeObj2DLocalization_ProcessFrame(IntPtr nativeObjAddr, IntPtr rImage);

//        [DllImport(LIBNAME)]
//        private static extern int nativeGetHomographies(IntPtr nativeObjAddr, IntPtr rHomographiesOut);

            [DllImport(LIBNAME)]
            private static extern int nativeObj2DLocalization_GetRTMatrixes(IntPtr nativeObjAddr, IntPtr rRTMatrixesOut);

            [DllImport(LIBNAME)]
            private static extern int nativeObj2DLocalization_GetReferenceObjects(IntPtr nativeObjAddr, IntPtr rRefObjsOut);

            [DllImport(LIBNAME)]
            private static extern int nativeObj2DLocalization_GetImage(IntPtr nativeObjAddr, IntPtr rImageOut);

            [DllImport(LIBNAME)]
            private static extern int nativeObj2DLocalization_SetImage(IntPtr nativeObjAddr, IntPtr rImage);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using BrainPad.Core;
using NatCamU.Core;
using OpenCVForUnity;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Net.Mime;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Metadata;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Object = System.Object;
using Utils = BrainPad.Core.Utils;





namespace BrainPad.ProjectionGuide
{
    
    [Serializable]
    public class RTMatrix
    {
        public int id;
        public double r00,r01,r02,r10,r11,r12,r20,r21,r22,r30,r31,r32;
        public double t00,t10,t20,t30;            
    }

    [Serializable]
    public class AreaCoords
    {
        public int id;
        public double x0, y0, x1, y1, x2, y2, x3, y3;
    }
        
    [Serializable]
    public class AreasCoords
    {
        public List<AreaCoords> areasCoords=new List<AreaCoords>();
        public List<RTMatrix> areasRTs=new List<RTMatrix>();
    }

    
    public class WorkingArea
    {
        public int areaID;
        public int markerID;
        private int status;
        public int objectID;
        
        public int SetStatus(int status)
        {
            this.status = status;
            return this.status;
        }
    }

    public class ToolArea
    {
        public int areaID;
        public int markerID;
        private int status;

        public int SetStatus(int status)
        {
            this.status = status;
            return this.status;
        }
    }
    
    public class ButtonArea
    {
        public int areaID;
        public int markerID;
        private int status;
        private int timeCounter;
        private static int TIME_PRESSED = 40;

        public int SetStatus(int status)
        {
            if (status == ProjectionGuide.TOOLAREA_FREE)
            {
                this.status = status;
                ResetTimeCounter();
            }
            else if (status == ProjectionGuide.TOOLAREA_BUSY)
            {
                int timeElapsed = GetTimeCounter();
                if (timeElapsed > TIME_PRESSED)
                {
                    this.status = ProjectionGuide.TOOLAREA_BUSY;
                    
                }
                else
                {
                    this.status = ProjectionGuide.TOOLAREA_FREE;
                    IncrementTimeCounter();
                }
            }
            return (int)(((double)GetTimeCounter()/(double)TIME_PRESSED)*100.0);
        }
        
        public int GetPercent()
        {
            return (int)(((double)GetTimeCounter()/(double)TIME_PRESSED)*100.0);
        }

        public int GetTimeCounter()
        {
            return timeCounter;
        }
        
        public int IncrementTimeCounter()
        {
            timeCounter ++;
            return timeCounter;
        }
        
        public int ResetTimeCounter()
        {
            timeCounter = 0;
            return timeCounter;
        }
    }
    
    
        
    
    
    public class ProjectionGuide : MonoBehaviour//BrainPadCamBehaviour
    {
        [Header("Augmented Reality")] public Camera UnityCamera;

        public GameObject MarkersPlane10;
        public GameObject MarkersPlane11;
        public GameObject MarkersPlane12;
        public GameObject MarkersPlane13;
        public GameObject MarkersPlane14;

        public GameObject MainMenu;
        public GameObject SetupMenu;
        public GameObject RestartMenu;
        
        public GameObject WorkBenchPlane;
        public GameObject WorkingArea_free;        
        public GameObject WorkingArea_busy;       
        public GameObject WorkingArea_undef;   
        public GameObject ObjectDetected;

        public GameObject ObjectTextInfo;
        public GameObject ProcessStateTextInfo;
        
        public GameObject SensibleArea1_free;
        public GameObject SensibleArea2_free;
        public GameObject SensibleArea3_free;
        public GameObject SensibleArea4_free;
        public GameObject SensibleArea1_busy;
        public GameObject SensibleArea2_busy;
        public GameObject SensibleArea3_busy;
        public GameObject SensibleArea4_busy;
        public GameObject SensibleArea1_undef;
        public GameObject SensibleArea2_undef;
        public GameObject SensibleArea3_undef;
        public GameObject SensibleArea4_undef;
        public GameObject SensibleArea3_pressed75;
        public GameObject SensibleArea4_pressed75;
        public GameObject SensibleArea3_pressed50;
        public GameObject SensibleArea4_pressed50;
        public GameObject SensibleArea3_pressed25;
        public GameObject SensibleArea4_pressed25;
        public GameObject SensibleArea3_pressed0;
        public GameObject SensibleArea4_pressed0;
        
        public GameObject TextArea1_A;
        public GameObject TextArea1_B;
        public GameObject TextArea1_C;
        public GameObject TextArea1_D;
        public GameObject TextArea2_A;

//        public GameObject ButtonZoomAdjustStart;
//        public GameObject ButtonZoomAdjustStop;
//        public GameObject ButtonUnCalibrate;
//        public GameObject ButtonUnConfigureWorkbench;
//        public GameObject ButtonObjectDetectionAndTrackingStart;



        private ToolArea toolArea1;
        private ToolArea toolArea2;
        private ButtonArea buttonArea1;
        private ButtonArea buttonArea2;



        private WorkingArea workingArea;
        
        
//        public GameObject Object1;

        private static int PROCESS_STATE_UNDEF = 0;
        private static int PROCESS_STATE_FINDOBJ = 1;
        private static int PROCESS_STATE_OBJDETECTED = 2;
        private static int PROCESS_STATE_FINDTOOLS = 3;
        private static int PROCESS_STATE_TOOLSDETECTED = 4;
        private static int PROCESS_STATE_PUSHBUTTON = 5;
//        private static int PROCESS_STATE_PUSHBUTTON_0 = 5;
//        private static int PROCESS_STATE_PUSHBUTTON_1 = 6;
//        private static int PROCESS_STATE_PUSHBUTTON_2 = 7;
//        private static int PROCESS_STATE_PUSHBUTTON_3 = 8;
//        private static int PROCESS_STATE_PUSHBUTTON_4 = 9;
        private static int PROCESS_STATE_COMPLETED = 10;
        
        

        private static int STATE_CHECK = 0;
        private static int STATE_UNCALIBRATED = 1;
        private static int STATE_CALIBRATE = 2;
        private static int STATE_CALIBRATED = 3;
        private static int STATE_CONFIGURE_WORKBENCH = 4;
        private static int STATE_CONFIGURED_WORKBENCH		=5;
        private static int STATE_UNCONFIGURED_WORKBENCH		=6;
        private static int STATE_READY						=7;
        private static int STATE_OBJDETECTANDTRACK			=10;
        private static int STATE_MARKERDETECT				=20;

        private static int WORKINGAREAID = 1;
        private static int TOOLAREA1ID = 2;
        private static int TOOLAREA2ID = 3;
        private static int BUTTON1AREAID = 11;
        private static int BUTTON2AREAID = 12;

        public static int TOOLAREA_UNDEF = -1;
        public static int TOOLAREA_FREE = 0;
        public static int TOOLAREA_BUSY = 1;

        private static int MAX_TIME_ZOOM_ADJUST = 60 * 30; //30 seconds
        private static int MAX_TIME_ODT_START = 60;//60 * 3;
        
        
        private static int MAX_NUM_MARKERS = 5;

        private static int MAX_NUM_SENSIBLE_AREAS = 10;
        private static int MAX_NUM_WORKING_AREA = 1;

        private static int STEP_WAIT_TIME = 40;
        private static int RESTART_WAIT_TIME = 100;

        private static int RESTART_MENU_ID = 1;
        private static int MAIN_MENU_ID = 2;
        private static int SETUP_MENU_ID = 3;
        
        private int _processStatus = PROCESS_STATE_UNDEF;
        

        private static bool _objectDetectionAndTrackingResultIsReady = false;
        
        private int _waitForZoomAdjustment=0;
        private int _waitObjectDetectionAndTrackingStart=0;
        
        public List<ArObject> ArObjects;

        public static ProjectionGuide _instance;
        private NativeProjectionGuide _nativeObject;

        public bool _isTracking;
        private List<KeyValuePair<int, Matrix4x4>> _foundObjects;
        private List<KeyValuePair<int, Matrix4x4>> _foundObjectsWB;
        private List<KeyValuePair<int, Matrix4x4>> _foundMarkers;
        private Mat _frameMatrix;

        private Mat _frameMatrixPreprocessed;

        private Mat _poseMatrix;
        private Mat _poseMatrixWB;
        private Mat _poseMarkersMatrixes;
        private Mat _poseMarkersMatrixesTmp;
        
        private Mat _rtP_C;


        private int _idMenuActivated = MAIN_MENU_ID;
        

        private bool _startObjectDetectionAndTracking = false;
        
        private bool _calibrationStarted = false;
        private bool _configureWorkBenchParamsLoaded = false;
        private bool _calibrationParamsLoaded = false;
        
        private string _calibDataFilename;
        private string _configWorkBenchDataFilename;
        
        
        private Matrix4x4 _rtMatrix4X4;
        private List<ArObjectInternal> _arObjectsInternal;

        private bool _initialized;
        private static Object _lock = new Object();

        private List<int> _markerIds=new List<int>();
        private List<int> _objsIds=new List<int>();
        
        private Mat _frameMatrixRGBPreprocessed;

        private List<KeyValuePair<int, Mat>> _xP_Ms;
        private List<KeyValuePair<int,int>> _sensibleAreas;
        private List<KeyValuePair<int, Mat>> _sensibleAreasCoords;
        private List<KeyValuePair<int, Mat>> _sensibleAreasRTs;
        private List<KeyValuePair<int, Mat>> _sensibleAreasRTsWRTProjector;

//        private List<KeyValuePair<int, Mat>> _workingAreaRTsWRTProjector;
        
        private double _z0 = 0;
        
        private int _numMarkers = 0;

        private Vector3 startVertex, endVertex;
        
        private int _numClassifiedObjs= 0;

        private int internalState_ = STATE_CHECK;
        
        private Texture2D _texture;
        private Color32[] _colors;
//        private Mat _worldPlane = new Mat(4, 3, CvType.CV_32FC1);

        Object lockObject=new Object();

        private static int NUM_CALIBRATION_MEASURES = 10;
        private int _numCalibrationFrames=0;

        private CallbackProcedure _callbackProcedure;

        private int _timer = 0;
        private int _maxTimerValue = 0;
        
        private const TextureFormat Format =
            
#if UNITY_IOS && !UNITY_EDITOR
            TextureFormat.BGRA32;
#else
            TextureFormat.RGBA32;
#endif

        
        
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate  void CallbackProcedure(int value);


        int SetTimer(int timer)
        {
            _maxTimerValue=timer;
            return ResetTimer();
        }

        int GetTimer()
        {
            return _timer;
        }
        bool Tick()
        {
            _timer++;
            if (_timer < _maxTimerValue)
                return false;
            else
            {
                return true;
            }
        }

        int ResetTimer()
        {
            _timer = 0;
            return _timer;
        }
        
        
        
        void Awake()
        {
//            for (int i = 0; i < ArObjects.Count; i++)
//            {
//                ArObjects[i].ArGameObjectRoot.SetActive(false);
//            }

            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
        }

        public void Start()
        {
            ReleaseObjects();
            Debug.Log("Creating Objects");
            _foundObjects = new List<KeyValuePair<int, Matrix4x4>>();            
            _foundObjectsWB = new List<KeyValuePair<int, Matrix4x4>>();
            _foundMarkers= new List<KeyValuePair<int, Matrix4x4>>();
            _arObjectsInternal = new List<ArObjectInternal>();
            _nativeObject = new NativeProjectionGuide();
            _poseMatrix = new Mat(5, 4, CvType.CV_64FC1);
            _poseMatrixWB = new Mat(5, 4, CvType.CV_64FC1);

            _xP_Ms = new List<KeyValuePair<int, Mat>>();//Mat(6,1,CvType.CV_64FC1);
            _sensibleAreas= new List<KeyValuePair<int, int>>();
            _sensibleAreasCoords= new List<KeyValuePair<int, Mat>>();
            _sensibleAreasRTs= new List<KeyValuePair<int, Mat>>();
            _sensibleAreasRTsWRTProjector= new List<KeyValuePair<int, Mat>>();
//            _workingAreaRTsWRTProjector = new List<KeyValuePair<int, Mat>>();
            _poseMarkersMatrixes = new Mat(5*MAX_NUM_MARKERS, 4, CvType.CV_64FC1);
            _poseMarkersMatrixesTmp = new Mat(5*MAX_NUM_MARKERS, 4, CvType.CV_64FC1);
            _rtP_C = new Mat(4, 4, CvType.CV_64FC1);

            workingArea = new WorkingArea();
            toolArea1 = new ToolArea();
            toolArea2 = new ToolArea();
            buttonArea1 = new ButtonArea();
            buttonArea2 = new ButtonArea();

            workingArea.markerID = 1;
            workingArea.areaID = WORKINGAREAID;
            workingArea.SetStatus(TOOLAREA_UNDEF);
            
            toolArea1.markerID = 2;
            toolArea1.areaID = TOOLAREA1ID;
            toolArea1.SetStatus(TOOLAREA_UNDEF);
            toolArea2.markerID = 3;
            toolArea2.areaID = TOOLAREA2ID;
            toolArea2.SetStatus(TOOLAREA_UNDEF);
            
            buttonArea1.markerID = 4;
            buttonArea1.areaID = BUTTON1AREAID;
            buttonArea1.SetStatus(TOOLAREA_UNDEF);
            buttonArea2.markerID = 5;
            buttonArea2.areaID = BUTTON2AREAID;
            buttonArea2.SetStatus(TOOLAREA_UNDEF);
            
            
            UnityCamera.projectionMatrix = LoadProjectionMatrix(2870,2948, 1200, 1141, UnityCamera);

            
            Debug.Log("CALIBRATION DIRECTORY: "+Application.persistentDataPath);
            _calibDataFilename = System.IO.Path.Combine(Application.persistentDataPath, "calibData.json"); // persistentDataPath unity save game path, actors.json name of the file containing actors 
            _configWorkBenchDataFilename= System.IO.Path.Combine(Application.persistentDataPath, "configWorkBenchData.json"); // persistentDataPath unity save game path, actors.json name of the file containing actors 
                    
            //UnityCamera.fieldOfView = 46;
            _nativeObject.SetCameraParams(1600,1200, 60, 46);
            
            _nativeObject.Configure();
            Utils.ConfigureArObjectsInternal(ArObjects, _arObjectsInternal, _nativeObject);

            _nativeObject.Init();
            _nativeObject.Start();

            

            _callbackProcedure = _nativeObject.OnResult;
            
            _nativeObject.SetCallbackProcedure(_callbackProcedure);

//            internalState_ = STATE_CALIBRATION;
            SelectCameraDevice();

            //SetPSNRThresholds(30, 25);
            //SetSSIMThresholds(0.8, 0.8);
            SetPSNRThresholds(-1, -1);
            SetSSIMThresholds(-1, -1);


            _startObjectDetectionAndTracking = false;
            _waitObjectDetectionAndTrackingStart = 0;

            _configureWorkBenchParamsLoaded = false;
            _calibrationParamsLoaded = false;
            
            
            _waitForZoomAdjustment = 0;
            //if zoom adjustment is required
//            _initialized = false;
            //else
            _initialized = true;

            

            
//            _nativeObject.SetState(internalState_);


//            double[] tmp = new double[1];
            //            tmp[0] = -1.0 / 2.0;


//            _xP_W.put(0, 0, WorldReferencePlane.transform.localEulerAngles.x*Mathf.Deg2Rad);
//            _xP_W.put(1, 0, WorldReferencePlane.transform.localEulerAngles.y*Mathf.Deg2Rad);
//            _xP_W.put(2, 0, WorldReferencePlane.transform.localEulerAngles.z*Mathf.Deg2Rad);
//            _xP_W.put(3, 0, WorldReferencePlane.transform.localPosition.x);
//            _xP_W.put(4, 0, WorldReferencePlane.transform.localPosition.y);
//            _xP_W.put(5, 0, WorldReferencePlane.transform.localPosition.z);

//            //RHS Rototranslation World to Projector xP_W 
//            _xP_W.put(0, 0, 90.0*Mathf.Deg2Rad);
//            _xP_W.put(1, 0, 0.0);
//            _xP_W.put(2, 0, 0.0);
//            _xP_W.put(3, 0, 0.0);
//            _xP_W.put(4, 0, -0.5);
//            _xP_W.put(5, 0, 2.0);
//
//
////            string str=Utils.matToString(_xP_W, false);
////            
////            Debug.Log(str);
//            Debug.Log(_xP_W.get(0, 0)[0]+" "+_xP_W.get(1, 0)[0]+" "+_xP_W.get(2, 0)[0]+" "+_xP_W.get(3, 0)[0]+" "+_xP_W.get(4, 0)[0]+" "+_xP_W.get(5, 0)[0]);
//            
//            _nativeObject.SetXProjector_W(_xP_W);

        }




        private void SelectCameraDevice()
        {
            _nativeObject.SetCameraID(1);

        }

//fx = 765.09, fy=768.83, Cx= 320, Cy= 240


        private Matrix4x4 LoadProjectionMatrix(float fx, float fy, float cx, float cy, Camera _mainCamera)
        {
            float w = _mainCamera.pixelWidth;
            float h = _mainCamera.pixelHeight;
            float nearDist = _mainCamera.nearClipPlane;
            float farDist = _mainCamera.farClipPlane;

            return MakeFrustumMatrix(
                nearDist * (-cx) / fx, nearDist * (w - cx) / fx,
                nearDist * (cy) / fy, nearDist * (cy - h) / fy,
                nearDist, farDist);
        }

        private Matrix4x4 MakeFrustumMatrix(float left, float right,
            float bottom, float top,
            float zNear, float zFar)
        {
            float x = 2.0f * zNear / (right - left);
            float y = 2.0f * zNear / (top - bottom);
            float A = (right + left) / (right - left);
            float B = (top + bottom) / (top - bottom);
            float C = -(zFar + zNear) / (zFar - zNear);
            float D = -(2.0f * zFar * zNear) / (zFar - zNear);

            var persp = new Matrix4x4();
            persp[0, 0] = x;
            persp[1, 1] = y;
            persp[0, 2] = A;
            persp[1, 2] = B;
            persp[2, 2] = C;
            persp[3, 2] = -1.0f;
            persp[2, 3] = D;

            var rhsToLhs = new Matrix4x4();
            rhsToLhs[0, 0] = 1.0f;
            rhsToLhs[1, 1] = -1.0f; // Flip Y (RHS -> LHS)
            rhsToLhs[2, 2] = 1.0f;
            rhsToLhs[3, 3] = 1.0f;

            return rhsToLhs * persp; // see comment above
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                ReleaseObjects();
            }
        }

        void OnGUI()
        {
            String idsStr="";
            
            for (int i = 0; i < _markerIds.Count; i++)
            {
                idsStr=idsStr+" "+_markerIds[i].ToString();
            }
//            GUILayout.TextField("Found " +_markerIds.Count+" markers -> IDs: "+ idsStr);
        }


        void StartProjectCalibrationMarkers(List<KeyValuePair<int,Mat>> xP_Ms)
        {
            UnityCamera.backgroundColor=new Color32(0,0,0,255);
            MarkersPlane10.SetActive(true);
            MarkersPlane11.SetActive(true);
            MarkersPlane12.SetActive(true);
//            MarkersPlane13.SetActive(true);
//            MarkersPlane14.SetActive(true);
        }
        void StopProjectCalibrationMarkers()
        {
            UnityCamera.backgroundColor=new Color32(0,0,0,255);
            MarkersPlane10.SetActive(false);
            MarkersPlane11.SetActive(false);
            MarkersPlane12.SetActive(false);
//            MarkersPlane13.SetActive(false);
//            MarkersPlane14.SetActive(false);
        }

        
        
        
        void MarkerDetection()
        {
            UnityCamera.backgroundColor=new Color32(255,255,255,255);
            _numMarkers = _nativeObject.GetMarkersRTMatrixesWRTProjector(_poseMarkersMatrixes);
    
//            _nativeObject.GetMarkersRTMatrixes(_poseMarkersMatrixesTmp);
            
            _markerIds.Clear();
            _foundMarkers.Clear();
            
            
    
            for (int i = 0; i < _poseMarkersMatrixes.rows() / 5; i++)
            {
                int markerId = (int) _poseMarkersMatrixes.get(i * 5, 0)[0];
    
                _markerIds.Add(markerId);
                Matrix4x4 rtMatrix4X4 = new Matrix4x4();
                rtMatrix4X4.SetRow(0,
                    new Vector4((float) _poseMarkersMatrixes.get(i * 5 + 1, 0)[0],
                        (float) _poseMarkersMatrixes.get(i * 5 + 1, 1)[0],
                        (float) _poseMarkersMatrixes.get(i * 5 + 1, 2)[0],
                        (float) _poseMarkersMatrixes.get(i * 5 + 1, 3)[0]));
                rtMatrix4X4.SetRow(1,
                    new Vector4((float) _poseMarkersMatrixes.get(i * 5 + 2, 0)[0],
                        (float) _poseMarkersMatrixes.get(i * 5 + 2, 1)[0],
                        (float) _poseMarkersMatrixes.get(i * 5 + 2, 2)[0],
                        (float) _poseMarkersMatrixes.get(i * 5 + 2, 3)[0]));
                rtMatrix4X4.SetRow(2,
                    new Vector4((float) _poseMarkersMatrixes.get(i * 5 + 3, 0)[0],
                        (float) _poseMarkersMatrixes.get(i * 5 + 3, 1)[0],
                        (float) _poseMarkersMatrixes.get(i * 5 + 3, 2)[0],
                        (float) _poseMarkersMatrixes.get(i * 5 + 3, 3)[0]));
                rtMatrix4X4.SetRow(3, new Vector4(0, 0, 0, 1));
    
    //                            Debug.Log("RT MATRIX: "+rtMatrix4X4.m00+" "+rtMatrix4X4.m01+" "+rtMatrix4X4.m02+" "+rtMatrix4X4.m03+"\n"+
    //                            rtMatrix4X4.m10+" "+rtMatrix4X4.m11+" "+rtMatrix4X4.m12+" "+rtMatrix4X4.m13+"\n"+
    //                            rtMatrix4X4.m20+" "+rtMatrix4X4.m21+" "+rtMatrix4X4.m22+" "+rtMatrix4X4.m23+"\n"+
    //                            rtMatrix4X4.m30+" "+rtMatrix4X4.m31+" "+rtMatrix4X4.m32+" "+rtMatrix4X4.m33);
    
                _foundMarkers.Add(new KeyValuePair<int, Matrix4x4>(markerId, rtMatrix4X4));
                
            }
                    

            List<IGrouping<int, KeyValuePair<int, Matrix4x4>>> foundMarkersGroups =
                _foundMarkers.GroupBy(i => i.Key).ToList();

            for (int i = 0; i < _arObjectsInternal.Count; i++)
            {
                IGrouping<int, KeyValuePair<int, Matrix4x4>> foundMarker =
                    foundMarkersGroups.FirstOrDefault(foundMarkersGroup =>
                        foundMarkersGroup.Key == _markerIds[i]);

                //                        Debug.Log("found marker 1a: " + foundMarker); 
                if (foundMarker == null)
                {
                    //                            Debug.Log("found marker 1b: " + "NULL "+_arObjectsInternal[i].Instances.Count); 
                    for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= 0; j--)
                    {
                        _arObjectsInternal[i].Instances[j].SetActive(false);
                    }
                }
                else
                {

                    //                            Debug.Log("found marker 2: " + foundMarker.Key); 
                    // Add missing instances
                    while (_arObjectsInternal[i].Instances.Count < foundMarker.Count())
                    {
                        _arObjectsInternal[i].Instances
                            .Add(Instantiate(_arObjectsInternal[i].ArObject.ArGameObjectRoot));
                    }


                    // Hide exceeding instances
                    for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= foundMarker.Count(); j--)
                    {
                        _arObjectsInternal[i].Instances[j].SetActive(false);
                    }

                    // New positions to Gameobjects
                    List<KeyValuePair<int, Matrix4x4>> foundMarkerList = foundMarker.ToList();

                    //                            Debug.Log("found marker 3: " + foundMarkerList[0].Key+" "+foundMarkerList[0].Value); 
                    for (int sharedIndex = 0;
                        sharedIndex < _arObjectsInternal[i].Instances.Count &&
                        sharedIndex < foundMarkerList.Count;
                        sharedIndex++)
                    {
                        GameObject gameObjectInstance = _arObjectsInternal[i].Instances[sharedIndex];
                        Matrix4x4 matrix4X4 = foundMarkerList[sharedIndex].Value;

                            gameObjectInstance.transform.position =
                                Utils.ExtractTranslationFromMatrix(ref matrix4X4);
                       
                        float ratio = 2*((float)_z0 / gameObjectInstance.transform.position.z);
                        Debug.Log("RATIO: "+_z0+" " + ratio);
                        
                        Vector3 vec=new Vector3(gameObjectInstance.transform.position.x,gameObjectInstance.transform.position.y,gameObjectInstance.transform.position.z);
                        gameObjectInstance.transform.position = vec;


                        gameObjectInstance.transform.rotation =
                            Utils.ExtractRotationFromMatrix(ref matrix4X4);

                        Debug.Log("Projector - Marker LHS Trasl: "+gameObjectInstance.transform.position.x+" "+gameObjectInstance.transform.position.y+" "+gameObjectInstance.transform.position.z);
                        Debug.Log("Projector - Marker LHS Rot: "+gameObjectInstance.transform.rotation.eulerAngles.x+" "+gameObjectInstance.transform.rotation.eulerAngles.y+" "+gameObjectInstance.transform.rotation.eulerAngles.z);
               
                        if (!gameObjectInstance.active)
                            gameObjectInstance.SetActive(true);
                    }
                }
            }
                    
        }

 
        int ObjectDetectionAndTracking(int processStatus)
        {

            int ret = 0;
            UnityCamera.backgroundColor=new Color32(0,0,0,255); 
                                                                  
//            _nativeObject.GetObjectRTMatrix(_poseMatrix);
            _nativeObject.GetObjectRTMatrixWRTProjector(_poseMatrix);
            _nativeObject.GetObjectRTWBMatrixWRTProjector(_poseMatrixWB);
            
            Debug.Log("ObjectDetectionAndTracking START");
       
            _objsIds.Clear();
            _foundObjects.Clear();
            _foundObjectsWB.Clear();

            int objectId = -1;
    
            for (int i = 0; i < _poseMatrix.rows() / 5; i++)
            {
                Debug.Log("ObjectDetectionAndTracking Obj:"+i);
       
                int maskedObjectId = (int) _poseMatrix.get(i * 5, 0)[0];
                objectId = maskedObjectId / 1000;
                _objsIds.Add(objectId);
    
    
                Matrix4x4 rtMatrix4X4 = new Matrix4x4();
                Matrix4x4 rtMatrix4X4WB = new Matrix4x4();
                rtMatrix4X4.SetRow(0,
                    new Vector4((float) _poseMatrix.get(i * 5 + 1, 0)[0],
                        (float) _poseMatrix.get(i * 5 + 1, 1)[0],
                        (float) _poseMatrix.get(i * 5 + 1, 2)[0],
                        (float) _poseMatrix.get(i * 5 + 1, 3)[0]));
                rtMatrix4X4.SetRow(1,
                    new Vector4((float) _poseMatrix.get(i * 5 + 2, 0)[0],
                        (float) _poseMatrix.get(i * 5 + 2, 1)[0],
                        (float) _poseMatrix.get(i * 5 + 2, 2)[0],
                        (float) _poseMatrix.get(i * 5 + 2, 3)[0]));
                rtMatrix4X4.SetRow(2,
                    new Vector4((float) _poseMatrix.get(i * 5 + 3, 0)[0],
                        (float) _poseMatrix.get(i * 5 + 3, 1)[0],
                        (float) _poseMatrix.get(i * 5 + 3, 2)[0],
                        (float) _poseMatrix.get(i * 5 + 3, 3)[0]));
                rtMatrix4X4.SetRow(3, new Vector4(0, 0, 0, 1));
                
                rtMatrix4X4WB.SetRow(0,
                    new Vector4((float) _poseMatrixWB.get(i * 5 + 1, 0)[0],
                        (float) _poseMatrixWB.get(i * 5 + 1, 1)[0],
                        (float) _poseMatrixWB.get(i * 5 + 1, 2)[0],
                        (float) _poseMatrixWB.get(i * 5 + 1, 3)[0]));
                rtMatrix4X4WB.SetRow(1,
                    new Vector4((float) _poseMatrixWB.get(i * 5 + 2, 0)[0],
                        (float) _poseMatrixWB.get(i * 5 + 2, 1)[0],
                        (float) _poseMatrixWB.get(i * 5 + 2, 2)[0],
                        (float) _poseMatrixWB.get(i * 5 + 2, 3)[0]));
                rtMatrix4X4WB.SetRow(2,
                    new Vector4((float) _poseMatrixWB.get(i * 5 + 3, 0)[0],
                        (float) _poseMatrixWB.get(i * 5 + 3, 1)[0],
                        (float) _poseMatrixWB.get(i * 5 + 3, 2)[0],
                        (float) _poseMatrixWB.get(i * 5 + 3, 3)[0]));
                rtMatrix4X4WB.SetRow(3, new Vector4(0, 0, 0, 1));
     
                _foundObjects.Add(new KeyValuePair<int, Matrix4x4>(objectId, rtMatrix4X4));
                _foundObjectsWB.Add(new KeyValuePair<int, Matrix4x4>(objectId, rtMatrix4X4WB));
                
                                Debug.Log("RT MATRIX: "+rtMatrix4X4.m00+" "+rtMatrix4X4.m01+" "+rtMatrix4X4.m02+" "+rtMatrix4X4.m03+"\n"+
                                rtMatrix4X4.m10+" "+rtMatrix4X4.m11+" "+rtMatrix4X4.m12+" "+rtMatrix4X4.m13+"\n"+
                                rtMatrix4X4.m20+" "+rtMatrix4X4.m21+" "+rtMatrix4X4.m22+" "+rtMatrix4X4.m23+"\n"+
                                rtMatrix4X4.m30+" "+rtMatrix4X4.m31+" "+rtMatrix4X4.m32+" "+rtMatrix4X4.m33);

            }

//
//            if (_foundObjects.Count > 0)
//            {
//                
//                Matrix4x4 rtP_O = _foundObjects[0].Value;
//                Matrix4x4 rtP_OWB = _foundObjectsWB[0].Value;
//                
//                ObjectDetected.transform.localPosition =
//                    Utils.ExtractTranslationFromMatrix(ref rtP_O);
//
//                WorkBenchPlane.transform.localPosition =
//                    Utils.ExtractTranslationFromMatrix(ref rtP_OWB);
//
//
//                Quaternion rotQuat = Utils.ExtractRotationFromMatrix(ref rtP_O);
//                
//                ObjectDetected.transform.localRotation = rotQuat;
//                
//                Quaternion rotQuatWB = Utils.ExtractRotationFromMatrix(ref rtP_OWB);
//                
//                
//                WorkBenchPlane.transform.localRotation = rotQuatWB; 
//                
//
//
//                Debug.Log("Projector - Object LHS Trasl: " + WorkBenchPlane.transform.position.x + " " +
//                          WorkBenchPlane.transform.position.y + " " + WorkBenchPlane.transform.position.z);
//                Debug.Log("Projector - Object LHS Rot: " + WorkBenchPlane.transform.rotation.eulerAngles.x + " " +
//                          WorkBenchPlane.transform.rotation.eulerAngles.y + " " +
//                          WorkBenchPlane.transform.rotation.eulerAngles.z);
//
////                ObjectDetected.SetActive(true);
////                WorkingAreaBusy.SetActive(true);
////                WorkBenchPlane.SetActive(true);
//            }
//            else
//            {
////                WorkingAreaFree.SetActive(true);
//                ObjectDetected.SetActive(false);
//                WorkBenchPlane.SetActive(false);
//            }
//
//            ret=RefreshWorkingAreaStatus(objectId, status);
            

            Debug.Log("ObjectDetectionAndTracking...END");

            if (ret == 0)
                return _foundObjects.Count;
            else
            {
                return ret;
            }
        }

        int CheckToolsAreas(int processStatus)
        {
            int ret = -1;
            Debug.Log("CheckToolsAreas...START");
            Mat sensibleAreaStatus = new Mat(MAX_NUM_SENSIBLE_AREAS, 2, CvType.CV_64FC1);
            _nativeObject.GetSensibleAreasStatus(sensibleAreaStatus);
            int numToolsFound = 0;
            for (int i = 0; i < sensibleAreaStatus.rows(); i++)
            {
                int id=(int)sensibleAreaStatus.get(i, 0)[0];
                int status=(int)sensibleAreaStatus.get(i, 1)[0];

                if (id == TOOLAREA1ID || id == TOOLAREA2ID)
                {
                    ret=RefreshToolAreaStatus(id, status);
                    if (ret==0 && status==TOOLAREA_BUSY)
                        numToolsFound++;
                }                
            }
            Debug.Log("CheckToolsAreas...END");
            if (ret == 0)
                return numToolsFound;
            else
                return ret;
        }

        int CheckButtonsAreas(int processStatus)
        {
            int ret = -1;
            Debug.Log("CheckButtonsAreas...START");
            Mat sensibleAreaStatus = new Mat(MAX_NUM_SENSIBLE_AREAS, 2, CvType.CV_64FC1);
            _nativeObject.GetSensibleAreasStatus(sensibleAreaStatus);
            int numButtonsFound = 0;
            for (int i = 0; i < sensibleAreaStatus.rows(); i++)
            {
                int id=(int)sensibleAreaStatus.get(i, 0)[0];
                int status=(int)sensibleAreaStatus.get(i, 1)[0];
                if (id == BUTTON1AREAID || id == BUTTON2AREAID)
                {
                    ret=RefreshButtonAreaStatus(id, status);
                    if (ret==0 && status==TOOLAREA_BUSY)
                        numButtonsFound++;
                }
            }
            Debug.Log("CheckButtonsAreas...END");
            if (ret == 0)
                return numButtonsFound;
            else
                return ret;
        }
        

        int ObjectDetectionAndTracking()
        {

            int ret = -1;
            UnityCamera.backgroundColor=new Color32(0,0,0,255); 
                                                                  
//            _nativeObject.GetObjectRTMatrix(_poseMatrix);
            _nativeObject.GetObjectRTMatrixWRTProjector(_poseMatrix);
            _nativeObject.GetObjectRTWBMatrixWRTProjector(_poseMatrixWB);
            
            Debug.Log("ObjectDetectionAndTracking START");
       
            _objsIds.Clear();
            _foundObjects.Clear();
            _foundObjectsWB.Clear();

            int status = TOOLAREA_FREE;
            int objectId = -1;
    
            for (int i = 0; i < _poseMatrix.rows() / 5; i++)
            {
                Debug.Log("ObjectDetectionAndTracking Obj:"+i);
       
                int maskedObjectId = (int) _poseMatrix.get(i * 5, 0)[0];
                objectId = maskedObjectId / 1000;
                _objsIds.Add(objectId);
    
    
                Matrix4x4 rtMatrix4X4 = new Matrix4x4();
                Matrix4x4 rtMatrix4X4WB = new Matrix4x4();
                rtMatrix4X4.SetRow(0,
                    new Vector4((float) _poseMatrix.get(i * 5 + 1, 0)[0],
                        (float) _poseMatrix.get(i * 5 + 1, 1)[0],
                        (float) _poseMatrix.get(i * 5 + 1, 2)[0],
                        (float) _poseMatrix.get(i * 5 + 1, 3)[0]));
                rtMatrix4X4.SetRow(1,
                    new Vector4((float) _poseMatrix.get(i * 5 + 2, 0)[0],
                        (float) _poseMatrix.get(i * 5 + 2, 1)[0],
                        (float) _poseMatrix.get(i * 5 + 2, 2)[0],
                        (float) _poseMatrix.get(i * 5 + 2, 3)[0]));
                rtMatrix4X4.SetRow(2,
                    new Vector4((float) _poseMatrix.get(i * 5 + 3, 0)[0],
                        (float) _poseMatrix.get(i * 5 + 3, 1)[0],
                        (float) _poseMatrix.get(i * 5 + 3, 2)[0],
                        (float) _poseMatrix.get(i * 5 + 3, 3)[0]));
                rtMatrix4X4.SetRow(3, new Vector4(0, 0, 0, 1));
                
                rtMatrix4X4WB.SetRow(0,
                    new Vector4((float) _poseMatrixWB.get(i * 5 + 1, 0)[0],
                        (float) _poseMatrixWB.get(i * 5 + 1, 1)[0],
                        (float) _poseMatrixWB.get(i * 5 + 1, 2)[0],
                        (float) _poseMatrixWB.get(i * 5 + 1, 3)[0]));
                rtMatrix4X4WB.SetRow(1,
                    new Vector4((float) _poseMatrixWB.get(i * 5 + 2, 0)[0],
                        (float) _poseMatrixWB.get(i * 5 + 2, 1)[0],
                        (float) _poseMatrixWB.get(i * 5 + 2, 2)[0],
                        (float) _poseMatrixWB.get(i * 5 + 2, 3)[0]));
                rtMatrix4X4WB.SetRow(2,
                    new Vector4((float) _poseMatrixWB.get(i * 5 + 3, 0)[0],
                        (float) _poseMatrixWB.get(i * 5 + 3, 1)[0],
                        (float) _poseMatrixWB.get(i * 5 + 3, 2)[0],
                        (float) _poseMatrixWB.get(i * 5 + 3, 3)[0]));
                rtMatrix4X4WB.SetRow(3, new Vector4(0, 0, 0, 1));
     
                _foundObjects.Add(new KeyValuePair<int, Matrix4x4>(objectId, rtMatrix4X4));
                _foundObjectsWB.Add(new KeyValuePair<int, Matrix4x4>(objectId, rtMatrix4X4WB));
                
                                Debug.Log("RT MATRIX: "+rtMatrix4X4.m00+" "+rtMatrix4X4.m01+" "+rtMatrix4X4.m02+" "+rtMatrix4X4.m03+"\n"+
                                rtMatrix4X4.m10+" "+rtMatrix4X4.m11+" "+rtMatrix4X4.m12+" "+rtMatrix4X4.m13+"\n"+
                                rtMatrix4X4.m20+" "+rtMatrix4X4.m21+" "+rtMatrix4X4.m22+" "+rtMatrix4X4.m23+"\n"+
                                rtMatrix4X4.m30+" "+rtMatrix4X4.m31+" "+rtMatrix4X4.m32+" "+rtMatrix4X4.m33);

                status = TOOLAREA_BUSY;
//                workingArea.SetStatus(TOOLAREA_BUSY);
               
//                workingArea.objectID = objectId;
            }


            if (_foundObjects.Count > 0)
            {
                
                Matrix4x4 rtP_O = _foundObjects[0].Value;
                Matrix4x4 rtP_OWB = _foundObjectsWB[0].Value;
                
                ObjectDetected.transform.localPosition =
                    Utils.ExtractTranslationFromMatrix(ref rtP_O);

                WorkBenchPlane.transform.localPosition =
                    Utils.ExtractTranslationFromMatrix(ref rtP_OWB);


                Quaternion rotQuat = Utils.ExtractRotationFromMatrix(ref rtP_O);
                
                ObjectDetected.transform.localRotation = rotQuat;
                
                Quaternion rotQuatWB = Utils.ExtractRotationFromMatrix(ref rtP_OWB);
                
                
                WorkBenchPlane.transform.localRotation = rotQuatWB; 
                


                Debug.Log("Projector - Object LHS Trasl: " + WorkBenchPlane.transform.position.x + " " +
                          WorkBenchPlane.transform.position.y + " " + WorkBenchPlane.transform.position.z);
                Debug.Log("Projector - Object LHS Rot: " + WorkBenchPlane.transform.rotation.eulerAngles.x + " " +
                          WorkBenchPlane.transform.rotation.eulerAngles.y + " " +
                          WorkBenchPlane.transform.rotation.eulerAngles.z);

//                ObjectDetected.SetActive(true);
//                WorkingAreaBusy.SetActive(true);
//                WorkBenchPlane.SetActive(true);
            }
            else
            {
//                WorkingAreaFree.SetActive(true);
                ObjectDetected.SetActive(false);
                WorkBenchPlane.SetActive(false);
            }

            ret=RefreshWorkingAreaStatus(objectId, status);
            

            Debug.Log("ObjectDetectionAndTracking...END");

            return ret;

        }

        int CheckSensibleAreas()
        {
            int ret = -1;
            Debug.Log("CheckSensibleAreas...START");
            Mat sensibleAreaStatus = new Mat(MAX_NUM_SENSIBLE_AREAS, 2, CvType.CV_64FC1);
            _nativeObject.GetSensibleAreasStatus(sensibleAreaStatus);

            for (int i = 0; i < sensibleAreaStatus.rows(); i++)
            {
                int id=(int)sensibleAreaStatus.get(i, 0)[0];
                int status=(int)sensibleAreaStatus.get(i, 1)[0];

                if (id == TOOLAREA1ID || id == TOOLAREA2ID)
                {
                    ret=RefreshToolAreaStatus(id, status);
                }
                else if (id == BUTTON1AREAID || id == BUTTON2AREAID)
                {
                    ret=RefreshButtonAreaStatus(id, status);                    
                }
            }
            Debug.Log("CheckSensibleAreas...END");
            return ret;
        }

        int SetupWorkbench()
        {
            int ret = -1;
            for (int i = 0; i < _sensibleAreasRTsWRTProjector.Count; i++)
            {
                Matrix4x4 rtMatrix4X4 = new Matrix4x4();
                if (_sensibleAreasRTsWRTProjector[i].Key == TOOLAREA1ID)
                {
                    
                    for (int j = 0; j < 4; j++)
                    {
                        rtMatrix4X4.SetRow(j,
                            new Vector4((float) _sensibleAreasRTsWRTProjector[i].Value.get(j , 0)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 1)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 2)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 3)[0]));
                    }
                    SensibleArea1_free.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea1_free.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea1_busy.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea1_busy.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea1_undef.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea1_undef.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    
                    TextArea1_B.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
                    TextArea1_B.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                }
                else if (_sensibleAreasRTsWRTProjector[i].Key == TOOLAREA2ID)
                {
                    
                    for (int j = 0; j < 4; j++)
                    {
                        rtMatrix4X4.SetRow(j,
                            new Vector4((float) _sensibleAreasRTsWRTProjector[i].Value.get(j , 0)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 1)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 2)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 3)[0]));
                    }
                    SensibleArea2_free.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea2_free.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea2_busy.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea2_busy.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea2_undef.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea2_undef.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                }
                else if (_sensibleAreasRTsWRTProjector[i].Key == BUTTON1AREAID)
                {
                    
                    for (int j = 0; j < 4; j++)
                    {
                        rtMatrix4X4.SetRow(j,
                            new Vector4((float) _sensibleAreasRTsWRTProjector[i].Value.get(j , 0)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 1)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 2)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 3)[0]));
                    }
                    SensibleArea3_free.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea3_free.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea3_busy.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea3_busy.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea3_undef.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea3_undef.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea3_pressed0.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea3_pressed0.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea3_pressed25.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea3_pressed25.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea3_pressed50.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea3_pressed50.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea3_pressed75.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea3_pressed75.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    
                    TextArea1_C.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
                    TextArea1_C.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    
                    
                }
                else if (_sensibleAreasRTsWRTProjector[i].Key == BUTTON2AREAID)
                {
                    
                    for (int j = 0; j < 4; j++)
                    {
                        rtMatrix4X4.SetRow(j,
                            new Vector4((float) _sensibleAreasRTsWRTProjector[i].Value.get(j , 0)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 1)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 2)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 3)[0]));
                    }
                    SensibleArea4_free.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea4_free.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea4_busy.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea4_busy.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea4_undef.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea4_undef.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea4_pressed0.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea4_pressed0.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea4_pressed25.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea4_pressed25.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea4_pressed50.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea4_pressed50.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    SensibleArea4_pressed75.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);            
                    SensibleArea4_pressed75.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    
                }
                else if (_sensibleAreasRTsWRTProjector[i].Key == WORKINGAREAID)
                {
                    
                    for (int j = 0; j < 4; j++)
                    {
                        rtMatrix4X4.SetRow(j,
                            new Vector4((float) _sensibleAreasRTsWRTProjector[i].Value.get(j , 0)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 1)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 2)[0],
                                (float) _sensibleAreasRTsWRTProjector[i].Value.get(j, 3)[0]));
                    }
                    WorkingArea_busy.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
                    WorkingArea_busy.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    WorkingArea_free.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
                    WorkingArea_free.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    WorkingArea_undef.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
                    WorkingArea_undef.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);

                    TextArea1_A.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
                    TextArea1_A.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    
                    TextArea1_D.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
                    TextArea1_D.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    TextArea2_A.transform.localPosition =
                        Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
                    TextArea2_A.transform.localRotation =
                        Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                    
                    ret = 0;

                }
                
            }
            
            SensibleArea1_free.SetActive(false);
            SensibleArea1_busy.SetActive(false);
            SensibleArea1_undef.SetActive(false);
//            SensibleArea2_free.SetActive(false);
//            SensibleArea2_busy.SetActive(false);
//            SensibleArea2_undef.SetActive(false);
            SensibleArea3_free.SetActive(false);
            SensibleArea3_busy.SetActive(false);
            SensibleArea3_undef.SetActive(false);
            SensibleArea3_pressed0.SetActive(false);
            SensibleArea3_pressed25.SetActive(false);
            SensibleArea3_pressed50.SetActive(false);
            SensibleArea3_pressed75.SetActive(false);
//            SensibleArea4_free.SetActive(false);
//            SensibleArea4_busy.SetActive(false);
//            SensibleArea4_undef.SetActive(false);
//            SensibleArea4_pressed0.SetActive(false);
//            SensibleArea4_pressed25.SetActive(false);
//            SensibleArea4_pressed50.SetActive(false);
//            SensibleArea4_pressed75.SetActive(false);
            WorkingArea_free.SetActive(false);
            WorkingArea_undef.SetActive(false);
            WorkingArea_busy.SetActive(false);
            
            TextArea1_A.SetActive(false);
            TextArea1_B.SetActive(false);
            TextArea1_C.SetActive(false);
            TextArea1_D.SetActive(false);
            TextArea2_A.SetActive(false);
            

            return ret;
        }

        void OnToolArea1Busy()
        {
            int idx = 0;
            for (int i = 0; i < _sensibleAreasRTsWRTProjector.Count; i++)
            {
                if (_sensibleAreasRTsWRTProjector[i].Key == TOOLAREA1ID)
                {
                    idx = i;
                }
            }
            Matrix4x4 rtMatrix4X4 = new Matrix4x4();
            for (int i = 0; i < 4; i++)
            {
                    rtMatrix4X4.SetRow(i,
                        new Vector4((float) _sensibleAreasRTsWRTProjector[idx].Value.get(i , 0)[0],
                            (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 1)[0],
                            (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 2)[0],
                            (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 3)[0]));
            }
                
            SensibleArea1_busy.transform.localPosition =
                    Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
            
            SensibleArea1_busy.transform.localRotation =
                    Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
            SensibleArea1_busy.SetActive(true);
            SensibleArea1_free.SetActive(false);
            SensibleArea1_undef.SetActive(false);
        }
        void OnToolArea1Free()
        {
            int idx = 0;
            for (int i = 0; i < _sensibleAreasRTsWRTProjector.Count; i++)
            {
                if (_sensibleAreasRTsWRTProjector[i].Key == TOOLAREA1ID)
                {
                    idx = i;
                }
            }
            Matrix4x4 rtMatrix4X4 = new Matrix4x4();
            for (int i = 0; i < 4; i++)
            {
                rtMatrix4X4.SetRow(i,
                    new Vector4((float) _sensibleAreasRTsWRTProjector[idx].Value.get(i , 0)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 1)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 2)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 3)[0]));
            }
                
            SensibleArea1_free.transform.localPosition =
                Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
            
            SensibleArea1_free.transform.localRotation =
                Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
            SensibleArea1_free.SetActive(true);
            SensibleArea1_busy.SetActive(false);
            SensibleArea1_undef.SetActive(false);
        }
        
        void OnToolArea1Undef()
        {
            int idx = 0;
            for (int i = 0; i < _sensibleAreasRTsWRTProjector.Count; i++)
            {
                if (_sensibleAreasRTsWRTProjector[i].Key == TOOLAREA1ID)
                {
                    idx = i;
                }
            }
            Matrix4x4 rtMatrix4X4 = new Matrix4x4();
            for (int i = 0; i < 4; i++)
            {
                rtMatrix4X4.SetRow(i,
                    new Vector4((float) _sensibleAreasRTsWRTProjector[idx].Value.get(i , 0)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 1)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 2)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 3)[0]));
            }
                
            SensibleArea1_undef.transform.localPosition =
                Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
            
            SensibleArea1_undef.transform.localRotation =
                Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
            SensibleArea1_undef.SetActive(true);
            SensibleArea1_free.SetActive(false);
            SensibleArea1_busy.SetActive(false);
        }
        
        void OnToolArea2Busy()
        {
            SensibleArea2_free.SetActive(true);
        }
        void OnToolArea2Free()
        {
            SensibleArea2_free.SetActive(false);
        }
        void OnToolArea2Undef()
        {
            SensibleArea2_free.SetActive(false);
        }
        
        
        void OnButtonArea1Busy()
        {
            int idx = 0;
            for (int i = 0; i < _sensibleAreasRTsWRTProjector.Count; i++)
            {
                if (_sensibleAreasRTsWRTProjector[i].Key == BUTTON1AREAID)
                {
                    idx = i;
                }
            }
            Matrix4x4 rtMatrix4X4 = new Matrix4x4();
            for (int i = 0; i < 4; i++)
            {
                rtMatrix4X4.SetRow(i,
                    new Vector4((float) _sensibleAreasRTsWRTProjector[idx].Value.get(i , 0)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 1)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 2)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 3)[0]));
            }
                
            SensibleArea3_busy.transform.localPosition =
                Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
            
            SensibleArea3_busy.transform.localRotation =
                Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
            SensibleArea3_busy.SetActive(true);
            SensibleArea3_free.SetActive(false);
            SensibleArea3_undef.SetActive(false);

        }
        
        void OnButtonArea1Pressed(int perc)
        {
            int idx = 0;
            for (int i = 0; i < _sensibleAreasRTsWRTProjector.Count; i++)
            {
                if (_sensibleAreasRTsWRTProjector[i].Key == BUTTON1AREAID)
                {
                    idx = i;
                }
            }
            Matrix4x4 rtMatrix4X4 = new Matrix4x4();
            for (int i = 0; i < 4; i++)
            {
                rtMatrix4X4.SetRow(i,
                    new Vector4((float) _sensibleAreasRTsWRTProjector[idx].Value.get(i , 0)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 1)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 2)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 3)[0]));
            }

            SensibleArea3_pressed0.SetActive(false);
            SensibleArea3_pressed25.SetActive(false);
            SensibleArea3_pressed50.SetActive(false);
            SensibleArea3_pressed75.SetActive(false);
            SensibleArea3_free.SetActive(false);
            SensibleArea3_free.SetActive(false);
            SensibleArea3_free.SetActive(false);
            SensibleArea3_undef.SetActive(false);
            
            if (perc > 75)
            {
                SensibleArea3_pressed75.transform.localPosition =
                    Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);

                SensibleArea3_pressed75.transform.localRotation =
                    Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                SensibleArea3_pressed75.SetActive(true);
            }
            else if (perc > 50)
            {
                SensibleArea3_pressed50.transform.localPosition =
                    Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);

                SensibleArea3_pressed50.transform.localRotation =
                    Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                SensibleArea3_pressed50.SetActive(true);
            }
            else if (perc > 25)
            {
                SensibleArea3_pressed25.transform.localPosition =
                    Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);

                SensibleArea3_pressed25.transform.localRotation =
                    Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                SensibleArea3_pressed25.SetActive(true);
            }
            else if (perc >0)
            {
                SensibleArea3_pressed0.transform.localPosition =
                    Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);

                SensibleArea3_pressed0.transform.localRotation =
                    Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
                SensibleArea3_pressed0.SetActive(true);
            }

        }
        
        void OnButtonArea1Free()
        {
            int idx = 0;
            for (int i = 0; i < _sensibleAreasRTsWRTProjector.Count; i++)
            {
                if (_sensibleAreasRTsWRTProjector[i].Key == BUTTON1AREAID)
                {
                    idx = i;
                }
            }
            Matrix4x4 rtMatrix4X4 = new Matrix4x4();
            for (int i = 0; i < 4; i++)
            {
                rtMatrix4X4.SetRow(i,
                    new Vector4((float) _sensibleAreasRTsWRTProjector[idx].Value.get(i , 0)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 1)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 2)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 3)[0]));
            }
            
            SensibleArea3_pressed0.SetActive(false);
            SensibleArea3_pressed25.SetActive(false);
            SensibleArea3_pressed50.SetActive(false);
            SensibleArea3_pressed75.SetActive(false);
            SensibleArea3_free.SetActive(false);
            SensibleArea3_free.SetActive(false);
            SensibleArea3_free.SetActive(false);
            SensibleArea3_undef.SetActive(false);
            
                
            SensibleArea3_free.transform.localPosition =
                Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
            
            SensibleArea3_free.transform.localRotation =
                Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
            SensibleArea3_free.SetActive(true);
            
        }
        
        void OnButtonArea1Undef()
        {
            int idx = 0;
            for (int i = 0; i < _sensibleAreasRTsWRTProjector.Count; i++)
            {
                if (_sensibleAreasRTsWRTProjector[i].Key == BUTTON1AREAID)
                {
                    idx = i;
                }
            }
            Matrix4x4 rtMatrix4X4 = new Matrix4x4();
            for (int i = 0; i < 4; i++)
            {
                rtMatrix4X4.SetRow(i,
                    new Vector4((float) _sensibleAreasRTsWRTProjector[idx].Value.get(i , 0)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 1)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 2)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 3)[0]));
            }
                
            SensibleArea3_pressed0.SetActive(false);
            SensibleArea3_pressed25.SetActive(false);
            SensibleArea3_pressed50.SetActive(false);
            SensibleArea3_pressed75.SetActive(false);
            SensibleArea3_free.SetActive(false);
            SensibleArea3_free.SetActive(false);
            SensibleArea3_free.SetActive(false);
            SensibleArea3_undef.SetActive(false);
            
            SensibleArea3_undef.transform.localPosition =
                Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
            
            SensibleArea3_undef.transform.localRotation =
                Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
            SensibleArea3_undef.SetActive(true);
        }
        void OnButtonArea2Busy()
        {
            SensibleArea4_free.SetActive(true);
        }
        void OnButtonArea2Pressed(int perc)
        {
            SensibleArea4_free.SetActive(false);
        }
        void OnButtonArea2Free()
        {
            SensibleArea4_free.SetActive(false);
        }
        void OnButtonArea2Undef()
        {
            SensibleArea4_free.SetActive(false);
        }


        void OnWorkingAreaBusy()
        {
            

            int idx = -1;
            for (int i = 0; i < _sensibleAreasRTsWRTProjector.Count; i++)
            {
                if (_sensibleAreasRTsWRTProjector[i].Key == WORKINGAREAID)
                {
                    idx = i;
                }
            }

            Matrix4x4 rtMatrix4X4 = new Matrix4x4();
            for (int i = 0; i < 4; i++)
            {
                rtMatrix4X4.SetRow(i,
                    new Vector4((float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 0)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 1)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 2)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 3)[0]));
            }

            WorkingArea_busy.transform.localPosition =
                Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);

            WorkingArea_busy.transform.localRotation =
                Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
            WorkingArea_busy.SetActive(true);
            WorkingArea_free.SetActive(false);
            WorkingArea_undef.SetActive(false);
            
        }
        
        void OnWorkingAreaFree()
        {
            

            int idx = -1;
            for (int i = 0; i < _sensibleAreasRTsWRTProjector.Count; i++)
            {
                if (_sensibleAreasRTsWRTProjector[i].Key == WORKINGAREAID)
                {
                    idx = i;
                }
            }

            Matrix4x4 rtMatrix4X4 = new Matrix4x4();
            for (int i = 0; i < 4; i++)
            {
                rtMatrix4X4.SetRow(i,
                    new Vector4((float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 0)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 1)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 2)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 3)[0]));
            }

            WorkingArea_free.transform.localPosition =
                Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);

            WorkingArea_free.transform.localRotation =
                Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
            WorkingArea_free.SetActive(true);
            WorkingArea_busy.SetActive(false);
            WorkingArea_undef.SetActive(false);
            
        }
        
        void OnWorkingAreaUndef()
        {
            

            int idx = -1;
            for (int i = 0; i < _sensibleAreasRTsWRTProjector.Count; i++)
            {
                if (_sensibleAreasRTsWRTProjector[i].Key == WORKINGAREAID)
                {
                    idx = i;
                }
            }

            Matrix4x4 rtMatrix4X4 = new Matrix4x4();
            for (int i = 0; i < 4; i++)
            {
                rtMatrix4X4.SetRow(i,
                    new Vector4((float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 0)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 1)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 2)[0],
                        (float) _sensibleAreasRTsWRTProjector[idx].Value.get(i, 3)[0]));
            }

            WorkingArea_undef.transform.localPosition =
                Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);

            WorkingArea_undef.transform.localRotation =
                Utils.ExtractRotationFromMatrix(ref rtMatrix4X4);
            WorkingArea_undef.SetActive(true);
            WorkingArea_busy.SetActive(false);
            WorkingArea_free.SetActive(false);
            
        }
        
        int RefreshWorkingAreaStatus(int idObj, int status)
        {
            int ret = -1;

            workingArea.objectID = idObj;
            
            status=workingArea.SetStatus(status);
            if (status == TOOLAREA_FREE)
                OnWorkingAreaFree();
            else if (status == TOOLAREA_BUSY)
                OnWorkingAreaBusy();
            else if (status == TOOLAREA_UNDEF)
                OnWorkingAreaUndef();
            
           
            ret = 0;
            
            return ret;
        }
        
        
        
        int RefreshToolAreaStatus(int id, int status)
        {
            int ret = -1;
            
            if (id == TOOLAREA1ID)
            {
                status=toolArea1.SetStatus(status);
//                if (status == TOOLAREA_FREE)
//                    OnToolArea1Free();
//                else if (status == TOOLAREA_BUSY)
//                    OnToolArea1Busy();
//                else if (status == TOOLAREA_UNDEF)
//                    OnToolArea1Undef();
            }
            else if (id == TOOLAREA2ID)
            {
                status=toolArea2.SetStatus(status);
//                if (status == TOOLAREA_FREE)
//                    OnToolArea2Free();
//                else if (status == TOOLAREA_BUSY)
//                    OnToolArea2Busy();
//                else if (status == TOOLAREA_UNDEF)
//                    OnToolArea2Undef();
            }

            ret = 0;
            
            return ret;
        }
        
        int RefreshButtonAreaStatus(int id, int status)
        {
            int ret = -1;
            
            if (id == BUTTON1AREAID)
            {
                int statusPerc=buttonArea1.SetStatus(status);
//                if (statusPerc == 0)
//                    OnButtonArea1Free();
//                else if (statusPerc > 0)
//                    OnButtonArea1Pressed(statusPerc);
//                else if (statusPerc ==100)
//                    OnButtonArea1Busy();
//                else
//                    OnButtonArea1Undef();
            }
            else if (id == BUTTON2AREAID)
            {
                status=buttonArea2.SetStatus(status);
//                if (status == TOOLAREA_FREE)
//                    OnButtonArea2Free();
//                else if (status == TOOLAREA_BUSY)
//                    OnButtonArea2Busy();
//                else if (status == TOOLAREA_UNDEF)
//                    OnButtonArea2Undef();
            }

            ret = 0;
            return ret;
        }
        
        int RemoveCalibrationParams(ref Mat rtP_C,string fileName)
        {
            int ret = -1;
            
            if (File.Exists(fileName))
            {
// loading all the text out of the file into a string, assuming the text is all JSON
                File.Move(fileName, fileName + "." + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute+DateTime.Now.Second+".bak");
//                File.Delete(fileName);

                ret = 0;
            }

            return ret;
        }

        int LoadCalibrationParams(ref Mat rtP_C,string fileName)
        {
            int ret = -1;
            
            if (File.Exists(fileName))
            {
// loading all the text out of the file into a string, assuming the text is all JSON
                string json = File.ReadAllText(fileName); 

                rtP_C = ConvertJsonToRTMatrix(json);

                ret = 0;
                _calibrationParamsLoaded = true;
            }

            return ret;
        }
        
        int SaveCalibrationParams(Mat rtP_C,string fileName)
        {
            int ret = 0;
            
            //Debug.Log("Saving: "+rtP_C.get(0,0)[0]);
            
           
            string json = ConvertRTMatrixToJson(rtP_C);//JsonUtility.ToJson(rtP_C);
            
            Debug.Log("As JSON: "+json);
            
            StreamWriter sw = File.CreateText(fileName); // if file doesnt exist, make the file in the specified path
            sw.Close();

            File.WriteAllText(fileName, json); // fill the file with the data(json)
            
            return ret;
        }

        int RemoveConfigureWorkBenchParams(ref List<KeyValuePair<int, Mat>> sensibleAreasCoords,ref List<KeyValuePair<int, Mat>> sensibleAreasRTs,string fileName)
        {
            int ret = -1;
            if (File.Exists(fileName))
            {
                //delete  the config file
                File.Move(fileName, fileName + "." + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute+DateTime.Now.Second+".bak");
//                File.Delete(fileName);
                
                sensibleAreasCoords.Clear();
                sensibleAreasRTs.Clear();
                

            }

            return ret;
        }
        
        
        int LoadConfigureWorkBenchParams(ref List<KeyValuePair<int, Mat>> sensibleAreasCoords,ref List<KeyValuePair<int, Mat>> sensibleAreasRTs,string fileName)
        {
            int ret = -1;
            if (File.Exists(fileName))
            {
                // loading all the text out of the file into a string, assuming the text is all JSON

                string json = File.ReadAllText(fileName); 

                // parse the JSON into an ActorContainer type and return in the LoadActors
                sensibleAreasCoords.Clear();
                sensibleAreasRTs.Clear();
                ret =  ConvertJsonToSensibleAreas(json,ref sensibleAreasCoords,ref sensibleAreasRTs);
                _configureWorkBenchParamsLoaded = true;
            }

            return ret;
        }
        
        int SaveConfigureWorkBenchParams(List<KeyValuePair<int, Mat>> sensibleAreasCoords,List<KeyValuePair<int, Mat>> sensibleAreasRTs,string fileName)
        {
            int ret = 0;
            
            string json = ConvertSensibleAreasToJson(sensibleAreasCoords,sensibleAreasRTs);//JsonUtility.ToJson(rtP_C);
            
            Debug.Log("As JSON: "+json);
            
            StreamWriter sw = File.CreateText(fileName); // if file doesnt exist, make the file in the specified path
            sw.Close();

            File.WriteAllText(fileName, json); // fill the file with the data(json)
            return ret;
        }

        


        Mat ConvertJsonToRTMatrix(string jsonString)
        {
            
            RTMatrix rtMatrix = JsonUtility.FromJson<RTMatrix>(jsonString);

            Mat matrixOut = new Mat(4, 4, CvType.CV_64FC1);
            
            matrixOut.put(0, 0, rtMatrix.r00);
            matrixOut.put(0, 1, rtMatrix.r01);
            matrixOut.put(0, 2, rtMatrix.r02);
            matrixOut.put(0, 3, rtMatrix.t00);
            matrixOut.put(1, 0, rtMatrix.r10);
            matrixOut.put(1, 1, rtMatrix.r11);
            matrixOut.put(1, 2, rtMatrix.r12);
            matrixOut.put(1, 3, rtMatrix.t10);
            matrixOut.put(2, 0, rtMatrix.r20);
            matrixOut.put(2, 1, rtMatrix.r21);
            matrixOut.put(2, 2, rtMatrix.r22);
            matrixOut.put(2, 3, rtMatrix.t20);
            matrixOut.put(3, 0, rtMatrix.r30);
            matrixOut.put(3, 1, rtMatrix.r31);
            matrixOut.put(3, 2, rtMatrix.r32);
            matrixOut.put(3, 3, rtMatrix.t30);

            return matrixOut;
        }

        

        string ConvertRTMatrixToJson(Mat matrix)
        {

            RTMatrix rtMatrix=new RTMatrix();
            rtMatrix.id = -1;
            rtMatrix.r00 = matrix.get(0, 0)[0];
            rtMatrix.r01 = matrix.get(0, 1)[0];
            rtMatrix.r02 = matrix.get(0, 2)[0];
            rtMatrix.t00 = matrix.get(0, 3)[0];
            rtMatrix.r10 = matrix.get(1, 0)[0];
            rtMatrix.r11 = matrix.get(1, 1)[0];
            rtMatrix.r12 = matrix.get(1, 2)[0];
            rtMatrix.t10 = matrix.get(1, 3)[0];
            rtMatrix.r20 = matrix.get(2, 0)[0];
            rtMatrix.r21 = matrix.get(2, 1)[0];
            rtMatrix.r22 = matrix.get(2, 2)[0];
            rtMatrix.t20 = matrix.get(2, 3)[0];            
            rtMatrix.r30 = matrix.get(3, 0)[0];
            rtMatrix.r31 = matrix.get(3, 1)[0];
            rtMatrix.r32 = matrix.get(3, 2)[0];
            rtMatrix.t30 = matrix.get(3, 3)[0];

            string jsonString = JsonUtility.ToJson(rtMatrix,true);
            
            return jsonString;
        }
        
        string ConvertSensibleAreasToJson(List<KeyValuePair<int, Mat>> sensibleAreasCoords,List<KeyValuePair<int, Mat>> sensibleAreasRTs)
        {
            AreasCoords areasCoords = new AreasCoords();

            for (int i = 0; i < sensibleAreasCoords.Count; i++)
            {
                AreaCoords areaCoords = new AreaCoords();
                areaCoords.id = sensibleAreasCoords[i].Key;
                areaCoords.x0 = sensibleAreasCoords[i].Value.get(0, 0)[0];
                areaCoords.y0 = sensibleAreasCoords[i].Value.get(0, 1)[0];
                areaCoords.x1 = sensibleAreasCoords[i].Value.get(1, 0)[0];
                areaCoords.y1 = sensibleAreasCoords[i].Value.get(1, 1)[0];
                areaCoords.x2 = sensibleAreasCoords[i].Value.get(2, 0)[0];
                areaCoords.y2 = sensibleAreasCoords[i].Value.get(2, 1)[0];
                areaCoords.x3 = sensibleAreasCoords[i].Value.get(3, 0)[0];
                areaCoords.y3 = sensibleAreasCoords[i].Value.get(3, 1)[0];

                RTMatrix areaRT=new RTMatrix();
                areaRT.id = sensibleAreasRTs[i].Key;
                areaRT.r00 = sensibleAreasRTs[i].Value.get(0, 0)[0];
                areaRT.r01 = sensibleAreasRTs[i].Value.get(0, 1)[0];
                areaRT.r02 = sensibleAreasRTs[i].Value.get(0, 2)[0];
                areaRT.t00 = sensibleAreasRTs[i].Value.get(0, 3)[0];
                areaRT.r10 = sensibleAreasRTs[i].Value.get(1, 0)[0];
                areaRT.r11 = sensibleAreasRTs[i].Value.get(1, 1)[0];
                areaRT.r12 = sensibleAreasRTs[i].Value.get(1, 2)[0];
                areaRT.t10 = sensibleAreasRTs[i].Value.get(1, 3)[0];
                areaRT.r20 = sensibleAreasRTs[i].Value.get(2, 0)[0];
                areaRT.r21 = sensibleAreasRTs[i].Value.get(2, 1)[0];
                areaRT.r22 = sensibleAreasRTs[i].Value.get(2, 2)[0];
                areaRT.t20 = sensibleAreasRTs[i].Value.get(2, 3)[0];            
                areaRT.r30 = sensibleAreasRTs[i].Value.get(3, 0)[0];
                areaRT.r31 = sensibleAreasRTs[i].Value.get(3, 1)[0];
                areaRT.r32 = sensibleAreasRTs[i].Value.get(3, 2)[0];
                areaRT.t30 = sensibleAreasRTs[i].Value.get(3, 3)[0];
                
                
                areasCoords.areasCoords.Add(areaCoords);
                areasCoords.areasRTs.Add(areaRT);
            }
            
            string jsonString = JsonUtility.ToJson(areasCoords,true);
            
            return jsonString;
        }

        int ConvertJsonToSensibleAreas(string jsonString, ref List<KeyValuePair<int, Mat>> sensibleAreasCoords ,ref List<KeyValuePair<int, Mat>> sensibleAreasRTs)
        {
            AreasCoords areasCoords = JsonUtility.FromJson<AreasCoords>(jsonString);

//            List<KeyValuePair<int, Mat>> areasCoordsOut = new List<KeyValuePair<int,Mat>>();
//            List<KeyValuePair<int, Mat>> areasRTsOut = new List<KeyValuePair<int,Mat>>();
                   
            for (int i = 0; i < areasCoords.areasCoords.Count; i++)
            {
                Mat areaCoords = new Mat(4, 2, CvType.CV_64FC1);

                int id = -1;

                id = areasCoords.areasCoords[i].id;
                areaCoords.put(0, 0, areasCoords.areasCoords[i].x0);
                areaCoords.put(0, 1, areasCoords.areasCoords[i].y0);
                areaCoords.put(1, 0, areasCoords.areasCoords[i].x1);
                areaCoords.put(1, 1, areasCoords.areasCoords[i].y1);
                areaCoords.put(2, 0, areasCoords.areasCoords[i].x2);
                areaCoords.put(2, 1, areasCoords.areasCoords[i].y2);
                areaCoords.put(3, 0, areasCoords.areasCoords[i].x3);
                areaCoords.put(3, 1, areasCoords.areasCoords[i].y3);
                sensibleAreasCoords.Add(new KeyValuePair<int, Mat>(id,areaCoords));
            }

            for (int i = 0; i < areasCoords.areasRTs.Count; i++)
            {
                Mat areaRT = new Mat(4, 4, CvType.CV_64FC1);

                int id = -1;

                id = areasCoords.areasRTs[i].id;
                
                areaRT.put(0, 0, areasCoords.areasRTs[i].r00);
                areaRT.put(0, 1, areasCoords.areasRTs[i].r01);
                areaRT.put(0, 2, areasCoords.areasRTs[i].r02);
                areaRT.put(0, 3, areasCoords.areasRTs[i].t00);
                areaRT.put(1, 0, areasCoords.areasRTs[i].r10);
                areaRT.put(1, 1, areasCoords.areasRTs[i].r11);
                areaRT.put(1, 2, areasCoords.areasRTs[i].r12);
                areaRT.put(1, 3, areasCoords.areasRTs[i].t10);
                areaRT.put(2, 0, areasCoords.areasRTs[i].r20);
                areaRT.put(2, 1, areasCoords.areasRTs[i].r21);
                areaRT.put(2, 2, areasCoords.areasRTs[i].r22);
                areaRT.put(2, 3, areasCoords.areasRTs[i].t20);
                areaRT.put(3, 0, areasCoords.areasRTs[i].r30);
                areaRT.put(3, 1, areasCoords.areasRTs[i].r31);
                areaRT.put(3, 2, areasCoords.areasRTs[i].r32);
                areaRT.put(3, 3, areasCoords.areasRTs[i].t30);

                sensibleAreasRTs.Add(new KeyValuePair<int, Mat>(id,areaRT));
                
                                    Debug.Log("AREAS RT: " + id + " " + areaRT.get(0, 0)[0] + " " + areaRT.get(0, 1)[0] + " " +
                                              areaRT.get(0, 2)[0] + " " + areaRT.get(0, 3)[0]
                              + " " + areaRT.get(1, 0)[0] + " " + areaRT.get(1, 1)[0] + " " + areaRT.get(1, 2)[0] + " " +
                                              areaRT.get(1, 3)[0]
                              + " " + areaRT.get(2, 0)[0] + " " + areaRT.get(2, 1)[0] + " " + areaRT.get(2, 2)[0] + " " +
                                              areaRT.get(2, 3)[0]
                              + " " + areaRT.get(3, 0)[0] + " " + areaRT.get(3, 1)[0] + " " + areaRT.get(3, 2)[0] + " " +
                                              areaRT.get(3, 3)[0]);
            }
            
            return 0;

        }
        
        

        string ConvertSensibleAreasCoordsToJson(List<KeyValuePair<int, Mat>> sensibleAreasCoords)
        {
            AreasCoords areasCoords = new AreasCoords();

            for (int i = 0; i < sensibleAreasCoords.Count; i++)
            {
                AreaCoords areaCoords = new AreaCoords();
                areaCoords.id = sensibleAreasCoords[i].Key;
                areaCoords.x0 = sensibleAreasCoords[i].Value.get(0, 0)[0];
                areaCoords.y0 = sensibleAreasCoords[i].Value.get(0, 1)[0];
                areaCoords.x1 = sensibleAreasCoords[i].Value.get(1, 0)[0];
                areaCoords.y1 = sensibleAreasCoords[i].Value.get(1, 1)[0];
                areaCoords.x2 = sensibleAreasCoords[i].Value.get(2, 0)[0];
                areaCoords.y2 = sensibleAreasCoords[i].Value.get(2, 1)[0];
                areaCoords.x3 = sensibleAreasCoords[i].Value.get(3, 0)[0];
                areaCoords.y3 = sensibleAreasCoords[i].Value.get(3, 1)[0];

                areasCoords.areasCoords.Add(areaCoords);
            }
            
            string jsonString = JsonUtility.ToJson(areasCoords,true);
            
            return jsonString;
        }

        List<KeyValuePair<int, Mat>> ConvertJsonToSensibleAreasCoords(string jsonString)
        {
            AreasCoords areasCoords = JsonUtility.FromJson<AreasCoords>(jsonString);

            List<KeyValuePair<int, Mat>> areasCoordsOut = new List<KeyValuePair<int,Mat>>();
                   
            for (int i = 0; i < areasCoords.areasCoords.Count; i++)
            {
                Mat areaCoords = new Mat(4, 2, CvType.CV_64FC1);

                int id = -1;

                id = areasCoords.areasCoords[i].id;
                areaCoords.put(0, 0, areasCoords.areasCoords[i].x0);
                areaCoords.put(0, 1, areasCoords.areasCoords[i].y0);
                areaCoords.put(1, 0, areasCoords.areasCoords[i].x1);
                areaCoords.put(1, 1, areasCoords.areasCoords[i].y1);
                areaCoords.put(2, 0, areasCoords.areasCoords[i].x2);
                areaCoords.put(2, 1, areasCoords.areasCoords[i].y2);
                areaCoords.put(3, 0, areasCoords.areasCoords[i].x3);
                areaCoords.put(3, 1, areasCoords.areasCoords[i].y3);
                areasCoordsOut.Add(new KeyValuePair<int, Mat>(id,areaCoords));
            }

            return areasCoordsOut;

        }
        
        int SetPSNRThresholds(double toolsThr,double buttonsThr)
        {
            int ret = -1;
            ret=_nativeObject.SetPSNRThresholds(toolsThr,buttonsThr);

            return ret;
        }
        
        int SetSSIMThresholds(double toolsThr,double buttonsThr)
        {
            int ret = -1;
            ret=_nativeObject.SetSSIMThresholds(toolsThr,buttonsThr);

            return ret;
        }

        int UnCalibrate()
        {
            int ret = -1;
            ret=_nativeObject.UnCalibrate();

            return ret;
        }
        int  UnConfigureWorkBench()
        {
            int ret = -1;
            
            ret=_nativeObject.UnConfigureWorkBench();
            
            return ret;
        }
        
        int Calibrate()
        {
            int ret = -1;
            _xP_Ms.Clear();
    
            //RHS Rototranslation World to Projector xP_W
            Mat xP_M1 = new Mat(4, 4, CvType.CV_64FC1);
            xP_M1.put(0, 0, 90.0 * Mathf.Deg2Rad);
            xP_M1.put(1, 0, 0.0);
            xP_M1.put(2, 0, 0.0);
            xP_M1.put(3, 0, 0.0);
            xP_M1.put(4, 0, -0.5);
            xP_M1.put(5, 0, 2.0);

            Debug.Log(xP_M1.get(0, 0)[0] + " " + xP_M1.get(1, 0)[0] + " " + xP_M1.get(2, 0)[0] + " " +
                      xP_M1.get(3, 0)[0] + " " + xP_M1.get(4, 0)[0] + " " + xP_M1.get(5, 0)[0]);

//                        _nativeObject.SetXProjector_W(_xP_W);

            _xP_Ms.Add(new KeyValuePair<int, Mat>(10, xP_M1));
            
            Mat xP_M2 = new Mat(4, 4, CvType.CV_64FC1);
            xP_M2.put(0, 0, 90.0 * Mathf.Deg2Rad);
            xP_M2.put(1, 0, 0.0);
            xP_M2.put(2, 0, 0.0);
            xP_M2.put(3, 0, 0.25);
            xP_M2.put(4, 0, -0.5);
            xP_M2.put(5, 0, 2.0);

            Debug.Log(xP_M2.get(0, 0)[0] + " " + xP_M2.get(1, 0)[0] + " " + xP_M2.get(2, 0)[0] + " " +
                      xP_M2.get(3, 0)[0] + " " + xP_M2.get(4, 0)[0] + " " + xP_M2.get(5, 0)[0]);

            //                        _nativeObject.SetXProjector_W(_xP_W);

            _xP_Ms.Add(new KeyValuePair<int, Mat>(11, xP_M2));
            
            Mat xP_M3 = new Mat(4, 4, CvType.CV_64FC1);
            xP_M3.put(0, 0, 90.0 * Mathf.Deg2Rad);
            xP_M3.put(1, 0, 0.0);
            xP_M3.put(2, 0, 0.0);
            xP_M3.put(3, 0, -0.25);
            xP_M3.put(4, 0, -0.5);
            xP_M3.put(5, 0, 2.0);

            Debug.Log(xP_M3.get(0, 0)[0] + " " + xP_M3.get(1, 0)[0] + " " + xP_M3.get(2, 0)[0] + " " +
                      xP_M3.get(3, 0)[0] + " " + xP_M3.get(4, 0)[0] + " " + xP_M3.get(5, 0)[0]);

            //                        _nativeObject.SetXProjector_W(_xP_W);

            _xP_Ms.Add(new KeyValuePair<int, Mat>(12, xP_M3));
            
            

            _nativeObject.SetCalibrationSetup(_xP_Ms);
            
            ret =_nativeObject.Calibrate();

            return ret;
        }
        
        int  ConfigureWorkBench()
        {
            int ret = -1;

            _sensibleAreas.Clear();
            _sensibleAreas.Add(new KeyValuePair<int, int>(workingArea.markerID, workingArea.areaID)); //markerID, areaID
            _sensibleAreas.Add(new KeyValuePair<int, int>(toolArea1.markerID, toolArea1.areaID));
            _sensibleAreas.Add(new KeyValuePair<int, int>(toolArea2.markerID, toolArea2.areaID));
            _sensibleAreas.Add(new KeyValuePair<int, int>(buttonArea1.markerID,buttonArea1.areaID));
            _sensibleAreas.Add(new KeyValuePair<int, int>(buttonArea2.markerID,buttonArea2.areaID));
            
            _nativeObject.SetConfigureWorkBenchSetup(_sensibleAreas);

            ret = _nativeObject.ConfigureWorkBench();
            
            return ret;
        }
        
        
        int SetEnableObjectDetectionAndTracking(bool enabled_)
        {
            return _nativeObject.SetEnableObjectDetectionAndTracking(enabled_);
        }

        
        int SetEnableToolsDetection(bool enabled_)
        {
            return _nativeObject.SetEnableToolsDetection(enabled_);
        }
        
        int SetEnableButtonsDetection(bool enabled_)
        {
            return _nativeObject.SetEnableButtonsDetection(enabled_);
        }


        
        public void OnSetup()
        {
//            StartProjectCalibrationMarkers(_xP_Ms);
            SetMenu(SETUP_MENU_ID);
            ActivateMenu();
//            .SetActive(false);
//            RestartMenu.SetActive(false);
//            SetupMenu.SetActive(true);
//            _setupMenuActivated = true;
        }
        
        
        public void OnUnCalibratePressed()
        {
            RemoveCalibrationParams(ref _rtP_C,_calibDataFilename);
            DeactivateMenu();
//            SetupMenu.SetActive(false);
//            MainMenu.SetActive(false);
//            RestartMenu.SetActive(false);
//            ButtonObjectDetectionAndTrackingStart.SetActive(false);
//            ButtonUnCalibrate.SetActive(false);
//            ButtonUnConfigureWorkbench.SetActive(false);
//            ButtonZoomAdjustStart.SetActive(false);
//            ButtonZoomAdjustStop.SetActive(false);
            UnCalibrate();
            
        }
        
        public void OnUnConfigureWorkbenchPressed()
        {
            RemoveConfigureWorkBenchParams(ref _sensibleAreasCoords,ref _sensibleAreasRTs,_configWorkBenchDataFilename);
            DeactivateMenu();
//            SetupMenu.SetActive(false);
//            MainMenu.SetActive(false);
//            RestartMenu.SetActive(false);
//            ButtonObjectDetectionAndTrackingStart.SetActive(false);
//            ButtonUnCalibrate.SetActive(false);
//            ButtonUnConfigureWorkbench.SetActive(false);
//            ButtonZoomAdjustStart.SetActive(false);
//            ButtonZoomAdjustStop.SetActive(false);
            UnConfigureWorkBench();
        }

        public void OnZoomAdjustStart()
        {
            StartProjectCalibrationMarkers(_xP_Ms);
        }
        
        public void OnZoomAdjustStop()
        {
            StopProjectCalibrationMarkers();
        }

        public void OnObjectDetectionAndTrackingStart()
        {
//            ButtonObjectDetectionAndTrackingStart.SetActive(false);
//            ButtonUnCalibrate.SetActive(false);
//            ButtonUnConfigureWorkbench.SetActive(false);
//            ButtonZoomAdjustStart.SetActive(false);
//            ButtonZoomAdjustStop.SetActive(false);
            
            DeactivateMenu();
            
            _nativeObject.ResetToolsAreas();
            
//            MainMenu.SetActive(false);
//            SetupMenu.SetActive(false);
//            RestartMenu.SetActive(false);
            _startObjectDetectionAndTracking = true;
        }
       
        
//        void ProcessFrameWork()
//        {
////            Mat emptyFrame=new Mat(0,0,CvType.CV_64F);
//
//            int ret = -1;
//            try
//            {
//                if (_initialized)
//                {
//
//                    internalState_ = _nativeObject.GetFSMState();
//                    if (internalState_ == STATE_CHECK)
//                    {
//                        Debug.Log("EXECUTE STATE_CHECK");
//                        ret = LoadCalibrationParams(ref _rtP_C, _calibDataFilename);
//                        if (ret == 0)
//                        {
//                            //IF LOAD CALIBRATION PARAMS FROM FILE
//                            ret=_nativeObject.SetCalibrationParams(_rtP_C);
//                            ret = 0;
//                        }
//                        else
//                        {
//                            StartProjectCalibrationMarkers(_xP_Ms);
////                            _calibrationStarted = true;
//                            ret = 0;
//                        }
//                    }
//                    else
//                    if (internalState_ == STATE_UNCALIBRATED)
//                    {
//                        Debug.Log("EXECUTE STATE_UNCALIBRATED");
//                        //LOAD CALIB PARAMS FROM FILE (IF EXISTS)
//                        
//
////                        ret = LoadCalibrationParams(ref _rtP_C,_calibDataFilename);
////                        if (ret == 0)
////                        {
////                               //IF LOAD CALIBRATION PARAMS FROM FILE
////                            ret=_nativeObject.SetCalibrationParams(_rtP_C);
////                        }
////                        else
////                        {
////                               
////                            //IF DO NOT LOAD CALIBRATION PARAMS FROM FILE
////                            if (!_calibrationStarted)
////                            {
////                                StartProjectCalibrationMarkers(_xP_Ms);
////                                _calibrationStarted = true;
////                                ret = 0;
////                            }
////                            else
////                            {
//
//                                //AUTOMATIC CALIBRATION START
//                                ret=Calibrate();
//                                _calibrationStarted = false;
//                                StopProjectCalibrationMarkers();
////                            }
//
////                        }
//
//                    }
////                    else if (internalState_ == STATE_CALIBRATE)
////                    {
////                        StartProjectCalibrationMarkers(_xP_Ms);
////                    }
//                    else if (internalState_ == STATE_CALIBRATED)
//                    {
//                        Debug.Log("EXECUTE STATE_CALIBRATED");
//                        StopProjectCalibrationMarkers();
//
//                        _nativeObject.GetCalibrationParams(_rtP_C);
//
//                        ret=SaveCalibrationParams(_rtP_C,_calibDataFilename);
//
//                        ButtonObjectDetectionAndTrackingStart.SetActive(true);
//                        ButtonUnCalibrate.SetActive(true);
//                        ButtonUnConfigureWorkbench.SetActive(true);
//                        ButtonZoomAdjustStart.SetActive(true);
//                        ButtonZoomAdjustStop.SetActive(true);
//                        
//                        if (ret == 0)
//                        {
//                            ret = LoadConfigureWorkBenchParams(ref _sensibleAreasCoords,ref _sensibleAreasRTs,_configWorkBenchDataFilename);
//                            if (ret == 0)
//                            {
//                                _nativeObject.SetConfigureWorkBenchParams(_sensibleAreasCoords,_sensibleAreasRTs);
//
//                               
//                            }
//                            
//                        }
//
//                    }
////                    else if (internalState_ == STATE_CONFIGURE_WORKBENCH)
////                    {
////                        
////                    }
//                    else if (internalState_ == STATE_UNCONFIGURED_WORKBENCH) 
//                    {
//                        StopProjectCalibrationMarkers();
//                        Debug.Log("EXECUTE STATE_UNCONFIGURED_WORKBENCH");
//                        
//                            //IF DO NOTLOAD CONFIGURATION PARAMS FROM FILE
//                            ret = ConfigureWorkBench();
//                
//                      
//                    }
//                    else if (internalState_ == STATE_CONFIGURED_WORKBENCH) 
//                    {
//                        Debug.Log("EXECUTE STATE_CONFIGURED_WORKBENCH");
//                        _nativeObject.GetConfigureWorkBenchParams(_sensibleAreasCoords,_sensibleAreasRTs);
//
//                        ret=SaveConfigureWorkBenchParams(_sensibleAreasCoords,_sensibleAreasRTs,_configWorkBenchDataFilename);
//
//                        ret = _nativeObject.GetSensibleAreasRTMatrixWRTProjector(_sensibleAreasRTsWRTProjector);
//                     
//                        StopProjectCalibrationMarkers();
//                        ButtonObjectDetectionAndTrackingStart.SetActive(true);
//                        ButtonUnCalibrate.SetActive(true);
//                        ButtonUnConfigureWorkbench.SetActive(true);
//                        ButtonZoomAdjustStart.SetActive(true);
//                        ButtonZoomAdjustStop.SetActive(true);
//
//                    }
//                    else if (internalState_ == STATE_READY)
//                    {
//                        Debug.Log("EXECUTE STATE_READY");
//                        StopProjectCalibrationMarkers();
//                        //AUTOMATIC OBJECT DETECTION AND TRACKING START
//                        if (_startObjectDetectionAndTracking)
//                            ret=SetEnableObjectDetectionAndTracking(true);
//                        
//                        else
//                        {
//                            
//                            ret = 0;
//                        }
//
////                        if (_waitObjectDetectionAndTrackingStart>MAX_TIME_ODT_START)
////                            ret=SetEnableObjectDetectionAndTracking(true);
////                        else
////                        {
////                            _waitObjectDetectionAndTrackingStart++;
//////                            ret=SetEnableObjectDetectionAndTracking(false);
////                        }
//
//                    }
//                    else if (internalState_ == STATE_MARKERDETECT)
//                    {
//                        Debug.Log("EXECUTE STATE_MARKERDETECT");
//                        MarkerDetection();
//                    }
//                    else if (internalState_ == STATE_OBJDETECTANDTRACK)
//                    {
//                        Debug.Log("EXECUTE STATE_OBJDETECTANDTRACK");
//                        ret=ObjectDetectionAndTracking();
//                        if (ret==0)
//                            ret=CheckSensibleAreas();
//                    }
//
//
//                    _nativeObject.ExecuteFSMState();
//
//                }
//                else
//                {
//                    _waitForZoomAdjustment++;
//                    if (_waitForZoomAdjustment > MAX_TIME_ZOOM_ADJUST) 
//                    {
//                        _initialized = true;
//                        StopProjectCalibrationMarkers();
//                    }
//                    else
//                    {
//                        StartProjectCalibrationMarkers(_xP_Ms);
//
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Debug.Log(e.StackTrace);
//            }
//        }    


        int ExecuteStatusUndef()
        {
            Debug.Log("EXECUTE ExecuteStatusUndef");
            int ret = -1;
            ret=SetupWorkbench();
            if (ret == 0)
            {
                ret=EnableWorkingStep(0);
                if (ret == 0)
                {
                    SetTimer(STEP_WAIT_TIME);
                    ret = 1;
                }
            }

            return ret;
            
        }
        
        int ExecuteStatusCompleted()
        {
            Debug.Log("EXECUTE ExecuteStatusCompleted");
            int ret = -1;
            if (Tick() == true)
            {

                ret = 1;
            }
            else
            {
                ret = EnableWorkingStep(8);
                ret = 0;
            }
            return ret;
        }

        int ExecuteStatusPushButton()
        {
            Debug.Log("EXECUTE ExecuteStatusPushButton");
            int ret = -1;            
            ret = SetEnableButtonsDetection(true);
            
            if (ret == 0)
            {
                int numButtonsFound = CheckButtonsAreas(_processStatus);
                Debug.Log("EXECUTE ExecuteStatusPushButton: "+buttonArea1.GetPercent());
                if (numButtonsFound == 1)
                {

                    if (buttonArea1.GetPercent() == 100)
                    {
                        ret = SetEnableButtonsDetection(false);
                        SetTimer(RESTART_WAIT_TIME);
                        if (ret == 0)
                            ret = 1;
                    }
                    else if (buttonArea1.GetPercent() >= 75)
                    {
                        ret = EnableWorkingStep(7);
                        if (ret == 0)
                            ret = 0;
                    }
                    else if (buttonArea1.GetPercent() >= 50)
                    {
                        ret = EnableWorkingStep(6);
                        if (ret == 0)
                            ret = 0;
                    }
                    else if (buttonArea1.GetPercent() >= 25)
                    {
                        ret = EnableWorkingStep(5);
                        if (ret == 0)
                            ret = 0;
                    }
                    else if (buttonArea1.GetPercent() > 0)
                    {
                        ret = EnableWorkingStep(4);
                        if (ret == 0)
                            ret = 0;
                    }
                    else
                    {
                        ret = EnableWorkingStep(4);
                        if (ret == 0)
                            ret = 0;
                    }
                }
                else if (numButtonsFound == 0)
                {
                    ret = EnableWorkingStep(4);
                    if (ret == 0)
                        ret = 0;
                }
                else
                {
                    ret = EnableWorkingStep(4);
                    if (ret == 0)
                        ret = -1;
                }
            }

            return ret;
        }

        int ExecuteStatusFindTools()
        {
            Debug.Log("EXECUTE ExecuteStatusFindTools");
            int ret = -1;
            ret = SetEnableToolsDetection(true);
            if (ret == 0)
            {
                int numToolsFound = CheckToolsAreas(_processStatus);
                if (numToolsFound == 1)
                {
                    if (Tick() == true)
                    {
                        ret = SetEnableToolsDetection(false);
                        if (ret == 0)
                        {
                            
                            SetTimer(STEP_WAIT_TIME);
                            ret = 1;
                        }
                    }
                    else
                    {
                        ret = EnableWorkingStep(3);
                        if (ret == 0)
                            ret = 0;
                    }
                }
                else if (numToolsFound == 0)
                {
                    SetTimer(STEP_WAIT_TIME);
                    ret = EnableWorkingStep(2);
                    if (ret == 0)
                        ret = 0;
                }
                else
                {
                    ret = EnableWorkingStep(2);
                    if (ret == 0)
                        ret = -1;
                }
            }

            return ret;
        }

        int ExecuteStatusFindObj()
        {
            Debug.Log("EXECUTE ExecuteStatusFindObj");
            int ret = -1;
            ret = SetEnableObjectDetectionAndTracking(true);
            if (ret == 0)
            {
                int numObjFound = ObjectDetectionAndTracking(_processStatus);
                if (numObjFound == 1)
                {
                    if (Tick() == true)
                    {

                        ret = SetEnableObjectDetectionAndTracking(false);
                        if (ret == 0)
                        {
                            SetTimer(STEP_WAIT_TIME);
                            ret = 1;
                        }
                    }
                    else
                    {
                        ret=EnableWorkingStep(1);
                        if (ret==0)
                            ret = 0;
                    }
                }
                else if (numObjFound == 0)
                {

                    SetTimer(STEP_WAIT_TIME);
                    ret=EnableWorkingStep(0);
                    if (ret == 0)
                        ret = 0;
                }
                else
                {
                    ret=EnableWorkingStep(0);
                    if (ret==0)
                        ret = -1;
                }
            }

            return ret;
        }

        int EnableWorkingStep(int numStep)
        {
            int ret = -1;
            Debug.Log("EnableWorkingStep: "+numStep);
            if (numStep == 0)
            {
                SensibleArea1_free.SetActive(false);                
                SensibleArea1_busy.SetActive(false);
                SensibleArea1_undef.SetActive(false);
//                SensibleArea2_free.SetActive(false);
//                SensibleArea2_busy.SetActive(false);
//                SensibleArea2_undef.SetActive(false);
                SensibleArea3_free.SetActive(false);
                SensibleArea3_busy.SetActive(false);
                SensibleArea3_undef.SetActive(false);
                SensibleArea3_pressed0.SetActive(false);
                SensibleArea3_pressed25.SetActive(false);
                SensibleArea3_pressed50.SetActive(false);
                SensibleArea3_pressed75.SetActive(false);
//                SensibleArea4_free.SetActive(false);
//                SensibleArea4_busy.SetActive(false);
//                SensibleArea4_undef.SetActive(false);                
//                SensibleArea4_pressed0.SetActive(false);
//                SensibleArea4_pressed25.SetActive(false);
//                SensibleArea4_pressed50.SetActive(false);
//                SensibleArea4_pressed75.SetActive(false);
                
                WorkingArea_free.SetActive(true);
                
                WorkingArea_busy.SetActive(false);
                WorkingArea_undef.SetActive(false);
                TextArea1_A.SetActive(true);
                
                TextArea1_B.SetActive(false);
                TextArea1_C.SetActive(false);
                TextArea1_D.SetActive(false);
                TextArea2_A.SetActive(false);
                
                
                ret = 0;
            }
            else if (numStep == 1)
            {
                SensibleArea1_free.SetActive(false);
                SensibleArea1_busy.SetActive(false);
                SensibleArea1_undef.SetActive(false);
//                SensibleArea2_free.SetActive(false);
//                SensibleArea2_busy.SetActive(false);
//                SensibleArea2_undef.SetActive(false);
                SensibleArea3_free.SetActive(false);
                SensibleArea3_busy.SetActive(false);
                SensibleArea3_undef.SetActive(false);
                SensibleArea3_pressed0.SetActive(false);
                SensibleArea3_pressed25.SetActive(false);
                SensibleArea3_pressed50.SetActive(false);
                SensibleArea3_pressed75.SetActive(false);
//                SensibleArea4_free.SetActive(false);
//                SensibleArea4_busy.SetActive(false);
//                SensibleArea4_undef.SetActive(false);
//                SensibleArea4_pressed0.SetActive(false);
//                SensibleArea4_pressed25.SetActive(false);
//                SensibleArea4_pressed50.SetActive(false);
//                SensibleArea4_pressed75.SetActive(false);
                WorkingArea_free.SetActive(false);
                
                WorkingArea_busy.SetActive(true);
                
                WorkingArea_undef.SetActive(false);
                TextArea1_A.SetActive(false);                
                TextArea1_B.SetActive(false);                
                TextArea1_C.SetActive(false);
                TextArea1_D.SetActive(false);
                
                TextArea2_A.SetActive(true);
                
                ret = 0;
            }
            else if (numStep == 2)
            {
                SensibleArea1_free.SetActive(true);
                
                SensibleArea1_busy.SetActive(false);
                SensibleArea1_undef.SetActive(false);               
//                SensibleArea2_free.SetActive(false);                
//                SensibleArea2_busy.SetActive(false);
//                SensibleArea2_undef.SetActive(false);
                SensibleArea3_free.SetActive(false);
                SensibleArea3_busy.SetActive(false);
                SensibleArea3_undef.SetActive(false);
                SensibleArea3_pressed0.SetActive(false);
                SensibleArea3_pressed25.SetActive(false);
                SensibleArea3_pressed50.SetActive(false);
                SensibleArea3_pressed75.SetActive(false);
//                SensibleArea4_free.SetActive(false);
//                SensibleArea4_busy.SetActive(false);
//                SensibleArea4_undef.SetActive(false);
//                SensibleArea4_pressed0.SetActive(false);
//                SensibleArea4_pressed25.SetActive(false);
//                SensibleArea4_pressed50.SetActive(false);
//                SensibleArea4_pressed75.SetActive(false);
                WorkingArea_free.SetActive(false);
                WorkingArea_busy.SetActive(false);
                
                WorkingArea_undef.SetActive(true);
                TextArea1_A.SetActive(false);
                
                TextArea1_B.SetActive(true);                
                
                TextArea1_C.SetActive(false);
                TextArea1_D.SetActive(false);
                
                TextArea2_A.SetActive(true);
                
                
                ret = 0;
            }
            else if (numStep == 3)
            {
                SensibleArea1_free.SetActive(false);
                
                SensibleArea1_busy.SetActive(true);                
                
                SensibleArea1_undef.SetActive(false);                
//                SensibleArea2_free.SetActive(false);                
//                SensibleArea2_busy.SetActive(false);                
//                SensibleArea2_undef.SetActive(false);
                SensibleArea3_free.SetActive(false);
                SensibleArea3_busy.SetActive(false);
                SensibleArea3_undef.SetActive(false);
                SensibleArea3_pressed0.SetActive(false);
                SensibleArea3_pressed25.SetActive(false);
                SensibleArea3_pressed50.SetActive(false);
                SensibleArea3_pressed75.SetActive(false);
//                SensibleArea4_free.SetActive(false);
//                SensibleArea4_busy.SetActive(false);
//                SensibleArea4_undef.SetActive(false);
//                SensibleArea4_pressed0.SetActive(false);
//                SensibleArea4_pressed25.SetActive(false);
//                SensibleArea4_pressed50.SetActive(false);
//                SensibleArea4_pressed75.SetActive(false);
                WorkingArea_free.SetActive(false);
                WorkingArea_busy.SetActive(false);
                
                WorkingArea_undef.SetActive(true);
                TextArea1_A.SetActive(false);                
                TextArea1_B.SetActive(false);                
                TextArea1_C.SetActive(false);
                TextArea1_D.SetActive(false);
                
                TextArea2_A.SetActive(true);
                
                ret = 0;
            }
            else if (numStep == 4)
            {
                SensibleArea1_free.SetActive(false);
                SensibleArea1_busy.SetActive(false);
                
                SensibleArea1_undef.SetActive(true);
                
//                SensibleArea2_free.SetActive(false);
//                SensibleArea2_busy.SetActive(false);                
//                SensibleArea2_undef.SetActive(false);
                
                SensibleArea3_free.SetActive(false);
                SensibleArea3_busy.SetActive(false);
                SensibleArea3_undef.SetActive(false);
                
                SensibleArea3_pressed0.SetActive(true);
                
                SensibleArea3_pressed25.SetActive(false);
                SensibleArea3_pressed50.SetActive(false);
                SensibleArea3_pressed75.SetActive(false);
//                SensibleArea4_free.SetActive(false);
//                SensibleArea4_busy.SetActive(false);
//                SensibleArea4_undef.SetActive(false);
//                SensibleArea4_pressed0.SetActive(false);
//                SensibleArea4_pressed25.SetActive(false);
//                SensibleArea4_pressed50.SetActive(false);
//                SensibleArea4_pressed75.SetActive(false);
                WorkingArea_free.SetActive(false);
                WorkingArea_busy.SetActive(false);
                
                WorkingArea_undef.SetActive(true);
                TextArea1_A.SetActive(false);                
                TextArea1_B.SetActive(false);
                
                TextArea1_C.SetActive(true);
                
                TextArea1_D.SetActive(false);
                
                TextArea2_A.SetActive(true);
                ret = 0;
            }
            else if (numStep == 5)
            {
                SensibleArea1_free.SetActive(false);
                SensibleArea1_busy.SetActive(false);
                
                SensibleArea1_undef.SetActive(true);
                
//                SensibleArea2_free.SetActive(false);
//                SensibleArea2_busy.SetActive(false);
//                SensibleArea2_undef.SetActive(false);
                
                SensibleArea3_free.SetActive(false);
                SensibleArea3_busy.SetActive(false);
                SensibleArea3_undef.SetActive(false);
                SensibleArea3_pressed0.SetActive(false);
                
                SensibleArea3_pressed25.SetActive(true);
                
                SensibleArea3_pressed50.SetActive(false);
                SensibleArea3_pressed75.SetActive(false);
//                SensibleArea4_free.SetActive(false);
//                SensibleArea4_busy.SetActive(false);
//                SensibleArea4_undef.SetActive(false);
//                SensibleArea4_pressed0.SetActive(false);               
//                SensibleArea4_pressed25.SetActive(false);               
//                SensibleArea4_pressed50.SetActive(false);
//                SensibleArea4_pressed75.SetActive(false);
                WorkingArea_free.SetActive(false);
                WorkingArea_busy.SetActive(false);
                
                WorkingArea_undef.SetActive(true);
                TextArea1_A.SetActive(false);                
                TextArea1_B.SetActive(false);
                
                TextArea1_C.SetActive(true);
                
                TextArea1_D.SetActive(false);
                
                TextArea2_A.SetActive(true);
                ret = 0;
            }
            else if (numStep == 6)
            {
                SensibleArea1_free.SetActive(false);
                SensibleArea1_busy.SetActive(false);
                
                SensibleArea1_undef.SetActive(true);
                
//                SensibleArea2_free.SetActive(false);
//                SensibleArea2_busy.SetActive(false);
//                SensibleArea2_undef.SetActive(false);
                
                SensibleArea3_free.SetActive(false);
                SensibleArea3_busy.SetActive(false);
                SensibleArea3_undef.SetActive(false);
                SensibleArea3_pressed0.SetActive(false);
                SensibleArea3_pressed25.SetActive(false);
                
                SensibleArea3_pressed50.SetActive(true);
                
                SensibleArea3_pressed75.SetActive(false);
//                SensibleArea4_free.SetActive(false);
//                SensibleArea4_busy.SetActive(false);
//                SensibleArea4_undef.SetActive(false);
//                SensibleArea4_pressed0.SetActive(false);                
//                SensibleArea4_pressed25.SetActive(false);                
//                SensibleArea4_pressed50.SetActive(false);                
//                SensibleArea4_pressed75.SetActive(false);
                WorkingArea_free.SetActive(false);
                WorkingArea_busy.SetActive(false);
                
                WorkingArea_undef.SetActive(true);
                TextArea1_A.SetActive(false);                
                TextArea1_B.SetActive(false);
                
                TextArea1_C.SetActive(true);
                
                TextArea1_D.SetActive(false);
                
                TextArea2_A.SetActive(true);
                ret = 0;
            }
            else if (numStep == 7)
            {
                SensibleArea1_free.SetActive(false);
                SensibleArea1_busy.SetActive(false);
                
                SensibleArea1_undef.SetActive(true);
                
//                SensibleArea2_free.SetActive(false);
//                SensibleArea2_busy.SetActive(false);
//                SensibleArea2_undef.SetActive(false);
                
                SensibleArea3_free.SetActive(false);
                SensibleArea3_busy.SetActive(false);
                SensibleArea3_undef.SetActive(false);
                SensibleArea3_pressed0.SetActive(false);
                SensibleArea3_pressed25.SetActive(false);
                SensibleArea3_pressed50.SetActive(false);
                
                SensibleArea3_pressed75.SetActive(true);
                
//                SensibleArea4_free.SetActive(false);
//                SensibleArea4_busy.SetActive(false);
//                SensibleArea4_undef.SetActive(false);
//                SensibleArea4_pressed0.SetActive(false);                
//                SensibleArea4_pressed25.SetActive(false);                
//                SensibleArea4_pressed50.SetActive(false);                
//                SensibleArea4_pressed75.SetActive(false);                
                WorkingArea_free.SetActive(false);
                WorkingArea_busy.SetActive(false);
                
                WorkingArea_undef.SetActive(true);
                
                TextArea1_A.SetActive(false);                
                TextArea1_B.SetActive(false);
                
                TextArea1_C.SetActive(true);
                
                TextArea1_D.SetActive(false);
                
                TextArea2_A.SetActive(true);
                ret = 0;
            }
            else if (numStep == 8)
            {
                SensibleArea1_free.SetActive(false);
                SensibleArea1_busy.SetActive(false);
                
                SensibleArea1_undef.SetActive(true);
                
//                SensibleArea2_free.SetActive(false);
//                SensibleArea2_busy.SetActive(false);
//                SensibleArea2_undef.SetActive(false);
                
                SensibleArea3_free.SetActive(false);
                
                SensibleArea3_busy.SetActive(true);
                
                SensibleArea3_undef.SetActive(false);
                SensibleArea3_pressed0.SetActive(false);
                SensibleArea3_pressed25.SetActive(false);
                SensibleArea3_pressed50.SetActive(false);
                SensibleArea3_pressed75.SetActive(false);
//                SensibleArea4_free.SetActive(false);
//                SensibleArea4_busy.SetActive(false);
//                SensibleArea4_undef.SetActive(false);
//                SensibleArea4_pressed0.SetActive(false);                
//                SensibleArea4_pressed25.SetActive(false);                
//                SensibleArea4_pressed50.SetActive(false);                
//                SensibleArea4_pressed75.SetActive(false);                
                WorkingArea_free.SetActive(false);
                WorkingArea_busy.SetActive(false);
                
                WorkingArea_undef.SetActive(true);
                TextArea1_A.SetActive(false);                
                TextArea1_B.SetActive(false);
                
                TextArea1_C.SetActive(false);
                
                TextArea1_D.SetActive(true);
                
                TextArea2_A.SetActive(true);
                ret = 0;
            }

            return ret;
        }

        void SetMenu(int idMenu)
        {
            _idMenuActivated = idMenu;
        }

        void DeactivateMenu()
        {
            MainMenu.SetActive(false);
            SetupMenu.SetActive(false);
            RestartMenu.SetActive(false);
            
        }

        void ActivateMenu()
        {
            MainMenu.SetActive(false);
            SetupMenu.SetActive(false);
            RestartMenu.SetActive(false);
            if (_idMenuActivated == RESTART_MENU_ID)
            {
                RestartMenu.SetActive(true);    
            }
            else if (_idMenuActivated==MAIN_MENU_ID)
            {
                MainMenu.SetActive(true);
            }
            else if (_idMenuActivated == SETUP_MENU_ID)
            {
                SetupMenu.SetActive(true);
            }
        }
        void ProcessFrameWork()
        {
//            Mat emptyFrame=new Mat(0,0,CvType.CV_64F);

            int ret = -1;
            try
            {
                if (_initialized)
                {

                    internalState_ = _nativeObject.GetFSMState();
                    if (internalState_ == STATE_CHECK)
                    {
                        _calibrationParamsLoaded = false;
                        _configureWorkBenchParamsLoaded = false;
                        _configureWorkBenchParamsLoaded = false;
                        Debug.Log("EXECUTE STATE_CHECK");

                        ret = 0;
                    }
                    else
                    if (internalState_ == STATE_UNCALIBRATED)
                    {
                        Debug.Log("EXECUTE STATE_UNCALIBRATED");
                        //LOAD CALIB PARAMS FROM FILE (IF EXISTS)
                        _calibrationParamsLoaded = false;
                        ret = LoadCalibrationParams(ref _rtP_C, _calibDataFilename);
                        if (ret == 0)
                        {
                            //IF LOAD CALIBRATION PARAMS FROM FILE
                            ret=_nativeObject.SetCalibrationParams(_rtP_C);
                        }
                        else
                        {
                            StartProjectCalibrationMarkers(_xP_Ms);
                            DeactivateMenu();
//                            ButtonObjectDetectionAndTrackingStart.SetActive(false);
//                            ButtonUnCalibrate.SetActive(false);
//                            ButtonUnConfigureWorkbench.SetActive(false);
//                            ButtonZoomAdjustStart.SetActive(false);
//                            ButtonZoomAdjustStop.SetActive(false);
                            //AUTOMATIC CALIBRATION START
                        }

                        ret = 0;


                    }
                    else if (internalState_ == STATE_CALIBRATE)
                    {
                        Debug.Log("EXECUTE STATE_CALIBRATE "+_calibrationParamsLoaded);
                        if (_calibrationParamsLoaded)
                        {
                            ret = 0;
                        }
                        else
                        {
                            ret = Calibrate();
                            _calibrationStarted = false;
                            StopProjectCalibrationMarkers();
                            _nativeObject.GetCalibrationParams(_rtP_C);

                            ret=SaveCalibrationParams(_rtP_C,_calibDataFilename);
                        }                     
                        
                                            
                    }
                    else if (internalState_ == STATE_UNCONFIGURED_WORKBENCH) 
                    {
                        
                        Debug.Log("EXECUTE STATE_UNCONFIGURED_WORKBENCH");
                        _configureWorkBenchParamsLoaded = false;
                        ret = LoadConfigureWorkBenchParams(ref _sensibleAreasCoords,ref _sensibleAreasRTs,_configWorkBenchDataFilename);
                        if (ret == 0)
                        {
                            _nativeObject.SetConfigureWorkBenchParams(_sensibleAreasCoords,_sensibleAreasRTs);
                        }
                        
//                        ButtonObjectDetectionAndTrackingStart.SetActive(false);
//                        ButtonUnCalibrate.SetActive(false);
//                        ButtonUnConfigureWorkbench.SetActive(false);
//                        ButtonZoomAdjustStart.SetActive(false);
//                        ButtonZoomAdjustStop.SetActive(false);                            
                        DeactivateMenu();
 
                        ret = 0;

                    }
                    else if (internalState_ == STATE_CONFIGURE_WORKBENCH) 
                    {
                        Debug.Log("EXECUTE STATE_CONFIGURE_WORKBENCH");
                                                
                        //IF DO NOTLOAD CONFIGURATION PARAMS FROM FILE
                        if (_configureWorkBenchParamsLoaded)
                        {
                            ret = 0;
                        }
                        else
                        {
                            ret = ConfigureWorkBench();
                            _nativeObject.GetConfigureWorkBenchParams(_sensibleAreasCoords, _sensibleAreasRTs);

                            ret = SaveConfigureWorkBenchParams(_sensibleAreasCoords, _sensibleAreasRTs, _configWorkBenchDataFilename);

                            
                        }
                        ret = _nativeObject.GetSensibleAreasRTMatrixWRTProjector(_sensibleAreasRTsWRTProjector);
//                        ret = _nativeObject.GetWorkingAreaRTMatrixWRTProjector(_workingAreaRTsWRTProjector);
//                        ButtonObjectDetectionAndTrackingStart.SetActive(true);
//                        ButtonUnCalibrate.SetActive(true);
//                        ButtonUnConfigureWorkbench.SetActive(true);
//                        ButtonZoomAdjustStart.SetActive(true);
//                        ButtonZoomAdjustStop.SetActive(true);

                    }
                    else if (internalState_ == STATE_READY)
                    {
                        Debug.Log("EXECUTE STATE_READY");
                        //AUTOMATIC OBJECT DETECTION AND TRACKING START
                        if (_startObjectDetectionAndTracking)
                        {

                            if (_processStatus == PROCESS_STATE_UNDEF)
                            {
                                ret = ExecuteStatusUndef();
                                if (ret == 1)
                                {
                                    _processStatus = PROCESS_STATE_FINDOBJ;
                                    ret = 0;
                                }

                            }
                            else if (_processStatus == PROCESS_STATE_FINDOBJ)
                            {
                                ret = ExecuteStatusFindObj();
                                if (ret == 1)
                                {
                                    _processStatus = PROCESS_STATE_FINDTOOLS;
                                    ret = 0;
                                }

                            }
                            else if (_processStatus == PROCESS_STATE_FINDTOOLS)
                            {
                                ret = ExecuteStatusFindTools();
                                if (ret == 1)
                                {
                                    _processStatus = PROCESS_STATE_PUSHBUTTON;
                                    ret = 0;
                                }

                            }
                            else if (_processStatus == PROCESS_STATE_PUSHBUTTON)
                            {

                                ret = ExecuteStatusPushButton();
                                if (ret == 1)
                                {
                                    _processStatus = PROCESS_STATE_COMPLETED;
                                    ret = 0;
                                }


                            }
                            else if (_processStatus == PROCESS_STATE_COMPLETED)
                            {
                                ret = ExecuteStatusCompleted();
                                if (ret == 1)
                                {
//                                    SceneManager.LoadScene( SceneManager.GetActiveScene().name );
                                    _processStatus = PROCESS_STATE_UNDEF;
                                    _startObjectDetectionAndTracking = false;
                                    SetMenu(RESTART_MENU_ID);
                                    ActivateMenu();
                                    ret = 0;
                                }
                            }

                        }
                        else
                        {
//                            ButtonObjectDetectionAndTrackingStart.SetActive(true);
//                            ButtonUnCalibrate.SetActive(true);
//                            ButtonUnConfigureWorkbench.SetActive(true);
//                            ButtonZoomAdjustStart.SetActive(true);
//                            ButtonZoomAdjustStop.SetActive(true);
                            ActivateMenu();
                            

                            ret = 0;
                        }
                    }                
                    else if (internalState_ == STATE_MARKERDETECT)
                    {
                        Debug.Log("EXECUTE STATE_MARKERDETECT");
                        MarkerDetection();
                    }
                    else if (internalState_ == STATE_OBJDETECTANDTRACK)
                    {
                        Debug.Log("EXECUTE STATE_OBJDETECTANDTRACK");

//                        if (_processStatus == PROCESS_STATE_UNDEF)
//                        {
//                            ret=ExecuteStatusUndef();
//                            if (ret == 1)
//                            {
//                                _processStatus = PROCESS_STATE_FINDOBJ;
//                                ret = 0;
//                            }
//
//                        }                            
//                        else if (_processStatus == PROCESS_STATE_FINDOBJ)
//                        {
//
//                            ret=ExecuteStatusFindObj();
//                            if (ret == 1)
//                            {
//                                _processStatus = PROCESS_STATE_FINDTOOLS;
//                                ret = 0;
//                            }
//                            
//                        }
////                        else if (_processStatus == PROCESS_STATE_OBJDETECTED)
//                           //                        {
//                           //                            
//                           //
//                           //                        }
//                        else if (_processStatus == PROCESS_STATE_FINDTOOLS)
//                        {
//                            ret=ExecuteStatusFindTools();
//                            if (ret == 1)
//                            {
//                                _processStatus = PROCESS_STATE_PUSHBUTTON;
//                                ret = 0;
//                            }
//
//                        }
////                        else if (_processStatus == PROCESS_STATE_TOOLSDETECTED)
////                        {
////                            if (Tick() == true)
////                            {
////                                _processStatus = PROCESS_STATE_PUSHBUTTON;
////                            }
////                            else
////                            {
////                                _processStatus = PROCESS_STATE_TOOLSDETECTED;
////                            }
////                        }
//                        else if (_processStatus == PROCESS_STATE_PUSHBUTTON)
//                        {
//                            
//                            ret=ExecuteStatusPushButton();
//                            if (ret == 1)
//                            {
//                                _processStatus = PROCESS_STATE_COMPLETED;
//                                ret = 0;
//                            }
//
//                            
//                        }
////                        else if (_processStatus == PROCESS_STATE_PUSHBUTTON_1)
////                        {
////                        }
////                        else if (_processStatus == PROCESS_STATE_PUSHBUTTON_2)
////                        {
////                        }
////                        else if (_processStatus == PROCESS_STATE_PUSHBUTTON_3)
////                        {
////                        }
////                        else if (_processStatus == PROCESS_STATE_PUSHBUTTON_4)
////                        {
////                        }
//                        else if (_processStatus == PROCESS_STATE_COMPLETED)
//                        {
//                            ret=ExecuteStatusCompleted();
//                            if (ret == 1)
//                            {
//                                _processStatus = PROCESS_STATE_UNDEF;
//                                ret = 0;
//                            }
//                        }

                        ret = 0;
                    }

                    Debug.Log("EXECUTE ExecuteFSMState START");
                    _nativeObject.ExecuteFSMState();
                    Debug.Log("EXECUTE ExecuteFSMState END");

                }
                else
                {
                    _waitForZoomAdjustment++;
                    if (_waitForZoomAdjustment > MAX_TIME_ZOOM_ADJUST) 
                    {
                        _initialized = true;
                        StopProjectCalibrationMarkers();
                    }
                    else
                    {
                        StartProjectCalibrationMarkers(_xP_Ms);

                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.StackTrace);
            }
        }    
        
        
        
        void Update()
        {
//            if (_initialized)
//            {
//                if (_calibrationStarted == false)
//                {
//                      _nativeObject.ExecuteFSMState();
////                    StartProjectCalibrationMarkers(_xP_Ms);
//                    _calibrationStarted = true;
//                }
//                else
//                {
//                    int ret = Calibrate();
//                }
//            }

            lock (lockObject)
            {
                ProcessFrameWork();
            }
        }
      

        void OnDestroy()
        {
            ReleaseObjects();
            _instance = null;
        }

        void ReleaseObjects()
        {
//            _initialized = false;
            if (_arObjectsInternal != null)
            {
                for (int i = 0; i < _arObjectsInternal.Count; i++)
                {
                    for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= 0; j--)
                    {
                        DestroyImmediate(_arObjectsInternal[i].Instances[j]);
                        _arObjectsInternal[i].Instances.RemoveAt(j);
                    }
                }
            }

            if (_nativeObject != null)
            {
                _nativeObject.Destroy();
                _nativeObject = null;
            }

            // Hide artifacts generated by screen rotation 
//            if (preview != null && preview.texture != null && _frameMatrix != null)
//            {
//                preview.texture = null;
//                preview.color = Color.black;
//            }

            if (_poseMatrix != null)
            {
                _poseMatrix.release();
                _poseMatrix = null;
            }
            
            if (_poseMarkersMatrixes != null)
            {
                _poseMarkersMatrixes.release();
                _poseMarkersMatrixes = null;
            }

            if (_frameMatrixPreprocessed != null)
            {
                lock (_lock)
                {
                    _frameMatrixPreprocessed.release();
                    _frameMatrixPreprocessed = null;
                }
            }
            
            
            if (_frameMatrixRGBPreprocessed != null)
            {
                lock (_lock)
                {
                    _frameMatrixRGBPreprocessed.release();
                    _frameMatrixRGBPreprocessed = null;
                }
            }
            
            

            _isTracking = false;
        }
        
        
       
        

        public class NativeProjectionGuide : IBrainPadNative
        {
            private readonly IntPtr _nativeObjAddr;

            public NativeProjectionGuide()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                _nativeObjAddr = nativeProjectionGuide_Create();
//#endif
            }

            public void SetCameraParams(int cameraWidth, int cameraHeight, float horizontalViewAngle,
                float verticalViewAngle)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeProjectionGuide_SetCameraParams(_nativeObjAddr, cameraWidth, cameraHeight, horizontalViewAngle,
                    verticalViewAngle);
//#endif
            }
            
            public void SetCameraID(int cameraID)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeProjectionGuide_SetCameraID(_nativeObjAddr, cameraID);
//#endif
            }
            
            public int GetFSMState()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_GetFSMState(_nativeObjAddr);
//#endif
            }

            public void ExecuteFSMState()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeProjectionGuide_ExecuteFSMState(_nativeObjAddr);
//#endif
            }
            
            public int SetPSNRThresholds(double toolsThr,double buttonsThr)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_SetPSNRThresholds(_nativeObjAddr,toolsThr,buttonsThr);
//#endif
            }
            public int SetSSIMThresholds(double toolsThr,double buttonsThr)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_SetSSIMThresholds(_nativeObjAddr,toolsThr,buttonsThr);
//#endif
            }
        
            
            
            public int UnCalibrate()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_UnCalibrate(_nativeObjAddr);
//#endif
            }
            public int UnConfigureWorkBench()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_UnConfigureWorkBench(_nativeObjAddr);
//#endif
            }
            
            public int Calibrate()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_Calibrate(_nativeObjAddr);
//#endif
            }
            public int ConfigureWorkBench()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_ConfigureWorkBench(_nativeObjAddr);
//#endif
            }
            public int SetEnableObjectDetectionAndTracking(bool _enabled)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_SetEnableObjectDetectionAndTracking(_nativeObjAddr,_enabled);
//#endif
            }
            public int SetEnableToolsDetection(bool _enabled)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_SetEnableToolsDetection(_nativeObjAddr,_enabled);
//#endif
            }

            public int SetEnableButtonsDetection(bool _enabled)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_SetEnableButtonsDetection(_nativeObjAddr,_enabled);
//#endif
            }


            public void OnResult(int value)
            {
                if (value==0)
                    _objectDetectionAndTrackingResultIsReady = true;
                else
                {
                    _objectDetectionAndTrackingResultIsReady = false;
                }
                Debug.Log("CALLBACK OBJECTDETECTION_AND_TRACKING RESULT: "+value);
                
            }

            public int SetCallbackProcedure(CallbackProcedure _callbackProcedure)
            {
                return nativeProjectionGuide_SetCallbackProcedure(_nativeObjAddr,_callbackProcedure);
            }

        
            
            public void SetCalibrationSetup(List<KeyValuePair<int,Mat>> xP_Ms)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                
                //transform List to Mat
                Mat xP_Ms_Mat = new Mat(xP_Ms.Count, 7,CvType.CV_64FC1);
                for (int i = 0; i < xP_Ms.Count; i++)
                {
                    xP_Ms_Mat.put(i, 0, (double)xP_Ms[i].Key); //markerID
                    
                    for (int j=0;j<6;j++)
                        xP_Ms_Mat.put(i, j+1, (double)xP_Ms[i].Value.get(j,0)[0]); //xP_M

                }
                
                nativeProjectionGuide_SetCalibrationSetup(_nativeObjAddr, xP_Ms_Mat.nativeObj);
//#endif
            }
            
            public int SetCalibrationParams(Mat rtP_C)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_SetCalibrationParams(_nativeObjAddr, rtP_C.nativeObj);
//#endif
            }
            public void GetCalibrationParams(Mat rtP_C)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeProjectionGuide_GetCalibrationParams(_nativeObjAddr, rtP_C.nativeObj);
//#endif
            }

            
            public void SetConfigureWorkBenchSetup(List<KeyValuePair<int,int>> sensibleAreas)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                //transform List to Mat
                Mat sensibleAreas_Mat = new Mat(sensibleAreas.Count, 2,CvType.CV_64FC1);
                for (int i = 0; i < sensibleAreas.Count; i++)
                {
                    sensibleAreas_Mat.put(i, 0, sensibleAreas[i].Key); //markerID
                    sensibleAreas_Mat.put(i, 1, sensibleAreas[i].Value); //areaID                    
                    
                }
                nativeProjectionGuide_SetConfigureWorkBenchSetup(_nativeObjAddr, sensibleAreas_Mat.nativeObj);
//#endif
            }
            
            public void SetConfigureWorkBenchParams(List<KeyValuePair<int, Mat>> sensibleAreasCoords,List<KeyValuePair<int, Mat>> sensibleAreasRTs)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                Mat sensibleAreasCoords_Mat = new Mat(sensibleAreasCoords.Count, 9,CvType.CV_64FC1);
                for (int i = 0; i < sensibleAreasCoords.Count; i++)
                {
                    sensibleAreasCoords_Mat.put(i, 0, (double)sensibleAreasCoords[i].Key); //areaID
                    sensibleAreasCoords_Mat.put(i, 1, (double)sensibleAreasCoords[i].Value.get(0,0)[0]); //(top)                  
                    sensibleAreasCoords_Mat.put(i, 2, (double)sensibleAreasCoords[i].Value.get(0,1)[0]); //(left)                  
                    sensibleAreasCoords_Mat.put(i, 3, (double)sensibleAreasCoords[i].Value.get(1,0)[0]); //(bottom)                  
                    sensibleAreasCoords_Mat.put(i, 4, (double)sensibleAreasCoords[i].Value.get(1,1)[0]); //(right)    
                    sensibleAreasCoords_Mat.put(i, 5, (double)sensibleAreasCoords[i].Value.get(2,0)[0]); //(bottom)                  
                    sensibleAreasCoords_Mat.put(i, 6, (double)sensibleAreasCoords[i].Value.get(2,1)[0]); //(right)    
                    sensibleAreasCoords_Mat.put(i, 7, (double)sensibleAreasCoords[i].Value.get(3,0)[0]); //(bottom)                  
                    sensibleAreasCoords_Mat.put(i, 8, (double)sensibleAreasCoords[i].Value.get(3,1)[0]); //(right)    
                    Debug.Log("AREAS: "+(double)sensibleAreasCoords_Mat.get(i,0)[0]+" " +(double)sensibleAreasCoords_Mat.get(i,1)[0]+" "+(double)sensibleAreasCoords_Mat.get(i,2)[0]
                              +" "+(double)sensibleAreasCoords_Mat.get(i,3)[0]+" "+(double)sensibleAreasCoords_Mat.get(i,4)[0] );
                    
                }
                
                Mat sensibleAreasRTs_Mat = new Mat(sensibleAreasRTs.Count*5, 4,CvType.CV_64FC1);
                for (int i = 0; i < sensibleAreasRTs.Count; i++)
                {
                    sensibleAreasRTs_Mat.put(i*5, 0, (double)sensibleAreasRTs[i].Key); //areaID
                    for (int k = 0; k < 4; k++)
                    {
                        for (int j = 0; j < 4; j++)
                        {
                            sensibleAreasRTs_Mat.put(i * 5 + 1 + j, k, (double) sensibleAreasRTs[i].Value.get(j, k)[0]);
                        }
                    }

//                    Debug.Log("AREA RT: " +sensibleAreasRTs_Mat.get(i * 5 + 0, 0)[0]+ " " + (double) sensibleAreasRTs_Mat.get(i * 5 + 1, 0)[0] + " " +
//                              (double) sensibleAreasRTs_Mat.get(i * 5 + 1, 1)[0] + " " +
//                              (double) sensibleAreasRTs_Mat.get(i * 5 + 1, 2)[0]
//                              + " " + (double) sensibleAreasRTs_Mat.get(i * 5 + 1, 3)[0]
//                              + " " + (double) sensibleAreasRTs_Mat.get(i * 5 + 2, 0)[0] + " " +
//                              (double) sensibleAreasRTs_Mat.get(i * 5 + 2, 1)[0] + " " +
//                              (double) sensibleAreasRTs_Mat.get(i * 5 + 2, 2)[0]
//                              + " " + (double) sensibleAreasRTs_Mat.get(i * 5 + 2, 3)[0]
//                              + " " + (double) sensibleAreasRTs_Mat.get(i * 5 + 3, 0)[0] + " " +
//                              (double) sensibleAreasRTs_Mat.get(i * 5 + 3, 1)[0] + " " +
//                              (double) sensibleAreasRTs_Mat.get(i * 5 + 3, 2)[0]
//                              + " " + (double) sensibleAreasRTs_Mat.get(i * 5 + 3, 3)[0]
//                              + " " + (double) sensibleAreasRTs_Mat.get(i * 5 + 4, 0)[0] + " " +
//                              (double) sensibleAreasRTs_Mat.get(i * 5 + 4, 1)[0] + " " +
//                              (double) sensibleAreasRTs_Mat.get(i * 5 + 4, 2)[0]
//                              + " " + (double) sensibleAreasRTs_Mat.get(i * 5 + 4, 3)[0]);

                }
                
                
                nativeProjectionGuide_SetConfigureWorkBenchParams(_nativeObjAddr, sensibleAreasCoords_Mat.nativeObj,sensibleAreasRTs_Mat.nativeObj);
//#endif
            }
            public void GetConfigureWorkBenchParams(List<KeyValuePair<int, Mat>> sensibleAreasCoords,List<KeyValuePair<int, Mat>> sensibleAreasRTs)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                sensibleAreasCoords.Clear();
                sensibleAreasRTs.Clear();
                
                Mat sensibleAreasCoords_Mat = new Mat(MAX_NUM_SENSIBLE_AREAS, 9,CvType.CV_64FC1); 
                Mat sensibleAreasRTs_Mat = new Mat(MAX_NUM_SENSIBLE_AREAS*5, 4,CvType.CV_64FC1); 
                
                nativeProjectionGuide_GetConfigureWorkBenchParams(_nativeObjAddr, sensibleAreasCoords_Mat.nativeObj,sensibleAreasRTs_Mat.nativeObj);
                for (int i = 0; i < sensibleAreasCoords_Mat.rows(); i++)
                {
                    int areaID = (int)sensibleAreasCoords_Mat.get(i, 0)[0];
                    Mat rect = new Mat(4, 2,CvType.CV_64FC1);
                    rect.put(0,0,sensibleAreasCoords_Mat.get(i, 1)[0]);
                    rect.put(0,1,sensibleAreasCoords_Mat.get(i, 2)[0]);
                    rect.put(1,0,sensibleAreasCoords_Mat.get(i, 3)[0]);
                    rect.put(1,1,sensibleAreasCoords_Mat.get(i, 4)[0]); 
                    rect.put(2,0,sensibleAreasCoords_Mat.get(i, 5)[0]);
                    rect.put(2,1,sensibleAreasCoords_Mat.get(i, 6)[0]);
                    rect.put(3,0,sensibleAreasCoords_Mat.get(i, 7)[0]);
                    rect.put(3,1,sensibleAreasCoords_Mat.get(i, 8)[0]);    
                    sensibleAreasCoords.Add(new KeyValuePair<int, Mat>(areaID,rect));
                    
//                    Debug.Log("AREAS: "+areaID+" "+rect.get(0,0)[0]+" "+rect.get(0,1)[0]+" "+rect.get(1,0)[0]+" "+rect.get(1,1)[0]+" "+rect.get(2,0)[0]+" "+rect.get(2,1)[0]+" "+rect.get(3,0)[0]+" "+rect.get(3,1)[0]);
                }
                
                for (int i = 0; i < sensibleAreasRTs_Mat.rows()/5; i++)
                {
                    int areaID = (int)sensibleAreasRTs_Mat.get(i*5, 0)[0];
                    Mat rt = new Mat(4, 4,CvType.CV_64FC1);
                    for (int k = 0; k < 4; k++)
                    {
                        for (int j = 0; j < 4; j++)
                        {
                            rt.put(j,k,sensibleAreasRTs_Mat.get(i*5+1+j, k)[0]);
                        }
                    }

                    sensibleAreasRTs.Add(new KeyValuePair<int, Mat>(areaID,rt));
//                    Debug.Log("AREAS RT: " + areaID + " " + rt.get(0, 0)[0] + " " + rt.get(0, 1)[0] + " " +
//                              rt.get(0, 2)[0] + " " + rt.get(0, 3)[0]
//                              + " " + rt.get(1, 0)[0] + " " + rt.get(1, 1)[0] + " " + rt.get(1, 2)[0] + " " +
//                              rt.get(1, 3)[0]
//                              + " " + rt.get(2, 0)[0] + " " + rt.get(2, 1)[0] + " " + rt.get(2, 2)[0] + " " +
//                              rt.get(2, 3)[0]
//                              + " " + rt.get(3, 0)[0] + " " + rt.get(3, 1)[0] + " " + rt.get(3, 2)[0] + " " +
//                              rt.get(3, 3)[0]);
                }
//#endif
            }

            
            
            public void AddNewObjectToDetect(int objectId, Texture2D referenceImage, Mat worldPlane)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                Mat referenceImageMat;
                Mat referenceImageGreyMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC1);
                if (referenceImage.format == TextureFormat.RGBA32)
                {
                    referenceImageMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC4);
                    OpenCVForUnity.Utils.fastTexture2DToMat(referenceImage, referenceImageMat);
                    Imgproc.cvtColor(referenceImageMat, referenceImageGreyMat, Imgproc.COLOR_RGBA2GRAY);
                }
                else if (referenceImage.format == TextureFormat.RGB24)
                {
                    referenceImageMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC3);
                    OpenCVForUnity.Utils.fastTexture2DToMat(referenceImage, referenceImageMat);
                    Imgproc.cvtColor(referenceImageMat, referenceImageGreyMat, Imgproc.COLOR_RGB2GRAY);
                }
                else if (referenceImage.format == TextureFormat.Alpha8)
                {
                    referenceImageMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC1);
                    OpenCVForUnity.Utils.fastTexture2DToMat(referenceImage, referenceImageMat);
                }
                else
                {
                    Debug.LogError(
                        "Error: wrong image target fromat, the image target should be 32bit RGB with alpha, 24bit RGB or 8bit GreyScale");
//                    EditorUtility.DisplayDialog("Error: wrong reference image", "Your project is correctly setup to run BrainPad", "Ok");
                    return;
                }

//                Old handling with OpenCVForUnity.Utils.texture2DToMat
//                Mat referenceImageMat; = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC4);
//                OpenCVForUnity.Utils.texture2DToMat(referenceImage, referenceImageMat);
//                Mat referenceImageGreyMat = new Mat(referenceImageMat.rows(), referenceImageMat.cols(), CvType.CV_8UC1);
//                Imgproc.cvtColor(referenceImageMat, referenceImageGreyMat, Imgproc.COLOR_RGBA2GRAY);

                Debug.Log("WORLDPLANE: " + worldPlane.get(0,0)[0]+" "+worldPlane.get(0,1)[0]+" "+worldPlane.get(0,2)[0]
                          +" "+worldPlane.get(1,0)[0]+" "+worldPlane.get(1,1)[0]+" "+worldPlane.get(1,2)[0]
                          +" "+worldPlane.get(2,0)[0]+" "+worldPlane.get(2,1)[0]+" "+worldPlane.get(2,2)[0]
                          +" "+worldPlane.get(3,0)[0]+" "+worldPlane.get(3,1)[0]+" "+worldPlane.get(3,2)[0]);
                
                nativeProjectionGuide_AddNewObjectToDetect(_nativeObjAddr, objectId, referenceImageGreyMat.nativeObj,
                    worldPlane.nativeObj);
//#endif
            }

            public void Init()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeProjectionGuide_Init(_nativeObjAddr);
//#endif
            }

            public void Configure()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeProjectionGuide_Configure(_nativeObjAddr);
//#endif
            }

            public void Start()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeProjectionGuide_Start(_nativeObjAddr);
//#endif
            }

            public void Stop()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeProjectionGuide_Stop(_nativeObjAddr);
//#endif
            }
            
//            public void SetState(int _state)
//            {
////#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
//
//                nativeProjectionGuide_SetState(_nativeObjAddr,_state);
////#endif
//            }
//            
//            public void SetXProjector_W(Mat xP_W)
//            {
//                nativeProjectionGuide_SetXProjector_W(_nativeObjAddr,xP_W.nativeObj);
//            }

//            public int ProcessFrame(Mat imageRgba,int cameraId)
//            {
////#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
//                return nativeProjectionGuide_ExecuteProcess(_nativeObjAddr, cameraId,imageRgba.nativeObj);
////#else
////            return 0;
////#endif
//            }

            public int GetObjectRTMatrix(Mat poseMatrixOut)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_GetObjectRTMatrix(_nativeObjAddr, poseMatrixOut.nativeObj);
//#else
//            return 0;
//#endif
            }
            public int GetObjectRTMatrixWRTProjector(Mat poseMatrixOut)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_GetObjectRTMatrixWRTProjector(_nativeObjAddr, poseMatrixOut.nativeObj);
//#else
//            return 0;
//#endif
            }
            
            public int GetObjectRTWBMatrixWRTProjector(Mat poseMatrixOut)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_GetObjectRTWBMatrixWRTProjector(_nativeObjAddr, poseMatrixOut.nativeObj);
//#else
//            return 0;
//#endif
            }


            public int GetSensibleAreasRTMatrixWRTProjector(List<KeyValuePair<int, Mat>> sensibleAreasRTsWRTProjector)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                sensibleAreasRTsWRTProjector.Clear();
                
                Mat sensibleAreasRTs_Mat = new Mat(MAX_NUM_SENSIBLE_AREAS*5, 4,CvType.CV_64FC1); 
                
                nativeProjectionGuide_GetSensibleAreasRTMatrixWRTProjector(_nativeObjAddr, sensibleAreasRTs_Mat.nativeObj);
                
                
                for (int i = 0; i < sensibleAreasRTs_Mat.rows()/5; i++)
                {
                    int areaID = (int)sensibleAreasRTs_Mat.get(i*5, 0)[0];
                    Mat rt = new Mat(4, 4,CvType.CV_64FC1);
                    for (int k = 0; k < 4; k++)
                    {
                        for (int j = 0; j < 4; j++)
                        {
                            rt.put(j,k,sensibleAreasRTs_Mat.get(i*5+1+j, k)[0]);
                        }
                    }

                    sensibleAreasRTsWRTProjector.Add(new KeyValuePair<int, Mat>(areaID,rt));
//                    Debug.Log("AREAS RT: " + areaID + " " + rt.get(0, 0)[0] + " " + rt.get(0, 1)[0] + " " +
//                              rt.get(0, 2)[0] + " " + rt.get(0, 3)[0]
//                              + " " + rt.get(1, 0)[0] + " " + rt.get(1, 1)[0] + " " + rt.get(1, 2)[0] + " " +
//                              rt.get(1, 3)[0]
//                              + " " + rt.get(2, 0)[0] + " " + rt.get(2, 1)[0] + " " + rt.get(2, 2)[0] + " " +
//                              rt.get(2, 3)[0]
//                              + " " + rt.get(3, 0)[0] + " " + rt.get(3, 1)[0] + " " + rt.get(3, 2)[0] + " " +
//                              rt.get(3, 3)[0]);
                }

                return 0;
//#else
//            return 0;
//#endif
            }
            
//            public int GetWorkingAreaRTMatrixWRTProjector(List<KeyValuePair<int, Mat>> workingAreaRTsWRTProjector)
//            {
////#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
//                workingAreaRTsWRTProjector.Clear();
//                
//                Mat workingAreaRTs_Mat = new Mat(MAX_NUM_WORKING_AREA*5, 4,CvType.CV_64FC1); 
//                
//                nativeProjectionGuide_GetWorkingAreaRTMatrixWRTProjector(_nativeObjAddr, workingAreaRTs_Mat.nativeObj);
//                
//                
//                for (int i = 0; i < workingAreaRTs_Mat.rows()/5; i++)
//                {
//                    int areaID = (int)workingAreaRTs_Mat.get(i*5, 0)[0];
//                    Mat rt = new Mat(4, 4,CvType.CV_64FC1);
//                    for (int k = 0; k < 4; k++)
//                    {
//                        for (int j = 0; j < 4; j++)
//                        {
//                            rt.put(j,k,workingAreaRTs_Mat.get(i*5+1+j, k)[0]);
//                        }
//                    }
//
//                    workingAreaRTsWRTProjector.Add(new KeyValuePair<int, Mat>(areaID,rt));
////                    Debug.Log("AREAS RT: " + areaID + " " + rt.get(0, 0)[0] + " " + rt.get(0, 1)[0] + " " +
////                              rt.get(0, 2)[0] + " " + rt.get(0, 3)[0]
////                              + " " + rt.get(1, 0)[0] + " " + rt.get(1, 1)[0] + " " + rt.get(1, 2)[0] + " " +
////                              rt.get(1, 3)[0]
////                              + " " + rt.get(2, 0)[0] + " " + rt.get(2, 1)[0] + " " + rt.get(2, 2)[0] + " " +
////                              rt.get(2, 3)[0]
////                              + " " + rt.get(3, 0)[0] + " " + rt.get(3, 1)[0] + " " + rt.get(3, 2)[0] + " " +
////                              rt.get(3, 3)[0]);
//                }
//
//                return 0;
////#else
////            return 0;
////#endif
//            }
            
            public int GetSensibleAreasStatus(Mat sensibleAreasStatus)
            {
                return nativeProjectionGuide_GetSensibleAreasStatus(_nativeObjAddr,sensibleAreasStatus.nativeObj);
            
            }
            
            
//            public int GetRTCamera_Projector(Mat rtC_POut)
//            {
////#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
//                return nativeProjectionGuide_GetRTCamera_Projector(_nativeObjAddr, rtC_POut.nativeObj);
////#else
////            return 0;
////#endif
//            }
//            
//            public int GetRTProjector_Camera(Mat rtP_COut)
//            {
////#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
//                return nativeProjectionGuide_GetRTProjector_Camera(_nativeObjAddr, rtP_COut.nativeObj);
////#else
////            return 0;
////#endif
//            }
            
            
            public int GetMarkersRTMatrixes(Mat poseMarkerMatrixesOut)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_GetMarkersRTMatrixes(_nativeObjAddr,poseMarkerMatrixesOut.nativeObj);
//#else
//            return 0;
//#endif
            }
            
            public int GetMarkersRTMatrixesWRTProjector(Mat poseMarkerMatrixesOut)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_GetMarkersRTMatrixesWRTProjector(_nativeObjAddr,poseMarkerMatrixesOut.nativeObj);
//#else
//            return 0;
//#endif
            }
            
            public int ResetToolsAreas()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProjectionGuide_ResetToolsAreas(_nativeObjAddr);
//#else
//            return 0;
//#endif
            }
            
            public void Destroy()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeProjectionGuide_Destroy(_nativeObjAddr);
//#endif
            }

//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
#if !UNITY_EDITOR
            const string LIBNAME = "Sapphire";
#else
//        const string LIBNAME = "__Internal";
            const string LIBNAME = "Sapphire";
#endif

            [DllImport(LIBNAME)]
            private static extern IntPtr nativeProjectionGuide_Create();

            [DllImport(LIBNAME)]
            private static extern void nativeProjectionGuide_Destroy(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_AddNewObjectToDetect(IntPtr nativeObjAddr, int objectId, IntPtr queryFrame,
                IntPtr worldPlane);

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_Init(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_SetCameraParams(IntPtr nativeObjAddr, int width, int height, float hFov,
                float vFov);

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_Configure(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_Start(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_Stop(IntPtr nativeObjAddr);

            
            
            
            
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_SetCameraID(IntPtr nativeObjAddr, int cameraID);
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_GetFSMState(IntPtr nativeObjAddr);
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_ExecuteFSMState(IntPtr nativeObjAddr);
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_SetPSNRThresholds(IntPtr nativeObjAddr,double toolsThr,double buttonsThr);
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_SetSSIMThresholds(IntPtr nativeObjAddr,double toolsThr,double buttonsThr);
            

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_UnCalibrate(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_UnConfigureWorkBench(IntPtr nativeObjAddr);
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_Calibrate(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_ConfigureWorkBench(IntPtr nativeObjAddr);
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_SetEnableObjectDetectionAndTracking(IntPtr nativeObjAddr,bool enabled_);
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_SetEnableToolsDetection(IntPtr nativeObjAddr,bool enabled_);
           
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_SetEnableButtonsDetection(IntPtr nativeObjAddr,bool enabled_);
           
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_SetCallbackProcedure(IntPtr nativeObjAddr,[MarshalAs(UnmanagedType.FunctionPtr)] CallbackProcedure callbackProcedure_);
            

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_SetCalibrationSetup(IntPtr nativeObjAddr, IntPtr rXP_Ms);

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_SetCalibrationParams(IntPtr nativeObjAddr, IntPtr rRTP_C);
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_GetCalibrationParams(IntPtr nativeObjAddr, IntPtr rRTP_COut);

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_SetConfigureWorkBenchSetup(IntPtr nativeObjAddr, IntPtr rSensibleAreas);
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_SetConfigureWorkBenchParams(IntPtr nativeObjAddr, IntPtr rSensibleAreasCoords,IntPtr rSensibleAreasRTs);

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_GetConfigureWorkBenchParams(IntPtr nativeObjAddr, IntPtr rSensibleAreasCoordsOut,IntPtr rSensibleAreasRTsOut);
            
            
            
            
            
            
            
            
            
//            [DllImport(LIBNAME)]
//            private static extern int nativeProjectionGuide_ExecuteProcess(IntPtr nativeObjAddr, int cameraId, IntPtr rImage);
            
//            [DllImport(LIBNAME)]
//            private static extern int nativeProjectionGuide_SetState(IntPtr nativeObjAddr, int _state);
//
//            [DllImport(LIBNAME)]
//            private static extern int nativeProjectionGuide_SetXProjector_W(IntPtr nativeObjAddr, IntPtr rXP_W);

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_GetObjectRTMatrix(IntPtr nativeObjAddr, IntPtr rRTMatrixOut);
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_GetObjectRTMatrixWRTProjector(IntPtr nativeObjAddr, IntPtr rRTMatrixOut);

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_GetObjectRTWBMatrixWRTProjector(IntPtr nativeObjAddr, IntPtr rRTMatrixOut);
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_GetSensibleAreasRTMatrixWRTProjector(IntPtr nativeObjAddr, IntPtr rSensibleAreasRTs);
                
//            [DllImport(LIBNAME)]
//            private static extern int nativeProjectionGuide_GetWorkingAreaRTMatrixWRTProjector(IntPtr nativeObjAddr, IntPtr rWorkingAreaRTs);
            
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_GetSensibleAreasStatus(IntPtr nativeObjAddr, IntPtr sensibleAreasStatusOut);
//            [DllImport(LIBNAME)]
//            private static extern int nativeProjectionGuide_GetRTCamera_Projector(IntPtr nativeObjAddr, IntPtr rRTC_POut);
//            
//            [DllImport(LIBNAME)]
//            private static extern int nativeProjectionGuide_GetRTProjector_Camera(IntPtr nativeObjAddr, IntPtr rRTP_COut);
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_GetMarkersRTMatrixes(IntPtr nativeObjAddr,IntPtr rRTMatrixOut);
            
            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_GetMarkersRTMatrixesWRTProjector(IntPtr nativeObjAddr,IntPtr rRTMatrixOut);

            [DllImport(LIBNAME)]
            private static extern int nativeProjectionGuide_ResetToolsAreas(IntPtr nativeObjAddr);

//            [DllImport(LIBNAME)]
//            private static extern int nativeProjectionGuide_GetReferenceObjects(IntPtr nativeObjAddr, IntPtr rRefObjsOut);
//
//            [DllImport(LIBNAME)]
//            private static extern int nativeProjectionGuide_GetImage(IntPtr nativeObjAddr, IntPtr rImageOut);
//
//            [DllImport(LIBNAME)]
//            private static extern int nativeProjectionGuide_SetImage(IntPtr nativeObjAddr, IntPtr rImage);
        }
    }
}
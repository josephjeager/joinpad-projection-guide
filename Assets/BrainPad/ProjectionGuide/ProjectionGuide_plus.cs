﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using BrainPad.Core;
using NatCamU.Core;
using OpenCVForUnity;
using UnityEditor;
using UnityEngine;
using System.IO;
using Object = System.Object;
using Utils = BrainPad.Core.Utils;

namespace BrainPad.ArTMaxConfiguratorPlus
{
    public class ArTMaxConfiguratorPlus : BrainPadCamBehaviour
    {
        [Header("Augmented Reality")] public Camera UnityCamera;

        private static int MAX_NUM_MARKERS=16;
            
        public List<ArObject> ArObjects;

        public static ArTMaxConfiguratorPlus _instance;
        private NativeArTMaxConfiguratorPlus _nativeObject;
        

        public bool _isTracking;
        private List<KeyValuePair<int, Matrix4x4>> _foundObjects;
        private List<KeyValuePair<int, Matrix4x4>> _foundMarkers;
        private Mat _frameMatrix;

        private Mat _frameMatrixPreprocessed;

        private Mat _poseMatrix;
        private Mat _poseMarkersMatrixes;

        private Mat _classifiedObjects;
        
        private Matrix4x4 _rtMatrix4X4;
        private List<ArObjectInternal> _arObjectsInternal;

        private bool _initialized;
        private static Object _lock = new Object();

        private List<int> _markerIds=new List<int>();
        
        //TEST - START
        private Mat _frameMatrixRGBPreprocessed;
        private List<int> _classifiedObjIds=new List<int>();
        //TEST - END

        
        private int _numMarkers = 0;

        private Vector3 startVertex, endVertex;
        
        private int _numClassifiedObjs= 0;
        
        private Texture2D _texture;
        private Color32[] _colors;
        private const TextureFormat Format =
            
#if UNITY_IOS && !UNITY_EDITOR
            TextureFormat.BGRA32;
#else
            TextureFormat.RGBA32;
#endif

        void Awake()
        {
            for (int i = 0; i < ArObjects.Count; i++)
            {
                ArObjects[i].ArGameObjectRoot.SetActive(false);
            }

            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
        }

        public override void OnStart()
        {
            base.OnStart();
                    
            ReleaseObjects();
            Debug.Log("Creating Objects");
            _foundObjects = new List<KeyValuePair<int, Matrix4x4>>();
            _foundMarkers= new List<KeyValuePair<int, Matrix4x4>>();
            _arObjectsInternal = new List<ArObjectInternal>();
            _nativeObject = new NativeArTMaxConfiguratorPlus();
            _poseMatrix = new Mat(5, 4, CvType.CV_64FC1);
            _poseMarkersMatrixes = new Mat(5*MAX_NUM_MARKERS, 4, CvType.CV_64FC1);
            
            //TEST - START
            
            _classifiedObjects = new Mat(2*MAX_NUM_MARKERS, 4, CvType.CV_64FC1);
            Debug.Log("Loading Assets 1");
            //Create Graph Buffer from file (.pb)
            TextAsset str=Resources.Load<TextAsset>("inferenceGraph.pb");
            byte[] inferenceGraphBuffer=str.bytes;
            Debug.Log("Loading Assets 2");
            //Create Graph Config Buffer from file (.pbtxt)
            str=Resources.Load<TextAsset>("inferenceGraphConfig.pbtxt");
            byte[] inferenceGraphConfigBuffer = str.bytes;
            Debug.Log("Loading Assets 3");
            //Create Object Labels Buffer from file (.txt)
            str=Resources.Load<TextAsset>("objectLabels");
            byte[] objectLabelsBuffer = str.bytes;
            Debug.Log("Loading Assets - DONE");
            
            _nativeObject.SetInferenceGraphBuffer(inferenceGraphBuffer, inferenceGraphConfigBuffer,objectLabelsBuffer);  
            Debug.Log("SetInferenceGraphBuffer");
            
            //TEST - END
            
            lock (_lock)
            {
                _frameMatrixPreprocessed = new Mat(NatCam.Preview.height, NatCam.Preview.width, CvType.CV_8UC1);
                _frameMatrixRGBPreprocessed = new Mat(NatCam.Preview.height, NatCam.Preview.width, CvType.CV_8UC3);
            }

            Utils.ConfigureCamera(UnityCamera, preview, _nativeObject);
            _nativeObject.Configure();
            Utils.ConfigureArObjectsInternal(ArObjects, _arObjectsInternal, _nativeObject);

            _nativeObject.Init();
            _nativeObject.Start();

            preview.color = Color.white;

            _initialized = true;
        }

        public override void OnFrame()
        {
//            try
//            {
//#if NATCAM_PRO || NATCAM_PROFESSIONAL
                if (!NatCam.PreviewMatrix(ref _frameMatrix)) return;
                _colors = _colors ?? new Color32[_frameMatrix.cols() * _frameMatrix.rows()];
                _texture = _texture ?? new Texture2D(_frameMatrix.cols(), _frameMatrix.rows(), Format, false, false);
                if (_texture.width != _frameMatrix.cols() || _texture.height != _frameMatrix.rows())
                    _texture.Resize(_frameMatrix.cols(), _frameMatrix.rows());

                OpenCVForUnity.Core.flip(_frameMatrix, _frameMatrix, 0);
                lock (_lock)
                {
                    Imgproc.cvtColor(_frameMatrix, _frameMatrixPreprocessed, Imgproc.COLOR_RGBA2GRAY);
                    Imgproc.cvtColor(_frameMatrix, _frameMatrixRGBPreprocessed, Imgproc.COLOR_RGBA2RGB);
                   
                }

        
    
            
            
//            New logic, now ProcessFrameWork() is executed on Unity main thread (Update())
//            ProcessFrameWork();

//            Not needed anymore
//            if (_isTracking)
//            {
//            OpenCVForUnity.Utils.matToTexture2D(_frameMatrix, _texture, _colors);
                OpenCVForUnity.Utils.fastMatToTexture2D(_frameMatrix, _texture);
                preview.texture = _texture;
//            }
//            catch (Exception e)
//            {
//                Debug.Log(e.StackTrace);
//            }

//            }
//#endif
        }

        void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus)
            {
                ReleaseObjects();
            }
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                ReleaseObjects();
            }
        }

        void OnGUI()
        {
            String idsStr="";
            
            for (int i = 0; i < _markerIds.Count; i++)
            {
                idsStr=idsStr+" "+_markerIds[i].ToString();
            }
            
            //TEST - START
            String classifiedIdsStr="";
            
            for (int i = 0; i < _classifiedObjIds.Count; i++)
            {
                classifiedIdsStr=classifiedIdsStr+" "+_classifiedObjIds[i].ToString();
            }
            //TEST - END
            
            GUILayout.TextField("Found " +_markerIds.Count+" markers -> IDs: "+ idsStr+ " Classified Objs: "+classifiedIdsStr);
        }
        
//        void DrawLine(Vector3 start, Vector3 end, Color color)
//        {
//            GameObject myLine = new GameObject();
//            myLine.transform.position = start;
//            myLine.AddComponent<LineRenderer>();
//            LineRenderer lr = myLine.GetComponent<LineRenderer>();
//            lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
//            lr.SetColors(color, color);
//            lr.SetWidth(0.1f, 0.1f);
//            lr.SetPosition(0, start);
//            lr.SetPosition(1, end);
////            GameObject.Destroy(myLine, duration);
//        }


        Mat DrawBox(Mat _frame, int _left, int _top, int _bottom,int _right)
        {
            byte[] color = new byte[3];
            color[0] = 255;
            color[1] = 0;
            color[2] = 255;
                
            for (int i = _left; i < _right; i++)
            {
                
//                Debug.Log("AAAAA: "+i+","+_top);
                _frame.get(i, _top)[0] = 1;
                _frame.get(i, _top)[1] = 0;
                _frame.get(i, _top)[2] = 1;
            }
            for (int i = _left; i < _right; i++)
            {
//                Debug.Log("AAAAA: "+i+","+_bottom);
                _frame.get(i, _bottom)[0] = 0;
                _frame.get(i, _bottom)[1] = 0;
                _frame.get(i, _bottom)[2] = 0;
            }
            for (int i = _top; i < _bottom; i++)
            {
                _frame.get(_left, i)[0] = 0;
                _frame.get(_left,i)[1] = 0;
                _frame.get(_left, i)[2] = 0;
            }
            for (int i = _top; i < _bottom; i++)
            {
                _frame.get(_right,i)[0] = 0;
                _frame.get(_right,i)[1] = 0;
                _frame.get(_right,i)[2] = 0;
            }

            return _frame;
        }
        
        
        void ProcessFrameWork()
        {
//            try
//            {
                if (_initialized)
                {
                    lock (_lock)
                    {
//                        _nativeObject.ProcessFrame(_frameMatrixPreprocessed);
                        //TEST - START
                        _nativeObject.ProcessFrame(_frameMatrixRGBPreprocessed);
                        //TEST - END
                    }

                   
                    _numMarkers=_nativeObject.GetMarkersRTMatrixes(_poseMarkersMatrixes);
 
                    _markerIds.Clear();
                    _foundMarkers.Clear();
                     
//                    for (int i = 0; i < _numMarkers; i++)
//                    {
//                        int markerId = (int) _poseMarkersMatrixes.get(i * 5, 0)[0];
//                        _markerIds.Add(markerId);
//                        
//                        Matrix4x4 rtMatrix4X4 = new Matrix4x4();
//                        rtMatrix4X4.SetRow(0,
//                            new Vector4((float) _poseMarkersMatrixes.get(i * 5 + 1, 0)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 1, 1)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 1, 2)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 1, 3)[0]));
//                        rtMatrix4X4.SetRow(1,
//                            new Vector4((float) _poseMarkersMatrixes.get(i * 5 + 2, 0)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 2, 1)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 2, 2)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 2, 3)[0]));
//                        rtMatrix4X4.SetRow(2,
//                            new Vector4((float) _poseMarkersMatrixes.get(i * 5 + 3, 0)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 3, 1)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 3, 2)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 3, 3)[0]));
//                        rtMatrix4X4.SetRow(3, new Vector4(0, 0, 0, 1));
//
//                        _foundMarkers.Add(new KeyValuePair<int, Matrix4x4>(0, rtMatrix4X4));
//                        
//                        _foundObjects.Add(new KeyValuePair<int, Matrix4x4>(0, rtMatrix4X4));
//                        
//                        Vector3 localPosition = Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
// 
//                        Debug.Log(
//                            "Marker ID: "+markerId+" RT trasl: "+ localPosition.x+" "+localPosition.y+" "+localPosition.z);
//                        
//                        
//                    }
//                    Debug.Log(
//                        "num Markers: "+_markerIds.Count);

                    //TEST - START
                    _numClassifiedObjs=_nativeObject.GetClassifiedObjecs(_classifiedObjects);
                    _classifiedObjIds.Clear();
                    
                    
                    for (int i = 0; i < _numClassifiedObjs; i++)
                    {
                        int classId = (int) _classifiedObjects.get(i * 2, 0)[0];
                        _classifiedObjIds.Add(classId);
                        startVertex=new Vector3((float)_classifiedObjects.get(i * 2+1, 0)[0],(float)_classifiedObjects.get(i * 2+1, 1)[0],0f);
                        endVertex=new Vector3((float)_classifiedObjects.get(i * 2+1, 2)[0],(float)_classifiedObjects.get(i * 2+1, 3)[0],0f);
//                        Color color = new Color(255, 0, 0);
//                        Debug.DrawLine(start, end);

//                        DrawBox(_frameMatrix, (int) _classifiedObjects.get(i * 2 + 1, 0)[0],
//                            (int) _classifiedObjects.get(i * 2 + 1, 1)[0],
//                            (int) _classifiedObjects.get(i * 2 + 1, 2)[0],
//                            (int) _classifiedObjects.get(i * 2 + 1, 3)[0]);
                        
//                        DrawLine(start, end, color);
                        
//                        Matrix4x4 box = new Matrix4x4();
//                        rtMatrix4X4.SetRow(0,
//                            new Vector4((float) _poseMarkersMatrixes.get(i * 5 + 1, 0)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 1, 1)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 1, 2)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 1, 3)[0]));
//                        rtMatrix4X4.SetRow(1,
//                            new Vector4((float) _poseMarkersMatrixes.get(i * 5 + 2, 0)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 2, 1)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 2, 2)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 2, 3)[0]));
//                        rtMatrix4X4.SetRow(2,
//                            new Vector4((float) _poseMarkersMatrixes.get(i * 5 + 3, 0)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 3, 1)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 3, 2)[0],
//                                (float) _poseMarkersMatrixes.get(i * 5 + 3, 3)[0]));
//                        rtMatrix4X4.SetRow(3, new Vector4(0, 0, 0, 1));
//
//                        _foundMarkers.Add(new KeyValuePair<int, Matrix4x4>(0, rtMatrix4X4));
//                        
//                        _foundObjects.Add(new KeyValuePair<int, Matrix4x4>(0, rtMatrix4X4));
//                        
//                        Vector3 localPosition = Utils.ExtractTranslationFromMatrix(ref rtMatrix4X4);
// 
//                        Debug.Log(
//                            "Marker ID: "+markerId+" RT trasl: "+ localPosition.x+" "+localPosition.y+" "+localPosition.z);
                        Debug.Log(
                            "Box Objs " + classId + " (" +_classifiedObjects.get(i * 2+1, 0)[0]+","+_classifiedObjects.get(i * 2+1, 1)[0]+"),("+_classifiedObjects.get(i * 2+1, 2)[0]+","+_classifiedObjects.get(i * 2+1, 3)[0]+")");

                    }
                    Debug.Log(
                        "num Classified Objs: "+_numClassifiedObjs+" "+_classifiedObjIds.Count);
                    //TEST - END
                    
                    
                    
//                    _nativeObject.GetRTMatrix(_poseMatrix);
//                    
//                    _foundObjects.Clear();
//
//                    for (int i = 0; i < _poseMatrix.rows() / 5; i++)
//                    {
//                        int maskedObjectId = (int) _poseMatrix.get(i * 5, 0)[0];
//                        int objectId = maskedObjectId / 1000;
//                        
//                        /*
//                        //check position
//                        double nx = (float) _poseMatrix.get(i * 5 + 1, 0)[0];//_rtMatrix.at<double>(0, 0);
//                        double ny = (float) _poseMatrix.get(i * 5 + 2, 0)[0];//_rtMatrix.at<double>(1, 0);
//                        double nz = (float) _poseMatrix.get(i * 5 + 3, 0)[0];//_rtMatrix.at<double>(2, 0);
//                        double sz = (float) _poseMatrix.get(i * 5 + 3, 1)[0];//_rtMatrix.at<double>(2, 1);
//                        double az = (float) _poseMatrix.get(i * 5 + 3, 2)[0];//_rtMatrix.at<double>(2, 2);
//
//                        double sy = Math.Sqrt(nx * nx + ny * ny);
//
//                        double phi= Math.Atan2(sz, az); //atan2(sinTheta*ax-cosTheta*ay,cosTheta*sy-sinTheta*sx);
//                        double gamma = Math.Atan2(-nz, sy);
//                        double theta = Math.Atan2(ny, nx);
//
//                        if (Math.Abs(phi) < 0.1 && Math.Abs(gamma) < 0.1 && Math.Abs(theta) < 0.1)
//                        {
//                        */
//                            Matrix4x4 rtMatrix4X4 = new Matrix4x4();
//                            rtMatrix4X4.SetRow(0,
//                                new Vector4((float) _poseMatrix.get(i * 5 + 1, 0)[0],
//                                    (float) _poseMatrix.get(i * 5 + 1, 1)[0],
//                                    (float) _poseMatrix.get(i * 5 + 1, 2)[0],
//                                    (float) _poseMatrix.get(i * 5 + 1, 3)[0]));
//                            rtMatrix4X4.SetRow(1,
//                                new Vector4((float) _poseMatrix.get(i * 5 + 2, 0)[0],
//                                    (float) _poseMatrix.get(i * 5 + 2, 1)[0],
//                                    (float) _poseMatrix.get(i * 5 + 2, 2)[0],
//                                    (float) _poseMatrix.get(i * 5 + 2, 3)[0]));
//                            rtMatrix4X4.SetRow(2,
//                                new Vector4((float) _poseMatrix.get(i * 5 + 3, 0)[0],
//                                    (float) _poseMatrix.get(i * 5 + 3, 1)[0],
//                                    (float) _poseMatrix.get(i * 5 + 3, 2)[0],
//                                    (float) _poseMatrix.get(i * 5 + 3, 3)[0]));
//                            rtMatrix4X4.SetRow(3, new Vector4(0, 0, 0, 1));
//
//                            _foundObjects.Add(new KeyValuePair<int, Matrix4x4>(objectId, rtMatrix4X4));
//                        //}
//                    }

                    
                    
                    List<IGrouping<int, KeyValuePair<int, Matrix4x4>>> foundObjetsGroups =
                        _foundObjects.GroupBy(i => i.Key).ToList();
                    

                    
                    for (int i = 0; i < _arObjectsInternal.Count; i++)
                    {
                        IGrouping<int, KeyValuePair<int, Matrix4x4>> foundObject =
                            foundObjetsGroups.FirstOrDefault(foundObjetsGroup => foundObjetsGroup.Key == i);
                        
                        
                        if (foundObject == null)
                        {
                            for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= 0; j--)
                            {
                                //                            Destroy(_arObjectsInternal[i].Instances[j]);
                                //                            _arObjectsInternal[i].Instances.RemoveAt(j);
                                _arObjectsInternal[i].Instances[j].SetActive(false);
                            }
                        }
                        else
                        {
                            // Add missing instances
                            while (_arObjectsInternal[i].Instances.Count < foundObject.Count())
                            {
                                _arObjectsInternal[i].Instances
                                    .Add(Instantiate(_arObjectsInternal[i].ArObject.ArGameObjectRoot));
                            }

                            
                            //                        // Remove exceeding instances
                            //                        for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= foundObject.Count(); j--)
                            //                        {
                            ////                            Destroy(_arObjectsInternal[i].Instances[j]);
                            ////                            _arObjectsInternal[i].Instances.RemoveAt(j);
                            //                            _arObjectsInternal[i].Instances[j].SetActive(false);
                            //                        }

                            // Hide exceeding instances
                            for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= foundObject.Count(); j--)
                            {
                                //                            Destroy(_arObjectsInternal[i].Instances[j]);
                                //                            _arObjectsInternal[i].Instances.RemoveAt(j);
                                _arObjectsInternal[i].Instances[j].SetActive(false);
                            }

                            // New positions to Gameobjects
                            List<KeyValuePair<int, Matrix4x4>> foundObjectList = foundObject.ToList();
                            for (int sharedIndex = 0;
                                sharedIndex < _arObjectsInternal[i].Instances.Count &&
                                sharedIndex < foundObjectList.Count;
                                sharedIndex++)
                            {
                                GameObject gameObjectInstance = _arObjectsInternal[i].Instances[sharedIndex];
                                Matrix4x4 matrix4X4 = foundObjectList[sharedIndex].Value;

                                
                                
                                // Flip x and y axes for PortraitUpsideDown
                                if (Screen.orientation == ScreenOrientation.PortraitUpsideDown)
                                {
                                    Vector3 localPosition = Utils.ExtractTranslationFromMatrix(ref matrix4X4);
                                    gameObjectInstance.transform.localPosition = new Vector3(-localPosition.x,
                                        -localPosition.y, localPosition.z);
                                    
                                }
                                else
                                {
                                    gameObjectInstance.transform.localPosition =
                                        Utils.ExtractTranslationFromMatrix(ref matrix4X4);
                                }
                                
                                
                                gameObjectInstance.transform.localRotation =
                                    Utils.ExtractRotationFromMatrix(ref matrix4X4);
                                
                                
                                
                                gameObjectInstance.SetActive(true);
                            }
                        }
                    }

                    _isTracking = _foundObjects.Count > 0;

                    //            Not needed anymore
                    //            if (!_isTracking && preview.texture != NatCam.Preview)
                    //            {
                    //                preview.texture = NatCam.Preview;
                    //            }
                }
//            }
//            catch (Exception e)
//            {
//                Debug.Log(e.StackTrace);
//            }
        }    

        void Update()
        {
            ProcessFrameWork();
        }

        void OnDestroy()
        {
            ReleaseObjects();
            _instance = null;
        }

        void ReleaseObjects()
        {
            _initialized = false;
            if (_arObjectsInternal != null)
            {
                for (int i = 0; i < _arObjectsInternal.Count; i++)
                {
                    for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= 0; j--)
                    {
                        DestroyImmediate(_arObjectsInternal[i].Instances[j]);
                        _arObjectsInternal[i].Instances.RemoveAt(j);
                    }
                }
            }

            if (_nativeObject != null)
            {
                _nativeObject.Destroy();
                _nativeObject = null;
            }

            // Hide artifacts generated by screen rotation 
            if (preview != null && preview.texture != null && _frameMatrix != null)
            {
                preview.texture = null;
                preview.color = Color.black;
            }

            if (_poseMatrix != null)
            {
                _poseMatrix.release();
                _poseMatrix = null;
            }
            
            if (_poseMarkersMatrixes != null)
            {
                _poseMarkersMatrixes.release();
                _poseMarkersMatrixes = null;
            }

            if (_frameMatrixPreprocessed != null)
            {
                lock (_lock)
                {
                    _frameMatrixPreprocessed.release();
                    _frameMatrixPreprocessed = null;
                }
            }
            
            //TEST - START
            if (_frameMatrixRGBPreprocessed != null)
            {
                lock (_lock)
                {
                    _frameMatrixRGBPreprocessed.release();
                    _frameMatrixRGBPreprocessed = null;
                }
            }
            //TEST - END
            

            _isTracking = false;
        }

        public class NativeArTMaxConfiguratorPlus : IBrainPadNative
        {
            private readonly IntPtr _nativeObjAddr;

            public NativeArTMaxConfiguratorPlus()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                _nativeObjAddr = nativeCreate();
//#endif
            }

            public void SetCameraParams(int cameraWidth, int cameraHeight, float horizontalViewAngle,
                float verticalViewAngle)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeSetCameraParams(_nativeObjAddr, cameraWidth, cameraHeight, horizontalViewAngle,
                    verticalViewAngle);
//#endif
            }

            public void AddNewObjectToDetect(int objectId, Texture2D referenceImage, Mat worldPlane)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                Mat referenceImageMat;
                Mat referenceImageGreyMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC1);
                if (referenceImage.format == TextureFormat.RGBA32)
                {
                    referenceImageMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC4);
                    OpenCVForUnity.Utils.fastTexture2DToMat(referenceImage, referenceImageMat);
                    Imgproc.cvtColor(referenceImageMat, referenceImageGreyMat, Imgproc.COLOR_RGBA2GRAY);
                }
                else if (referenceImage.format == TextureFormat.RGB24)
                {
                    referenceImageMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC3);
                    OpenCVForUnity.Utils.fastTexture2DToMat(referenceImage, referenceImageMat);
                    Imgproc.cvtColor(referenceImageMat, referenceImageGreyMat, Imgproc.COLOR_RGB2GRAY);
                }
                else if (referenceImage.format == TextureFormat.Alpha8)
                {
                    referenceImageMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC1);
                    OpenCVForUnity.Utils.fastTexture2DToMat(referenceImage, referenceImageMat);
                }
                else
                {
                    Debug.LogError(
                        "Error: wrong image target fromat, the image target should be 32bit RGB with alpha, 24bit RGB or 8bit GreyScale");
//                    EditorUtility.DisplayDialog("Error: wrong reference image", "Your project is correctly setup to run BrainPad", "Ok");
                    return;
                }

//                Old handling with OpenCVForUnity.Utils.texture2DToMat
//                Mat referenceImageMat; = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC4);
//                OpenCVForUnity.Utils.texture2DToMat(referenceImage, referenceImageMat);
//                Mat referenceImageGreyMat = new Mat(referenceImageMat.rows(), referenceImageMat.cols(), CvType.CV_8UC1);
//                Imgproc.cvtColor(referenceImageMat, referenceImageGreyMat, Imgproc.COLOR_RGBA2GRAY);

                nativeAddNewObjectToDetect(_nativeObjAddr, objectId, referenceImageGreyMat.nativeObj,
                    worldPlane.nativeObj);
//#endif
            }

            public void Init()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeInit(_nativeObjAddr);
//#endif
            }

            public void Configure()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeConfigure(_nativeObjAddr);
//#endif
            }

            public void Start()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeStart(_nativeObjAddr);
//#endif
            }

            public void Stop()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeStop(_nativeObjAddr);
//#endif
            }

            public int ProcessFrame(Mat imageRgba)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProcessFrame(_nativeObjAddr, imageRgba.nativeObj);
//#else
//            return 0;
//#endif
            }

            public int GetRTMatrix(Mat poseMatrixOut)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeGetRTMatrix(_nativeObjAddr, poseMatrixOut.nativeObj);
//#else
//            return 0;
//#endif
            }
            
            public int GetMarkersRTMatrixes(Mat poseMarkerMatrixesOut)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeGetMarkersRTMatrixes(_nativeObjAddr,poseMarkerMatrixesOut.nativeObj);
//#else
//            return 0;
//#endif
            }
            
            //TEST - START
            public int GetClassifiedObjecs(Mat classifiedObjects)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeGetDetectedClassifiedObjects(_nativeObjAddr,classifiedObjects.nativeObj);
//#else
//            return 0;
//#endif
            }
            public int SetInferenceGraphBuffer(byte[] inferenceGraphBuffer,byte[] inferenceGraphConfigBuffer,byte[] objectLabelsBuffer)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
//                IntPtr inferenceGraphBufferAddr = inferenceGraphBuffer;
//                IntPtr inferenceGraphConfigBufferAddr = inferenceGraphConfigBuffer;
//                IntPtr objectLabelsBufferAddr = objectLabelsBuffer;
                int inferenceGraphBufferLength = inferenceGraphBuffer.Length;
                int inferenceGraphConfigBufferLength = inferenceGraphConfigBuffer.Length;
                int objectLabelsBufferLength = objectLabelsBuffer.Length;
                
                return nativeSetInferenceGraphBuffer(_nativeObjAddr, inferenceGraphBuffer,inferenceGraphBufferLength,inferenceGraphConfigBuffer,inferenceGraphConfigBufferLength,objectLabelsBuffer,objectLabelsBufferLength);
//#else
//            return 0;
//#endif
            }
            //TEST - END

            public void Destroy()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeDestroy(_nativeObjAddr);
//#endif
            }

//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
#if !UNITY_EDITOR
            const string LIBNAME = "ArTMaxConfiguratorPlus";
#else
//        const string LIBNAME = "__Internal";
            const string LIBNAME = "Sapphire";
#endif

            [DllImport(LIBNAME)]
            private static extern IntPtr nativeCreate();

            [DllImport(LIBNAME)]
            private static extern void nativeDestroy(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeAddNewObjectToDetect(IntPtr nativeObjAddr, int objectId, IntPtr queryFrame,
                IntPtr worldPlane);

            [DllImport(LIBNAME)]
            private static extern int nativeInit(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeSetCameraParams(IntPtr nativeObjAddr, int width, int height, float hFov,
                float vFov);

            [DllImport(LIBNAME)]
            private static extern int nativeConfigure(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeStart(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeStop(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeProcessFrame(IntPtr nativeObjAddr, IntPtr rImage);

//        [DllImport(LIBNAME)]
//        private static extern int nativeGetHomographies(IntPtr nativeObjAddr, IntPtr rHomographiesOut);

            [DllImport(LIBNAME)]
            private static extern int nativeGetRTMatrix(IntPtr nativeObjAddr, IntPtr rRTMatrixOut);
            
            [DllImport(LIBNAME)]
            private static extern int nativeGetMarkersRTMatrixes(IntPtr nativeObjAddr,IntPtr rRTMatrixOut);

            //TEST - START
            [DllImport(LIBNAME)]
            private static extern int nativeGetDetectedClassifiedObjects(IntPtr nativeObjAddr,IntPtr rDetectedObjectsOut);

            [DllImport(LIBNAME)]
            private static extern int  nativeSetInferenceGraphBuffer(IntPtr nativeObj, byte[] rInferenceGraphBuffer,int inferenceGraphBufferLength,byte[] rInferenceGraphConfigBuffer,int inferenceGraphConfigBufferLength,byte[] objectLabelsBuffer,int objectLabelsBufferLength);
            //TEST - END
                        
            [DllImport(LIBNAME)]
            private static extern int nativeGetReferenceObjects(IntPtr nativeObjAddr, IntPtr rRefObjsOut);

            [DllImport(LIBNAME)]
            private static extern int nativeGetImage(IntPtr nativeObjAddr, IntPtr rImageOut);

            [DllImport(LIBNAME)]
            private static extern int nativeSetImage(IntPtr nativeObjAddr, IntPtr rImage);
        }
    }
}